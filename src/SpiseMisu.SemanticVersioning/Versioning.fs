namespace SpiseMisu

module Semantic =
  open Microsoft.FSharp.Reflection
  open System
  open System.Reflection
  
  type version = Major | Minor | Patch
  type netType =
    | Abstract | Class | Enum | Interface | Static | Struct
    (* | Primitive | ValueType *)
    | Module | RecordType | SumType | UnionConstructor | UnionTags
  
  module private Print =
    type 'a roc = System.Collections.ObjectModel.ReadOnlyCollection<'a>
    type catarg = CustomAttributeTypedArgument
    
    let whenStatic : bool -> string -> string =
      fun flag fulltypename ->
        match flag with
          | true -> fulltypename
          | false -> sprintf "(Instance/Inheritance of %s)" fulltypename
    
    let rec typeFullName =
      fun (t:Type) ->
        let fullname =
          match t.IsGenericType with
            | false ->
              match String.IsNullOrEmpty t.FullName with
                | true -> sprintf "'%s" (t.Name.ToUpperInvariant())
                | false -> t.FullName 
            | true ->
              let args =
                t.GetGenericArguments()
                |> Array.map typeFullName
                |> Array.reduce(sprintf "%s,%s")
              sprintf "%s<%s>" (t.FullName.Substring(0,t.FullName.IndexOf('`'))) args
        let guid = Guid.NewGuid().ToString()
        fullname.Replace("+Tags",guid).Replace('+','.').Replace(guid,"+Tags")
    
    let enumHlp : Type -> (string * string) array =
      fun t -> t.IsEnum |> function
        | false -> [| |]
        | true ->
          t.GetFields()
          |> Array.filter(fun x -> x.FieldType.IsEnum)
          |> Array.map(fun x -> x.Name, sprintf "%A" (x.GetRawConstantValue()))
    
    let enumValues : Type -> (string * string) array =
      fun t ->
        t
        |> enumHlp
        |> Array.map(fun (name,value) -> sprintf "%s:%s" name value)
        |> Array.sort
        |> Array.reduce(fun x y -> sprintf "%s; %s" x y)
        |> fun s ->
          let t' = typeFullName t
          [| t',(sprintf "%s values: [ %s ]" t' s) |]
    
    let enums : Type -> (string * string) array =
      fun t ->
        t
        |> enumHlp
        |> Array.map(
          fun (name,_) ->
            let t' = typeFullName t
            t',(sprintf "%s.%s : %s" t' name t')
          )
    
    let unionValues : Type -> (string * string) array =
      fun t ->
        FSharpType.GetUnionCases(t)
        |> Array.map(
          fun x ->
            let ps =
              match Array.isEmpty (x.GetFields()) with
                | true -> String.Empty
                | false ->
                  x.GetFields()
                  |> Array.map(fun pi -> typeFullName pi.PropertyType)
                  |> Array.fold(fun a x -> a + x) String.Empty
            match String.IsNullOrEmpty ps with
              | true -> x.Name
              | false -> sprintf "%s of %s" x.Name ps
          )
        |> Array.sort
        |> Array.reduce(fun x y -> sprintf "%s | %s" x y)
        |> fun s ->
          let t' = typeFullName t
          [| t',(sprintf "%s values: [ %s ]" t' s) |]
    
    let parameters =
      fun(cad:CustomAttributeData seq, pi:ParameterInfo array) ->
        match Array.isEmpty pi with
          | true -> "System.Void"
          | false ->
            let groupArgs (xs:CustomAttributeTypedArgument) =
              match xs.Value.GetType().Equals(typeof<roc<catarg>>) with
                | false ->
                  Seq.empty
                | true ->
                  seq{ yield xs.Value }
                  |> Seq.cast<System.Reflection.CustomAttributeTypedArgument seq>
                  |> Seq.map(fun ys -> ys |> Seq.map(fun y -> y.Value) |> Seq.cast<int>)
                  |> Seq.concat
              
            let currying =
              cad
              |> Seq.map(
                fun xs ->
                  xs.ConstructorArguments
                  |> Seq.map(groupArgs)
                  |> Seq.concat
                )
                |> Seq.concat
              
            let ps =
              pi
              |> Array.map(
                fun x -> sprintf "%s:%s" x.Name (typeFullName x.ParameterType))
            
            let ps' =
              match Seq.isEmpty currying with
                | true ->
                  match Array.isEmpty ps with
                    | true  -> [| |]
                    | false -> [| ps |> Array.reduce(sprintf "%s * %s") |]
                | false ->
                  currying
                  |> Seq.fold(fun (i,a) x -> (i+x,ps.[i .. i+(x-1)] :: a)) (0,[])
                  |> fun (_,xs) ->
                    xs |> List.map(
                         fun ys ->
                           match Array.isEmpty ys with
                             | true  -> ""
                             | false -> ys |> Array.reduce(sprintf "%s * %s"))
                       |> List.rev
                       |> List.toArray
            
            ps'
            |> Array.reduce(sprintf "%s -> %s")
    
    let field =
      fun (t:string, fi:FieldInfo) ->
        sprintf "%s.%s : %s"
          (whenStatic (fi.IsStatic) t)
          fi.Name
          (typeFullName fi.FieldType)
              
    let constructor' =
      fun (t:string, ci:ConstructorInfo) ->
        sprintf "new %s : %s -> %s" t (parameters (ci.CustomAttributes,ci.GetParameters())) t
    
    let recordConstructor =
      fun (t:string, ci:ConstructorInfo) ->
        sprintf "{ %s.%s } -> %s" t (parameters (ci.CustomAttributes,ci.GetParameters())) t
    
    let unionConstructor' =
      fun (t:string, t':string, ci:ConstructorInfo) ->
        sprintf "%s : %s -> %s"
          t (parameters (ci.CustomAttributes,ci.GetParameters())) t'
    
    let property =
      fun (t:string, pi:PropertyInfo) ->
        sprintf "%s.%s : %s"
          (whenStatic (pi.GetGetMethod().IsStatic) t)
          pi.Name
          (typeFullName pi.PropertyType)
    
    let method' =
      fun (t:string, mi:MethodInfo) ->
        sprintf "%s.%s : %s -> %s"
          (whenStatic (mi.IsStatic) t)
          mi.Name
          (parameters (mi.CustomAttributes,mi.GetParameters()))
          (typeFullName mi.ReturnType)
    
    let event' =
      fun (t:string, ei:EventInfo) ->
        let mi = ei.EventHandlerType.GetMethod("Invoke")
        sprintf "%s.%s : %s -> %s"
          (whenStatic (mi.IsStatic) t)
          (sprintf "%s.Add" ei.Name)
          (parameters (mi.CustomAttributes,mi.GetParameters()))
          (typeFullName mi.ReturnType)
  
  module private Reflect =
    let private tagNetHlp : Type -> (string option * string option) =
      fun t ->
        match t with
          | _ when t.FullName.EndsWith("+Tags") -> (Some "UnionTags", None)
          | _ -> 
            t.CustomAttributes
            |> Seq.filter(
              fun x ->
                x.AttributeType.Name.Contains("Debugger") |> not)
            |> Seq.map(
              fun x ->
                x.AttributeType.Name,
                x.ConstructorArguments |> Seq.map(fun x -> x.Value.ToString()))
            |> Seq.map(
              fun (x,ys) ->
                (if x.Equals("CompilationMappingAttribute") then None else Some x),
                (if Seq.isEmpty ys then None else Seq.head ys |> Some))
            |> fun xs ->
              if Seq.isEmpty xs then (None,None) else Seq.head xs
    
    let tagNetType : Type -> netType =
      fun t ->
        // TODO: Active/Partial Patterns, MeasureOfUnits? Any other?
        match tagNetHlp t with
          | Some "UnionTags"       , _________________ -> netType.UnionTags
          | Some "StructAttribute" , _________________ -> netType.Struct
          | ______________________ , Some "Module"     -> netType.Module
          | ______________________ , Some "RecordType" -> netType.RecordType
          | ______________________ , Some "SumType"    -> netType.SumType
          | ______________________ ,__________________ ->
            match t with
              | _ when t.IsInterface ->
                netType.Interface
              | _ when t.IsEnum ->
                netType.Enum
              | _ when t.IsAbstract && t.IsClass && t.IsSealed ->
                netType.Static
              | _ when t.IsValueType && not t.IsEnum && not t.IsPrimitive ->
                netType.Struct
              | _ when t.IsAbstract ->
                netType.Abstract
              | _ -> netType.Class
    
    let exportedTypes : Assembly -> Type array =
      fun asm ->
        let ts =
          try
            asm.GetExportedTypes()
          with
            | :? ReflectionTypeLoadException as ex ->
              ex.Types
              |> Array.filter(function | null -> false | _ -> true)
            | _ as ex -> failwith ex.Message
        ts
            
    let constructers : BindingFlags list -> Type -> (string * ConstructorInfo) array =
      fun bfs t ->
        t.GetConstructors(bfs |> List.fold(fun a x -> a ||| x) BindingFlags.Instance)
        |> Array.sortBy(fun x -> x.Name, x.GetParameters() |> Array.length)
        |> Array.map(fun x -> Print.typeFullName t, x)
  
  module Versioning =
    open SpiseMisu.NuGet
    
    let private typeInfo : Type -> string =
      fun x ->
        let bt : Type -> string =
          function | null -> "none" | _ -> x.BaseType |> Print.typeFullName
        
        sprintf "%s (.NET type: %A and base: %s)"
          (Print.typeFullName x) (Reflect.tagNetType x) (bt x.BaseType)
    
    let raw : Assembly -> (string * string) Set =
      fun asm ->
        let isSumType : Type -> bool =
          fun t ->
            t.IsNested &&
            (t.BaseType |> function
               | null  -> false
               | type' -> (Reflect.tagNetType type') = SumType)
            
        let ts =
          asm
          |> Reflect.exportedTypes
        let namespaces =
          ts
          |> Array.map(fun x -> x.Namespace)
          |> Array.distinct
          |> Array.map (fun x -> x,(sprintf "%s (Namespace)" x))
        let types =
          ts
          |> Array.filter(isSumType >> not)
          |> Array.map (fun x -> (Print.typeFullName x,typeInfo x))
        let enums =
          ts
          |> Array.filter (Reflect.tagNetType >> function | Enum -> true | _ -> false)
          |> Array.map (fun x -> Print.enums x)
          |> Array.concat
        let enumValues =
          ts
          |> Array.filter (Reflect.tagNetType >> function | Enum -> true | _ -> false)
          |> Array.map (fun x -> Print.enumValues x)
          |> Array.concat
        let unionValues =
          ts
          |> Array.filter (Reflect.tagNetType >> function | SumType -> true | _ -> false)
          |> Array.map (fun x -> Print.unionValues x)
          |> Array.concat
        
        ts
        |> Array.filter(isSumType >> not)
        |> Array.map(
          fun t ->
            t.GetMembers()
            |> Array.map(
              fun m ->
                t |> Reflect.tagNetType,
                t |> Print.typeFullName,
                m))
        |> Array.concat
        |> Array.map (
          fun (tag,fullname,m) ->
            // Handle cases like the Fsharp.Core does (and a bit more):
            //
            // https://github.com/Microsoft/visualfsharp/blob/master/src/fsharp/
            //   FSharp.Core.Unittests/LibraryTestFx.fs#L103-L110
            //
            match m.MemberType with
              | MemberTypes.Constructor ->
                let nameInfo = (fullname, m :?> ConstructorInfo)
                match tag with
                  | RecordType -> fullname,(Print.recordConstructor nameInfo)
                  | __________ -> fullname,(Print.constructor' nameInfo)
              | MemberTypes.Event ->
                fullname,(Print.event' (fullname, m :?> EventInfo))
              | MemberTypes.Field -> 
                fullname,(Print.field (fullname, m :?> FieldInfo))
              | MemberTypes.Method ->
                fullname,(Print.method' (fullname, m :?> MethodInfo))
              | MemberTypes.NestedType ->
                let nt = (m :?> Type)
                match tag with
                  | SumType ->
                    [| nt |]
                    |> Array.collect (Reflect.constructers [BindingFlags.NonPublic])
                    |> Array.map (fun (x,y) -> Print.unionConstructor' (x,fullname,y))
                    |> Array.fold(fun a x -> x + a) String.Empty
                    |> fun x -> fullname, x 
                  | _ ->
                    // Already handled in `let types = ...`
                    fullname, System.String.Empty 
              | MemberTypes.Property ->
                fullname, (Print.property (fullname, m :?> PropertyInfo))
              | _ ->
                failwith
                  (sprintf "not handled: (%A,%s,%A)" tag fullname m.MemberType))
        |> Array.append namespaces
        |> Array.append types
        |> Array.append enums
        |> Array.append enumValues
        |> Array.append unionValues
        |> Array.distinct
        |> Array.filter (fun (_,x) -> System.String.IsNullOrEmpty x |> not)
        |> Array.fold(fun a xy -> a |> Set.add xy) Set.empty
    
    let private prettyPrint : int -> string -> string =
      fun indentation str ->
        let tab = String.replicate indentation " "
        
        match str.Contains(" : ") with
          | false -> sprintf "\n%s%s" tab str
          | true ->
            str.Split([| " : " |],StringSplitOptions.None)
            |> Array.map(fun x -> tab + x)
            |> Array.reduce(
              fun x y ->
                sprintf "%s : \n%s%s"
                  x tab y)
            |> function | s -> "\n" + s
            |> function | s -> s.Split([| " -> " |],StringSplitOptions.None)
            |> Array.reduce(
              fun x y ->
                sprintf "%s\n%s%s-> %s"
                  x tab tab y)
            |> fun x -> x.Contains("Removed: ") |> function
              | true  -> x.Replace("Removed: ", "- ")
              | false -> x
            |> fun x -> x.Contains("Added: ") |> function
              | true  -> x.Replace("Added: ", "+ ")
              | false -> x
    
    let private sort' : string -> string =
      fun x ->
        x.Replace("Removed: ", System.String.Empty)
         .Replace("Added: ", System.String.Empty)
         .Replace("new ", System.String.Empty)
         .Replace("(Instance/Inheritance of ", System.String.Empty)
         .Replace("( ", System.String.Empty)
         .Replace("{ ", System.String.Empty)
    
    let versionNr : Assembly -> string =
      fun asm -> sprintf "%A" (asm.GetName().Version)
    
    let docs : Assembly -> string array =
      fun asm ->
        raw asm
        |> Set.toArray
        |> Array.groupBy(fun (g,_) -> g)
        |> Array.sortBy(fun (g,_) -> g)
        |> Array.map(
          fun (g,xs) ->
            let (_,ys) = xs |> Array.unzip
            Array.append
              [| sprintf "\n* %s" g |]
              (ys |> Array.sortBy(sort'))
          )
        |> Array.concat
        |> Array.map (fun x -> x |> prettyPrint 8)
    
    let bump : string -> Assembly -> Assembly -> string =
      fun verNr released modified ->
        let pub = raw released
        let dev = raw modified
        
        let deleted = pub - dev
        let created = dev - pub
        
        let ver =
          match Set.isEmpty deleted, Set.isEmpty created with
            | true  , true  -> version.Patch
            | false , _____ -> version.Major
            | _____ , false -> version.Minor
        
        let hlp n xs =
          xs
          |> Array.mapi(
            fun i x ->
              match i with
                | _ when i = n -> x + 1
                | _ when i > n -> 0
                | _ -> x)
        
        let nr =
          verNr.Split('.')
          |> Array.map (
            fun x -> x |> Int32.TryParse |> function
              | true, value -> value
              | _ -> 0)
          
        let nr' =
          match ver with
            | version.Patch -> nr |> hlp 2
            | version.Minor -> nr |> hlp 1
            | version.Major -> nr |> hlp 0
          |> Array.map string
          |> Array.reduce (fun x y -> sprintf "%s.%s" x y)
        
        sprintf "### %s - %s - %A"
          nr' (DateTime.Now.ToUniversalTime().ToString("MMM dd yyyy")) ver
    
    let diff : Assembly -> Assembly -> string array =
      fun source target ->
        let pub = raw source
        let dev = raw target
        
        let deleted =
          pub - dev
          |> Set.map(fun (x,y) -> (x,"Removed: " + y))
        let created =
          dev - pub
          |> Set.map(fun (x,y) -> (x,"Added: " + y))
        
        deleted + created
        |> Set.toArray
        |> Array.groupBy(fun (g,_) -> g)
        |> Array.sortBy(fun (g,_) -> g)
        |> Array.map(
          fun (g,xs) ->
            let (_,ys) = xs |> Array.unzip
            Array.append
              [| sprintf "\n* %s" g |]
              (ys |> Array.sortBy(sort'))
          )
        |> Array.concat
        |> Array.map (fun x -> x |> prettyPrint 8)
    
    let asmbump : Assembly -> Assembly -> string =
      fun released modified ->
        let verNr = versionNr released
        bump verNr released modified
    
    let asmdiff : Assembly -> Assembly -> string array =
      fun source target ->
        diff source target
    
    let nugetbump : string option -> string -> dotNet -> Assembly -> string =
      fun apiUrl packageID dotnet modified ->
        SpiseMisu.NuGet.get apiUrl packageID None dotnet
        |> function
          | Choice1Of2 ( verNr, latestStable ) ->
            bump verNr latestStable modified
          | Choice2Of2 ex -> ex.Message
      
    let nugetdiff :
      string option -> string -> dotNet -> string option
      -> string option -> string -> dotNet -> string option
      -> string array =
      fun apiUrl1 packageID1 dotnet1 verNr1 apiUrl2 packageID2 dotnet2 verNr2 ->
        let v1 = SpiseMisu.NuGet.get apiUrl1 packageID1 verNr1 dotnet1
        let v2 = SpiseMisu.NuGet.get apiUrl2 packageID2 verNr2 dotnet2
        
        match v1,v2 with
          | (Choice1Of2 (_,asm1)), (Choice1Of2 (_,asm2)) ->
            diff asm1 asm2
          | Choice2Of2 ex1, Choice2Of2 ex2 ->
            [| ex1.Message; ex2.Message |]
          | Choice2Of2 ex, _ | _, Choice2Of2 ex ->
            [| ex.Message |]
