(*** hide ***)
// This block of code is omitted in the generated HTML documentation. Use 
// it to define helpers that you do not want to show in the documentation.
#I "../../bin/SpiseMisu.SemanticVersioning/"

(**
SpiseMisu.SemanticVersioning
======================

This is a library to handle [Semantic Versioning (SemVer)][semver] for *.NET Assemblies* and *NuGet packages* (C# and F#, even proprietary, due to Reflection).

The library is heavily inspired in [Elm package][elm], which enforces *SemVer* by the code itself, providing two main actions: *bump* and *diff*.

Produced output is *Markdown*, which is easily readable.

For more information on *Semantic Versioning* and this library, please read the following [blog post][blog] I wrote for the [F# Advent Calendar in English 2016][advent].

> **Note**: It's important to understand that this library tries to help minimize the dependency hell as described in the blog post. It doesn't solve the compatible/incompatible dependencies problem, as that is [NP-complete][npcomplete].

<div class="row">
  <div class="span1"></div>
  <div class="span6">
    <div class="well well-small" id="nuget">
      The SpiseMisu.SemanticVersioning library can be <a href="https://nuget.org/packages/SpiseMisu.SemanticVersioning">installed from NuGet</a>:
      <pre>PM> Install-Package SpiseMisu.SemanticVersioning</pre>
    </div>
  </div>
  <div class="span1"></div>
</div>

Example
-------

This example demonstrates using a function defined in this sample library.

*)
#r @"SpiseMisu.SemanticVersioning.dll"

open System    
open System.Diagnostics
open System.Reflection

open SpiseMisu

let asm1 =
  Assembly.LoadFile
    @"./lib/Enum.dll"
let asm2 =
  Assembly.LoadFile
    @"./lib/Enum2.dll"
let asm3 =
  Assembly.LoadFile
    @"./lib/Enum3.dll"

Semantic.Versioning.asmbump asm1 asm2
|> printfn "%s"

Semantic.Versioning.asmdiff asm1 asm2
|> Array.iter(printfn "%s")

Semantic.Versioning.asmbump asm1 asm3
|> printfn "%s"

Semantic.Versioning.asmdiff asm1 asm3
|> Array.iter(printfn "%s")

(**
> **Note**: For output and other example files, see [Output files][output].

Samples & documentation
-----------------------

The library comes with comprehensible documentation. 
It can include tutorials automatically generated from `*.fsx` files in [the content folder][content]. 
The API reference is automatically generated from Markdown comments in the library implementation.

 * [Tutorial](tutorial.html) contains a further explanation of this sample library.

 * [API Reference](reference/index.html) contains automatically generated documentation for all types, modules
   and functions in the library. This includes additional brief samples on using most of the
   functions.
 
Contributing and copyleft
-------------------------

The project is hosted on [GitHub][gh] where you can [report issues][issues], fork 
the project and submit pull requests. If you're adding a new public API, please also 
consider adding [samples][content] that can be turned into a documentation. You might
also want to read the [library design notes][readme] to understand how it works.

The library is available under Public Domain license, which allows modification and 
redistribution for both commercial and non-commercial purposes. For more information see the 
[License file][license] in the GitHub repository. 

  [semver]: http://semver.org/
  [blog]: http://blog.stermon.com/articles/2016/12/01/semantic-versioning-dotnet-libs-and-nuget-pkgs
  [advent]: https://sergeytihon.wordpress.com/2016/10/23/f-advent-calendar-in-english-2016
  [elm]: https://github.com/elm-lang/elm-package
  [npcomplete]: https://research.swtch.com/version-sat
  [content]: https://github.com/spisemisu/SpiseMisu.SemanticVersioning/tree/master/docs/content
  [gh]: https://github.com/spisemisu/SpiseMisu.SemanticVersioning
  [issues]: https://github.com/spisemisu/SpiseMisu.SemanticVersioning/issues
  [readme]: https://github.com/spisemisu/SpiseMisu.SemanticVersioning/blob/master/README.md
  [license]: https://github.com/spisemisu/SpiseMisu.SemanticVersioning/blob/master/LICENSE.txt
  [output]: https://github.com/spisemisu/SpiseMisu.SemanticVersioning/tree/sync/example/
*)
