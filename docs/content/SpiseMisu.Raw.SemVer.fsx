(**
SpiseMisu.Raw.SemVer.fsx
========================
*)

#!/usr/bin/env fsharpi

#I @"../bin/SpiseMisu.SemanticVersioning/"
#r @"SpiseMisu.SemanticVersioning.dll"

open System    
open System.Diagnostics
open System.Reflection

open SpiseMisu

let assembly =
  Assembly.LoadFile
    @"./packages/Newtonsoft.Json/lib/net45/Newtonsoft.Json.dll"

Semantic.Versioning.raw assembly
|> Set.toArray
|> Array.iter(fun (prefix, body) -> printfn "%s - %s" prefix body)
(**
> **Note**: For output and other example files, see [Output files][output].

  [output]: https://github.com/spisemisu/SpiseMisu.SemanticVersioning/tree/sync/example/ *)
