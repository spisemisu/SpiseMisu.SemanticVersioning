#!/bin/bash

# Json.NET:
nuget install Newtonsoft.Json -OutputDirectory packages -ExcludeVersion
nuget install Newtonsoft.Json -OutputDirectory packages -Version 8.0.3
nuget install Newtonsoft.Json -OutputDirectory packages -Version 8.0.1
nuget install Newtonsoft.Json -OutputDirectory packages -Version 7.0.1
