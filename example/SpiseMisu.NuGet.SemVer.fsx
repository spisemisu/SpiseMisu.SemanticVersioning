#!/usr/bin/env fsharpi

#I @"../bin/SpiseMisu.SemanticVersioning/"
#r @"SpiseMisu.SemanticVersioning.dll"

open System    
open System.Diagnostics
open System.Reflection

open SpiseMisu

let pkgid = @"Newtonsoft.Json"

let assembly =
  Assembly.LoadFile
    @"./packages/Newtonsoft.Json.7.0.1/lib/net45/Newtonsoft.Json.dll"

Semantic.Versioning.nugetbump
  None pkgid NuGet.dotNet.Net45 assembly
|> printfn "%s"

Semantic.Versioning.nugetdiff
  None pkgid NuGet.dotNet.Net45 None
  None pkgid NuGet.dotNet.Net45 (Some "7.0.1")
|> Array.iter(printfn "%s")
