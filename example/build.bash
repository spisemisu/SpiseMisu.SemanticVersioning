#!/bin/bash

if [ ! -d lib/ ]; then
    mkdir -p lib/;
fi

for f in src/*.cs
do
    name=${f##*/}
    if [ ${name%.*} != "AssemblyInfo" ] ; then
	echo "Compiling:" $f
	mcs -target:library -out:"./lib/"${name%.*}".dll" $f "./src/AssemblyInfo.cs"
    fi
done

for f in src/*.fs
do
    name=${f##*/}
    if [ ${name%.*} != "AssemblyInfo" ] ; then
	echo "Compiling:" $f
	fsharpc --target:library --out:"./lib/"${name%.*}".dll" $f "./src/AssemblyInfo.fs"
    fi
done
