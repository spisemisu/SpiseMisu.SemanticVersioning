        
* Newtonsoft.Json

        Newtonsoft.Json (Namespace)

        
* Newtonsoft.Json.Bson

        Newtonsoft.Json.Bson (Namespace)

        
* Newtonsoft.Json.Bson.BsonObjectId

        Newtonsoft.Json.Bson.BsonObjectId (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Bson.BsonObjectId : 
                value:System.Byte[]
                -> Newtonsoft.Json.Bson.BsonObjectId

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonObjectId).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonObjectId).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonObjectId).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonObjectId).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonObjectId).Value : 
                System.Byte[]

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonObjectId).get_Value : 
                System.Void
                -> System.Byte[]

        
* Newtonsoft.Json.Bson.BsonReader

        Newtonsoft.Json.Bson.BsonReader (.NET type: Class and base: Newtonsoft.Json.JsonReader)

        new Newtonsoft.Json.Bson.BsonReader : 
                reader:System.IO.BinaryReader * readRootValueAsArray:System.Boolean * dateTimeKindHandling:System.DateTimeKind
                -> Newtonsoft.Json.Bson.BsonReader

        new Newtonsoft.Json.Bson.BsonReader : 
                reader:System.IO.BinaryReader
                -> Newtonsoft.Json.Bson.BsonReader

        new Newtonsoft.Json.Bson.BsonReader : 
                stream:System.IO.Stream * readRootValueAsArray:System.Boolean * dateTimeKindHandling:System.DateTimeKind
                -> Newtonsoft.Json.Bson.BsonReader

        new Newtonsoft.Json.Bson.BsonReader : 
                stream:System.IO.Stream
                -> Newtonsoft.Json.Bson.BsonReader

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).Close : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).CloseInput : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).DateParseHandling : 
                Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).DateTimeKindHandling : 
                System.DateTimeKind

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).Depth : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).FloatParseHandling : 
                Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).JsonNet35BinaryCompatibility : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).MaxDepth : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).QuoteChar : 
                System.Char

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).Read : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsBytes : 
                System.Void
                -> System.Byte[]

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsDateTime : 
                System.Void
                -> System.Nullable<System.DateTime>

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsDateTimeOffset : 
                System.Void
                -> System.Nullable<System.DateTimeOffset>

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsDecimal : 
                System.Void
                -> System.Nullable<System.Decimal>

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsInt32 : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadRootValueAsArray : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).Skip : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).SupportMultipleContent : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).TokenType : 
                Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).Value : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ValueType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_CloseInput : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_DateParseHandling : 
                System.Void
                -> Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_DateTimeKindHandling : 
                System.Void
                -> System.DateTimeKind

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_Depth : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_FloatParseHandling : 
                System.Void
                -> Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_JsonNet35BinaryCompatibility : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_MaxDepth : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_QuoteChar : 
                System.Void
                -> System.Char

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_ReadRootValueAsArray : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_SupportMultipleContent : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_TokenType : 
                System.Void
                -> Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_Value : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).get_ValueType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_CloseInput : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_DateParseHandling : 
                value:Newtonsoft.Json.DateParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_DateTimeKindHandling : 
                value:System.DateTimeKind
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_FloatParseHandling : 
                value:Newtonsoft.Json.FloatParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_JsonNet35BinaryCompatibility : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_MaxDepth : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_ReadRootValueAsArray : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).set_SupportMultipleContent : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Bson.BsonWriter

        Newtonsoft.Json.Bson.BsonWriter (.NET type: Class and base: Newtonsoft.Json.JsonWriter)

        new Newtonsoft.Json.Bson.BsonWriter : 
                stream:System.IO.Stream
                -> Newtonsoft.Json.Bson.BsonWriter

        new Newtonsoft.Json.Bson.BsonWriter : 
                writer:System.IO.BinaryWriter
                -> Newtonsoft.Json.Bson.BsonWriter

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).Close : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).CloseOutput : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).DateFormatHandling : 
                Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).DateTimeKindHandling : 
                System.DateTimeKind

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).FloatFormatHandling : 
                Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).Flush : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).Formatting : 
                Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).StringEscapeHandling : 
                Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteComment : 
                text:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteEnd : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteEndArray : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteEndConstructor : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteEndObject : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteNull : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteObjectId : 
                value:System.Byte[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WritePropertyName : 
                name:System.String * escape:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WritePropertyName : 
                name:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteRaw : 
                json:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteRawValue : 
                json:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteRegex : 
                pattern:System.String * options:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteStartArray : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteStartConstructor : 
                name:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteStartObject : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteState : 
                Newtonsoft.Json.WriteState

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteToken : 
                reader:Newtonsoft.Json.JsonReader * writeChildren:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteToken : 
                reader:Newtonsoft.Json.JsonReader
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteToken : 
                token:Newtonsoft.Json.JsonToken * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteToken : 
                token:Newtonsoft.Json.JsonToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteUndefined : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Byte
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Byte[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Char
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.DateTime
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.DateTimeOffset
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Decimal
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Double
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Guid
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Int16
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Int64
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Byte>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Char>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.DateTime>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.DateTimeOffset>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Decimal>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Double>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Guid>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Int16>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Int64>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.SByte>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.Single>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.TimeSpan>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.UInt16>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.UInt32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Nullable<System.UInt64>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.SByte
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Single
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.TimeSpan
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.UInt16
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.UInt32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.UInt64
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteValue : 
                value:System.Uri
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).WriteWhitespace : 
                ws:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_CloseOutput : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_DateFormatHandling : 
                System.Void
                -> Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_DateTimeKindHandling : 
                System.Void
                -> System.DateTimeKind

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_FloatFormatHandling : 
                System.Void
                -> Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_Formatting : 
                System.Void
                -> Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_StringEscapeHandling : 
                System.Void
                -> Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).get_WriteState : 
                System.Void
                -> Newtonsoft.Json.WriteState

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).set_CloseOutput : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).set_DateFormatHandling : 
                value:Newtonsoft.Json.DateFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).set_DateTimeKindHandling : 
                value:System.DateTimeKind
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).set_FloatFormatHandling : 
                value:Newtonsoft.Json.FloatFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).set_Formatting : 
                value:Newtonsoft.Json.Formatting
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Bson.BsonWriter).set_StringEscapeHandling : 
                value:Newtonsoft.Json.StringEscapeHandling
                -> System.Void

        
* Newtonsoft.Json.ConstructorHandling

        Newtonsoft.Json.ConstructorHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.ConstructorHandling values: [ AllowNonPublicDefaultConstructor:1; Default:0 ]

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ConstructorHandling).value__ : 
                System.Int32

        Newtonsoft.Json.ConstructorHandling.AllowNonPublicDefaultConstructor : 
                Newtonsoft.Json.ConstructorHandling

        Newtonsoft.Json.ConstructorHandling.Default : 
                Newtonsoft.Json.ConstructorHandling

        
* Newtonsoft.Json.Converters

        Newtonsoft.Json.Converters (Namespace)

        
* Newtonsoft.Json.Converters.BinaryConverter

        Newtonsoft.Json.Converters.BinaryConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.BinaryConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.BinaryConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BinaryConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.BsonObjectIdConverter

        Newtonsoft.Json.Converters.BsonObjectIdConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.BsonObjectIdConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.BsonObjectIdConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.BsonObjectIdConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.CustomCreationConverter<'T>

        Newtonsoft.Json.Converters.CustomCreationConverter<'T> (.NET type: Abstract and base: Newtonsoft.Json.JsonConverter)

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).Create : 
                objectType:System.Type
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.CustomCreationConverter<'T>).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.DataSetConverter

        Newtonsoft.Json.Converters.DataSetConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.DataSetConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.DataSetConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).CanConvert : 
                valueType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataSetConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.DataTableConverter

        Newtonsoft.Json.Converters.DataTableConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.DataTableConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.DataTableConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).CanConvert : 
                valueType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DataTableConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.DateTimeConverterBase

        Newtonsoft.Json.Converters.DateTimeConverterBase (.NET type: Abstract and base: Newtonsoft.Json.JsonConverter)

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DateTimeConverterBase).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.DiscriminatedUnionConverter

        Newtonsoft.Json.Converters.DiscriminatedUnionConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.DiscriminatedUnionConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.DiscriminatedUnionConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.DiscriminatedUnionConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.EntityKeyMemberConverter

        Newtonsoft.Json.Converters.EntityKeyMemberConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.EntityKeyMemberConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.EntityKeyMemberConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.EntityKeyMemberConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.ExpandoObjectConverter

        Newtonsoft.Json.Converters.ExpandoObjectConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.ExpandoObjectConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.ExpandoObjectConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.ExpandoObjectConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.IsoDateTimeConverter

        Newtonsoft.Json.Converters.IsoDateTimeConverter (.NET type: Class and base: Newtonsoft.Json.Converters.DateTimeConverterBase)

        new Newtonsoft.Json.Converters.IsoDateTimeConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.IsoDateTimeConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).DateTimeFormat : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).DateTimeStyles : 
                System.Globalization.DateTimeStyles

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).get_DateTimeFormat : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).get_DateTimeStyles : 
                System.Void
                -> System.Globalization.DateTimeStyles

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).set_DateTimeFormat : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.IsoDateTimeConverter).set_DateTimeStyles : 
                value:System.Globalization.DateTimeStyles
                -> System.Void

        
* Newtonsoft.Json.Converters.JavaScriptDateTimeConverter

        Newtonsoft.Json.Converters.JavaScriptDateTimeConverter (.NET type: Class and base: Newtonsoft.Json.Converters.DateTimeConverterBase)

        new Newtonsoft.Json.Converters.JavaScriptDateTimeConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.JavaScriptDateTimeConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.JavaScriptDateTimeConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.KeyValuePairConverter

        Newtonsoft.Json.Converters.KeyValuePairConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.KeyValuePairConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.KeyValuePairConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.KeyValuePairConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.RegexConverter

        Newtonsoft.Json.Converters.RegexConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.RegexConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.RegexConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.RegexConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.StringEnumConverter

        Newtonsoft.Json.Converters.StringEnumConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.StringEnumConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.StringEnumConverter

        new Newtonsoft.Json.Converters.StringEnumConverter : 
                camelCaseText:System.Boolean
                -> Newtonsoft.Json.Converters.StringEnumConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).AllowIntegerValues : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).CamelCaseText : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).get_AllowIntegerValues : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).get_CamelCaseText : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).set_AllowIntegerValues : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.StringEnumConverter).set_CamelCaseText : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Converters.VersionConverter

        Newtonsoft.Json.Converters.VersionConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.VersionConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.VersionConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.VersionConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Converters.XmlNodeConverter

        Newtonsoft.Json.Converters.XmlNodeConverter (.NET type: Class and base: Newtonsoft.Json.JsonConverter)

        new Newtonsoft.Json.Converters.XmlNodeConverter : 
                System.Void
                -> Newtonsoft.Json.Converters.XmlNodeConverter

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).CanConvert : 
                valueType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).DeserializeRootElementName : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).OmitRootObject : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).WriteArrayAttribute : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).get_DeserializeRootElementName : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).get_OmitRootObject : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).get_WriteArrayAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).set_DeserializeRootElementName : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).set_OmitRootObject : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Converters.XmlNodeConverter).set_WriteArrayAttribute : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.DateFormatHandling

        Newtonsoft.Json.DateFormatHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.DateFormatHandling values: [ IsoDateFormat:0; MicrosoftDateFormat:1 ]

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateFormatHandling).value__ : 
                System.Int32

        Newtonsoft.Json.DateFormatHandling.IsoDateFormat : 
                Newtonsoft.Json.DateFormatHandling

        Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat : 
                Newtonsoft.Json.DateFormatHandling

        
* Newtonsoft.Json.DateParseHandling

        Newtonsoft.Json.DateParseHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.DateParseHandling values: [ DateTime:1; DateTimeOffset:2; None:0 ]

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateParseHandling).value__ : 
                System.Int32

        Newtonsoft.Json.DateParseHandling.DateTime : 
                Newtonsoft.Json.DateParseHandling

        Newtonsoft.Json.DateParseHandling.DateTimeOffset : 
                Newtonsoft.Json.DateParseHandling

        Newtonsoft.Json.DateParseHandling.None : 
                Newtonsoft.Json.DateParseHandling

        
* Newtonsoft.Json.DateTimeZoneHandling

        Newtonsoft.Json.DateTimeZoneHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.DateTimeZoneHandling values: [ Local:0; RoundtripKind:3; Unspecified:2; Utc:1 ]

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DateTimeZoneHandling).value__ : 
                System.Int32

        Newtonsoft.Json.DateTimeZoneHandling.Local : 
                Newtonsoft.Json.DateTimeZoneHandling

        Newtonsoft.Json.DateTimeZoneHandling.RoundtripKind : 
                Newtonsoft.Json.DateTimeZoneHandling

        Newtonsoft.Json.DateTimeZoneHandling.Unspecified : 
                Newtonsoft.Json.DateTimeZoneHandling

        Newtonsoft.Json.DateTimeZoneHandling.Utc : 
                Newtonsoft.Json.DateTimeZoneHandling

        
* Newtonsoft.Json.DefaultValueHandling

        Newtonsoft.Json.DefaultValueHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.DefaultValueHandling values: [ Ignore:1; IgnoreAndPopulate:3; Include:0; Populate:2 ]

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.DefaultValueHandling).value__ : 
                System.Int32

        Newtonsoft.Json.DefaultValueHandling.Ignore : 
                Newtonsoft.Json.DefaultValueHandling

        Newtonsoft.Json.DefaultValueHandling.IgnoreAndPopulate : 
                Newtonsoft.Json.DefaultValueHandling

        Newtonsoft.Json.DefaultValueHandling.Include : 
                Newtonsoft.Json.DefaultValueHandling

        Newtonsoft.Json.DefaultValueHandling.Populate : 
                Newtonsoft.Json.DefaultValueHandling

        
* Newtonsoft.Json.FloatFormatHandling

        Newtonsoft.Json.FloatFormatHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.FloatFormatHandling values: [ DefaultValue:2; String:0; Symbol:1 ]

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.FloatFormatHandling).value__ : 
                System.Int32

        Newtonsoft.Json.FloatFormatHandling.DefaultValue : 
                Newtonsoft.Json.FloatFormatHandling

        Newtonsoft.Json.FloatFormatHandling.String : 
                Newtonsoft.Json.FloatFormatHandling

        Newtonsoft.Json.FloatFormatHandling.Symbol : 
                Newtonsoft.Json.FloatFormatHandling

        
* Newtonsoft.Json.FloatParseHandling

        Newtonsoft.Json.FloatParseHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.FloatParseHandling values: [ Decimal:1; Double:0 ]

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.FloatParseHandling).value__ : 
                System.Int32

        Newtonsoft.Json.FloatParseHandling.Decimal : 
                Newtonsoft.Json.FloatParseHandling

        Newtonsoft.Json.FloatParseHandling.Double : 
                Newtonsoft.Json.FloatParseHandling

        
* Newtonsoft.Json.Formatting

        Newtonsoft.Json.Formatting (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.Formatting values: [ Indented:1; None:0 ]

        (Instance/Inheritance of Newtonsoft.Json.Formatting).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Formatting).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Formatting).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Formatting).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Formatting).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.Formatting).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Formatting).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Formatting).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Formatting).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Formatting).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Formatting).value__ : 
                System.Int32

        Newtonsoft.Json.Formatting.Indented : 
                Newtonsoft.Json.Formatting

        Newtonsoft.Json.Formatting.None : 
                Newtonsoft.Json.Formatting

        
* Newtonsoft.Json.IArrayPool<'T>

        Newtonsoft.Json.IArrayPool<'T> (.NET type: Interface and base: none)

        (Instance/Inheritance of Newtonsoft.Json.IArrayPool<'T>).Rent : 
                minimumLength:System.Int32
                -> T[]

        (Instance/Inheritance of Newtonsoft.Json.IArrayPool<'T>).Return : 
                array:T[]
                -> System.Void

        
* Newtonsoft.Json.IJsonLineInfo

        Newtonsoft.Json.IJsonLineInfo (.NET type: Interface and base: none)

        (Instance/Inheritance of Newtonsoft.Json.IJsonLineInfo).HasLineInfo : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.IJsonLineInfo).LineNumber : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.IJsonLineInfo).LinePosition : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.IJsonLineInfo).get_LineNumber : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.IJsonLineInfo).get_LinePosition : 
                System.Void
                -> System.Int32

        
* Newtonsoft.Json.JsonArrayAttribute

        Newtonsoft.Json.JsonArrayAttribute (.NET type: Class and base: Newtonsoft.Json.JsonContainerAttribute)

        new Newtonsoft.Json.JsonArrayAttribute : 
                System.Void
                -> Newtonsoft.Json.JsonArrayAttribute

        new Newtonsoft.Json.JsonArrayAttribute : 
                allowNullItems:System.Boolean
                -> Newtonsoft.Json.JsonArrayAttribute

        new Newtonsoft.Json.JsonArrayAttribute : 
                id:System.String
                -> Newtonsoft.Json.JsonArrayAttribute

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).AllowNullItems : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).Description : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).Id : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).IsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).ItemConverterParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).ItemConverterType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).ItemIsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).ItemReferenceLoopHandling : 
                Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).ItemTypeNameHandling : 
                Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).NamingStrategyParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).NamingStrategyType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).Title : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_AllowNullItems : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_Description : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_Id : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_IsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_ItemConverterParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_ItemConverterType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_ItemIsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_ItemReferenceLoopHandling : 
                System.Void
                -> Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_ItemTypeNameHandling : 
                System.Void
                -> Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_Title : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_TypeId : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_AllowNullItems : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_Description : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_Id : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_IsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_ItemConverterParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_ItemConverterType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_ItemIsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_ItemReferenceLoopHandling : 
                value:Newtonsoft.Json.ReferenceLoopHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_ItemTypeNameHandling : 
                value:Newtonsoft.Json.TypeNameHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_Title : 
                value:System.String
                -> System.Void

        
* Newtonsoft.Json.JsonConstructorAttribute

        Newtonsoft.Json.JsonConstructorAttribute (.NET type: Class and base: System.Attribute)

        new Newtonsoft.Json.JsonConstructorAttribute : 
                System.Void
                -> Newtonsoft.Json.JsonConstructorAttribute

        (Instance/Inheritance of Newtonsoft.Json.JsonConstructorAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConstructorAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonConstructorAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonConstructorAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConstructorAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConstructorAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonConstructorAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonConstructorAttribute).get_TypeId : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.JsonContainerAttribute

        Newtonsoft.Json.JsonContainerAttribute (.NET type: Abstract and base: System.Attribute)

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).Description : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).Id : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).IsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).ItemConverterParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).ItemConverterType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).ItemIsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).ItemReferenceLoopHandling : 
                Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).ItemTypeNameHandling : 
                Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).NamingStrategyParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).NamingStrategyType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).Title : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_Description : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_Id : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_IsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_ItemConverterParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_ItemConverterType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_ItemIsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_ItemReferenceLoopHandling : 
                System.Void
                -> Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_ItemTypeNameHandling : 
                System.Void
                -> Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_Title : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_TypeId : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_Description : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_Id : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_IsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_ItemConverterParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_ItemConverterType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_ItemIsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_ItemReferenceLoopHandling : 
                value:Newtonsoft.Json.ReferenceLoopHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_ItemTypeNameHandling : 
                value:Newtonsoft.Json.TypeNameHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_Title : 
                value:System.String
                -> System.Void

        
* Newtonsoft.Json.JsonConvert

        Newtonsoft.Json.JsonConvert (.NET type: Static and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.JsonConvert).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConvert).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonConvert).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonConvert).ToString : 
                System.Void
                -> System.String

        Newtonsoft.Json.JsonConvert.DefaultSettings : 
                System.Func<Newtonsoft.Json.JsonSerializerSettings>

        Newtonsoft.Json.JsonConvert.DeserializeAnonymousType : 
                value:System.String * anonymousTypeObject:'T * settings:Newtonsoft.Json.JsonSerializerSettings
                -> 'T

        Newtonsoft.Json.JsonConvert.DeserializeAnonymousType : 
                value:System.String * anonymousTypeObject:'T
                -> 'T

        Newtonsoft.Json.JsonConvert.DeserializeObject : 
                value:System.String * converters:Newtonsoft.Json.JsonConverter[]
                -> 'T

        Newtonsoft.Json.JsonConvert.DeserializeObject : 
                value:System.String * settings:Newtonsoft.Json.JsonSerializerSettings
                -> 'T

        Newtonsoft.Json.JsonConvert.DeserializeObject : 
                value:System.String * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.Object

        Newtonsoft.Json.JsonConvert.DeserializeObject : 
                value:System.String * type:System.Type * converters:Newtonsoft.Json.JsonConverter[]
                -> System.Object

        Newtonsoft.Json.JsonConvert.DeserializeObject : 
                value:System.String * type:System.Type * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.Object

        Newtonsoft.Json.JsonConvert.DeserializeObject : 
                value:System.String * type:System.Type
                -> System.Object

        Newtonsoft.Json.JsonConvert.DeserializeObject : 
                value:System.String
                -> 'T

        Newtonsoft.Json.JsonConvert.DeserializeObject : 
                value:System.String
                -> System.Object

        Newtonsoft.Json.JsonConvert.DeserializeObjectAsync : 
                value:System.String * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.Threading.Tasks.Task<'T>

        Newtonsoft.Json.JsonConvert.DeserializeObjectAsync : 
                value:System.String * type:System.Type * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.Threading.Tasks.Task<System.Object>

        Newtonsoft.Json.JsonConvert.DeserializeObjectAsync : 
                value:System.String
                -> System.Threading.Tasks.Task<'T>

        Newtonsoft.Json.JsonConvert.DeserializeObjectAsync : 
                value:System.String
                -> System.Threading.Tasks.Task<System.Object>

        Newtonsoft.Json.JsonConvert.DeserializeXNode : 
                value:System.String * deserializeRootElementName:System.String * writeArrayAttribute:System.Boolean
                -> System.Xml.Linq.XDocument

        Newtonsoft.Json.JsonConvert.DeserializeXNode : 
                value:System.String * deserializeRootElementName:System.String
                -> System.Xml.Linq.XDocument

        Newtonsoft.Json.JsonConvert.DeserializeXNode : 
                value:System.String
                -> System.Xml.Linq.XDocument

        Newtonsoft.Json.JsonConvert.DeserializeXmlNode : 
                value:System.String * deserializeRootElementName:System.String * writeArrayAttribute:System.Boolean
                -> System.Xml.XmlDocument

        Newtonsoft.Json.JsonConvert.DeserializeXmlNode : 
                value:System.String * deserializeRootElementName:System.String
                -> System.Xml.XmlDocument

        Newtonsoft.Json.JsonConvert.DeserializeXmlNode : 
                value:System.String
                -> System.Xml.XmlDocument

        Newtonsoft.Json.JsonConvert.False : 
                System.String

        Newtonsoft.Json.JsonConvert.NaN : 
                System.String

        Newtonsoft.Json.JsonConvert.NegativeInfinity : 
                System.String

        Newtonsoft.Json.JsonConvert.Null : 
                System.String

        Newtonsoft.Json.JsonConvert.PopulateObject : 
                value:System.String * target:System.Object * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.Void

        Newtonsoft.Json.JsonConvert.PopulateObject : 
                value:System.String * target:System.Object
                -> System.Void

        Newtonsoft.Json.JsonConvert.PopulateObjectAsync : 
                value:System.String * target:System.Object * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.Threading.Tasks.Task

        Newtonsoft.Json.JsonConvert.PositiveInfinity : 
                System.String

        Newtonsoft.Json.JsonConvert.SerializeObject : 
                value:System.Object * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeObject : 
                value:System.Object * formatting:Newtonsoft.Json.Formatting * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeObject : 
                value:System.Object * formatting:Newtonsoft.Json.Formatting * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeObject : 
                value:System.Object * formatting:Newtonsoft.Json.Formatting
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeObject : 
                value:System.Object * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeObject : 
                value:System.Object * type:System.Type * formatting:Newtonsoft.Json.Formatting * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeObject : 
                value:System.Object * type:System.Type * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeObject : 
                value:System.Object
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeObjectAsync : 
                value:System.Object * formatting:Newtonsoft.Json.Formatting * settings:Newtonsoft.Json.JsonSerializerSettings
                -> System.Threading.Tasks.Task<System.String>

        Newtonsoft.Json.JsonConvert.SerializeObjectAsync : 
                value:System.Object * formatting:Newtonsoft.Json.Formatting
                -> System.Threading.Tasks.Task<System.String>

        Newtonsoft.Json.JsonConvert.SerializeObjectAsync : 
                value:System.Object
                -> System.Threading.Tasks.Task<System.String>

        Newtonsoft.Json.JsonConvert.SerializeXNode : 
                node:System.Xml.Linq.XObject * formatting:Newtonsoft.Json.Formatting * omitRootObject:System.Boolean
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeXNode : 
                node:System.Xml.Linq.XObject * formatting:Newtonsoft.Json.Formatting
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeXNode : 
                node:System.Xml.Linq.XObject
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeXmlNode : 
                node:System.Xml.XmlNode * formatting:Newtonsoft.Json.Formatting * omitRootObject:System.Boolean
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeXmlNode : 
                node:System.Xml.XmlNode * formatting:Newtonsoft.Json.Formatting
                -> System.String

        Newtonsoft.Json.JsonConvert.SerializeXmlNode : 
                node:System.Xml.XmlNode
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Boolean
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Byte
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Char
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.DateTime * format:Newtonsoft.Json.DateFormatHandling * timeZoneHandling:Newtonsoft.Json.DateTimeZoneHandling
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.DateTime
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.DateTimeOffset * format:Newtonsoft.Json.DateFormatHandling
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.DateTimeOffset
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Decimal
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Double
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Enum
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Guid
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Int16
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Int32
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Int64
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Object
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.SByte
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Single
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.String * delimiter:System.Char * stringEscapeHandling:Newtonsoft.Json.StringEscapeHandling
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.String * delimiter:System.Char
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.String
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.TimeSpan
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.UInt16
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.UInt32
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.UInt64
                -> System.String

        Newtonsoft.Json.JsonConvert.ToString : 
                value:System.Uri
                -> System.String

        Newtonsoft.Json.JsonConvert.True : 
                System.String

        Newtonsoft.Json.JsonConvert.Undefined : 
                System.String

        Newtonsoft.Json.JsonConvert.get_DefaultSettings : 
                System.Void
                -> System.Func<Newtonsoft.Json.JsonSerializerSettings>

        Newtonsoft.Json.JsonConvert.set_DefaultSettings : 
                value:System.Func<Newtonsoft.Json.JsonSerializerSettings>
                -> System.Void

        
* Newtonsoft.Json.JsonConverter

        Newtonsoft.Json.JsonConverter (.NET type: Abstract and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).CanConvert : 
                objectType:System.Type
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).CanRead : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).CanWrite : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).GetSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).ReadJson : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type * existingValue:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).WriteJson : 
                writer:Newtonsoft.Json.JsonWriter * value:System.Object * serializer:Newtonsoft.Json.JsonSerializer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).get_CanRead : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverter).get_CanWrite : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.JsonConverterAttribute

        Newtonsoft.Json.JsonConverterAttribute (.NET type: Class and base: System.Attribute)

        new Newtonsoft.Json.JsonConverterAttribute : 
                converterType:System.Type * converterParameters:System.Object[]
                -> Newtonsoft.Json.JsonConverterAttribute

        new Newtonsoft.Json.JsonConverterAttribute : 
                converterType:System.Type
                -> Newtonsoft.Json.JsonConverterAttribute

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).ConverterParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).ConverterType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).get_ConverterParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).get_ConverterType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterAttribute).get_TypeId : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.JsonConverterCollection

        Newtonsoft.Json.JsonConverterCollection (.NET type: Class and base: System.Collections.ObjectModel.Collection<Newtonsoft.Json.JsonConverter>)

        new Newtonsoft.Json.JsonConverterCollection : 
                System.Void
                -> Newtonsoft.Json.JsonConverterCollection

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).Add : 
                item:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).Clear : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).Contains : 
                item:Newtonsoft.Json.JsonConverter
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).CopyTo : 
                array:Newtonsoft.Json.JsonConverter[] * index:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).Count : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).GetEnumerator : 
                System.Void
                -> System.Collections.Generic.IEnumerator<Newtonsoft.Json.JsonConverter>

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).IndexOf : 
                item:Newtonsoft.Json.JsonConverter
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).Insert : 
                index:System.Int32 * item:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).Item : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).Remove : 
                item:Newtonsoft.Json.JsonConverter
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).RemoveAt : 
                index:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).get_Count : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).get_Item : 
                index:System.Int32
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.JsonConverterCollection).set_Item : 
                index:System.Int32 * value:Newtonsoft.Json.JsonConverter
                -> System.Void

        
* Newtonsoft.Json.JsonDictionaryAttribute

        Newtonsoft.Json.JsonDictionaryAttribute (.NET type: Class and base: Newtonsoft.Json.JsonContainerAttribute)

        new Newtonsoft.Json.JsonDictionaryAttribute : 
                System.Void
                -> Newtonsoft.Json.JsonDictionaryAttribute

        new Newtonsoft.Json.JsonDictionaryAttribute : 
                id:System.String
                -> Newtonsoft.Json.JsonDictionaryAttribute

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).Description : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).Id : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).IsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).ItemConverterParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).ItemConverterType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).ItemIsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).ItemReferenceLoopHandling : 
                Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).ItemTypeNameHandling : 
                Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).NamingStrategyParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).NamingStrategyType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).Title : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_Description : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_Id : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_IsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_ItemConverterParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_ItemConverterType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_ItemIsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_ItemReferenceLoopHandling : 
                System.Void
                -> Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_ItemTypeNameHandling : 
                System.Void
                -> Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_Title : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_TypeId : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_Description : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_Id : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_IsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_ItemConverterParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_ItemConverterType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_ItemIsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_ItemReferenceLoopHandling : 
                value:Newtonsoft.Json.ReferenceLoopHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_ItemTypeNameHandling : 
                value:Newtonsoft.Json.TypeNameHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_Title : 
                value:System.String
                -> System.Void

        
* Newtonsoft.Json.JsonException

        Newtonsoft.Json.JsonException (.NET type: Class and base: System.Exception)

        new Newtonsoft.Json.JsonException : 
                System.Void
                -> Newtonsoft.Json.JsonException

        new Newtonsoft.Json.JsonException : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> Newtonsoft.Json.JsonException

        new Newtonsoft.Json.JsonException : 
                message:System.String * innerException:System.Exception
                -> Newtonsoft.Json.JsonException

        new Newtonsoft.Json.JsonException : 
                message:System.String
                -> Newtonsoft.Json.JsonException

        (Instance/Inheritance of Newtonsoft.Json.JsonException).Data : 
                System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.JsonException).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonException).GetBaseException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonException).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonException).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonException).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonException).HResult : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonException).HelpLink : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonException).InnerException : 
                System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonException).Message : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonException).Source : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonException).StackTrace : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonException).TargetSite : 
                System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.JsonException).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonException).get_Data : 
                System.Void
                -> System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.JsonException).get_HResult : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonException).get_HelpLink : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonException).get_InnerException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonException).get_Message : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonException).get_Source : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonException).get_StackTrace : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonException).get_TargetSite : 
                System.Void
                -> System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.JsonException).set_HelpLink : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonException).set_Source : 
                value:System.String
                -> System.Void

        
* Newtonsoft.Json.JsonExtensionDataAttribute

        Newtonsoft.Json.JsonExtensionDataAttribute (.NET type: Class and base: System.Attribute)

        new Newtonsoft.Json.JsonExtensionDataAttribute : 
                System.Void
                -> Newtonsoft.Json.JsonExtensionDataAttribute

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).ReadData : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).WriteData : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).get_ReadData : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).get_TypeId : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).get_WriteData : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).set_ReadData : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonExtensionDataAttribute).set_WriteData : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.JsonIgnoreAttribute

        Newtonsoft.Json.JsonIgnoreAttribute (.NET type: Class and base: System.Attribute)

        new Newtonsoft.Json.JsonIgnoreAttribute : 
                System.Void
                -> Newtonsoft.Json.JsonIgnoreAttribute

        (Instance/Inheritance of Newtonsoft.Json.JsonIgnoreAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonIgnoreAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonIgnoreAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonIgnoreAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonIgnoreAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonIgnoreAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonIgnoreAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonIgnoreAttribute).get_TypeId : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.JsonObjectAttribute

        Newtonsoft.Json.JsonObjectAttribute (.NET type: Class and base: Newtonsoft.Json.JsonContainerAttribute)

        new Newtonsoft.Json.JsonObjectAttribute : 
                System.Void
                -> Newtonsoft.Json.JsonObjectAttribute

        new Newtonsoft.Json.JsonObjectAttribute : 
                id:System.String
                -> Newtonsoft.Json.JsonObjectAttribute

        new Newtonsoft.Json.JsonObjectAttribute : 
                memberSerialization:Newtonsoft.Json.MemberSerialization
                -> Newtonsoft.Json.JsonObjectAttribute

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).Description : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).Id : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).IsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).ItemConverterParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).ItemConverterType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).ItemIsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).ItemReferenceLoopHandling : 
                Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).ItemRequired : 
                Newtonsoft.Json.Required

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).ItemTypeNameHandling : 
                Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).MemberSerialization : 
                Newtonsoft.Json.MemberSerialization

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).NamingStrategyParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).NamingStrategyType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).Title : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_Description : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_Id : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_IsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_ItemConverterParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_ItemConverterType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_ItemIsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_ItemReferenceLoopHandling : 
                System.Void
                -> Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_ItemRequired : 
                System.Void
                -> Newtonsoft.Json.Required

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_ItemTypeNameHandling : 
                System.Void
                -> Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_MemberSerialization : 
                System.Void
                -> Newtonsoft.Json.MemberSerialization

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_Title : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_TypeId : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_Description : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_Id : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_IsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_ItemConverterParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_ItemConverterType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_ItemIsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_ItemReferenceLoopHandling : 
                value:Newtonsoft.Json.ReferenceLoopHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_ItemRequired : 
                value:Newtonsoft.Json.Required
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_ItemTypeNameHandling : 
                value:Newtonsoft.Json.TypeNameHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_MemberSerialization : 
                value:Newtonsoft.Json.MemberSerialization
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_Title : 
                value:System.String
                -> System.Void

        
* Newtonsoft.Json.JsonPropertyAttribute

        Newtonsoft.Json.JsonPropertyAttribute (.NET type: Class and base: System.Attribute)

        new Newtonsoft.Json.JsonPropertyAttribute : 
                System.Void
                -> Newtonsoft.Json.JsonPropertyAttribute

        new Newtonsoft.Json.JsonPropertyAttribute : 
                propertyName:System.String
                -> Newtonsoft.Json.JsonPropertyAttribute

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).DefaultValueHandling : 
                Newtonsoft.Json.DefaultValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).IsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).ItemConverterParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).ItemConverterType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).ItemIsReference : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).ItemReferenceLoopHandling : 
                Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).ItemTypeNameHandling : 
                Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).NamingStrategyParameters : 
                System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).NamingStrategyType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).NullValueHandling : 
                Newtonsoft.Json.NullValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).ObjectCreationHandling : 
                Newtonsoft.Json.ObjectCreationHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).Order : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).PropertyName : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).ReferenceLoopHandling : 
                Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).Required : 
                Newtonsoft.Json.Required

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).TypeNameHandling : 
                Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_DefaultValueHandling : 
                System.Void
                -> Newtonsoft.Json.DefaultValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_IsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_ItemConverterParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_ItemConverterType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_ItemIsReference : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_ItemReferenceLoopHandling : 
                System.Void
                -> Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_ItemTypeNameHandling : 
                System.Void
                -> Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_NullValueHandling : 
                System.Void
                -> Newtonsoft.Json.NullValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_ObjectCreationHandling : 
                System.Void
                -> Newtonsoft.Json.ObjectCreationHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_Order : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_PropertyName : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_ReferenceLoopHandling : 
                System.Void
                -> Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_Required : 
                System.Void
                -> Newtonsoft.Json.Required

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_TypeId : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_TypeNameHandling : 
                System.Void
                -> Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_DefaultValueHandling : 
                value:Newtonsoft.Json.DefaultValueHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_IsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_ItemConverterParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_ItemConverterType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_ItemIsReference : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_ItemReferenceLoopHandling : 
                value:Newtonsoft.Json.ReferenceLoopHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_ItemTypeNameHandling : 
                value:Newtonsoft.Json.TypeNameHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_NullValueHandling : 
                value:Newtonsoft.Json.NullValueHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_ObjectCreationHandling : 
                value:Newtonsoft.Json.ObjectCreationHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_Order : 
                value:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_PropertyName : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_ReferenceLoopHandling : 
                value:Newtonsoft.Json.ReferenceLoopHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_Required : 
                value:Newtonsoft.Json.Required
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_TypeNameHandling : 
                value:Newtonsoft.Json.TypeNameHandling
                -> System.Void

        
* Newtonsoft.Json.JsonReader

        Newtonsoft.Json.JsonReader (.NET type: Abstract and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).Close : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).CloseInput : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).DateParseHandling : 
                Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).Depth : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).FloatParseHandling : 
                Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).MaxDepth : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).QuoteChar : 
                System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).Read : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsBytes : 
                System.Void
                -> System.Byte[]

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsDateTime : 
                System.Void
                -> System.Nullable<System.DateTime>

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsDateTimeOffset : 
                System.Void
                -> System.Nullable<System.DateTimeOffset>

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsDecimal : 
                System.Void
                -> System.Nullable<System.Decimal>

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsInt32 : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).Skip : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).SupportMultipleContent : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).TokenType : 
                Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).Value : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).ValueType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_CloseInput : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_DateParseHandling : 
                System.Void
                -> Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_Depth : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_FloatParseHandling : 
                System.Void
                -> Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_MaxDepth : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_QuoteChar : 
                System.Void
                -> System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_SupportMultipleContent : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_TokenType : 
                System.Void
                -> Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_Value : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).get_ValueType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).set_CloseInput : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).set_DateParseHandling : 
                value:Newtonsoft.Json.DateParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).set_FloatParseHandling : 
                value:Newtonsoft.Json.FloatParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).set_MaxDepth : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReader).set_SupportMultipleContent : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.JsonReaderException

        Newtonsoft.Json.JsonReaderException (.NET type: Class and base: Newtonsoft.Json.JsonException)

        new Newtonsoft.Json.JsonReaderException : 
                System.Void
                -> Newtonsoft.Json.JsonReaderException

        new Newtonsoft.Json.JsonReaderException : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> Newtonsoft.Json.JsonReaderException

        new Newtonsoft.Json.JsonReaderException : 
                message:System.String * innerException:System.Exception
                -> Newtonsoft.Json.JsonReaderException

        new Newtonsoft.Json.JsonReaderException : 
                message:System.String
                -> Newtonsoft.Json.JsonReaderException

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).Data : 
                System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).GetBaseException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).HResult : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).HelpLink : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).InnerException : 
                System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).LineNumber : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).LinePosition : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).Message : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).Source : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).StackTrace : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).TargetSite : 
                System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_Data : 
                System.Void
                -> System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_HResult : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_HelpLink : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_InnerException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_LineNumber : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_LinePosition : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_Message : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_Source : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_StackTrace : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).get_TargetSite : 
                System.Void
                -> System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).set_HelpLink : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonReaderException).set_Source : 
                value:System.String
                -> System.Void

        
* Newtonsoft.Json.JsonRequiredAttribute

        Newtonsoft.Json.JsonRequiredAttribute (.NET type: Class and base: System.Attribute)

        new Newtonsoft.Json.JsonRequiredAttribute : 
                System.Void
                -> Newtonsoft.Json.JsonRequiredAttribute

        (Instance/Inheritance of Newtonsoft.Json.JsonRequiredAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonRequiredAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonRequiredAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonRequiredAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonRequiredAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonRequiredAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonRequiredAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonRequiredAttribute).get_TypeId : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.JsonSerializationException

        Newtonsoft.Json.JsonSerializationException (.NET type: Class and base: Newtonsoft.Json.JsonException)

        new Newtonsoft.Json.JsonSerializationException : 
                System.Void
                -> Newtonsoft.Json.JsonSerializationException

        new Newtonsoft.Json.JsonSerializationException : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> Newtonsoft.Json.JsonSerializationException

        new Newtonsoft.Json.JsonSerializationException : 
                message:System.String * innerException:System.Exception
                -> Newtonsoft.Json.JsonSerializationException

        new Newtonsoft.Json.JsonSerializationException : 
                message:System.String
                -> Newtonsoft.Json.JsonSerializationException

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).Data : 
                System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).GetBaseException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).HResult : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).HelpLink : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).InnerException : 
                System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).Message : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).Source : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).StackTrace : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).TargetSite : 
                System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).get_Data : 
                System.Void
                -> System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).get_HResult : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).get_HelpLink : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).get_InnerException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).get_Message : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).get_Source : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).get_StackTrace : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).get_TargetSite : 
                System.Void
                -> System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).set_HelpLink : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializationException).set_Source : 
                value:System.String
                -> System.Void

        
* Newtonsoft.Json.JsonSerializer

        Newtonsoft.Json.JsonSerializer (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.JsonSerializer : 
                System.Void
                -> Newtonsoft.Json.JsonSerializer

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Binder : 
                System.Runtime.Serialization.SerializationBinder

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).CheckAdditionalContent : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).ConstructorHandling : 
                Newtonsoft.Json.ConstructorHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Context : 
                System.Runtime.Serialization.StreamingContext

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).ContractResolver : 
                Newtonsoft.Json.Serialization.IContractResolver

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Converters : 
                Newtonsoft.Json.JsonConverterCollection

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).DateFormatHandling : 
                Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).DateParseHandling : 
                Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).DefaultValueHandling : 
                Newtonsoft.Json.DefaultValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Deserialize : 
                reader:Newtonsoft.Json.JsonReader * objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Deserialize : 
                reader:Newtonsoft.Json.JsonReader
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Deserialize : 
                reader:Newtonsoft.Json.JsonReader
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Deserialize : 
                reader:System.IO.TextReader * objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).EqualityComparer : 
                System.Collections.IEqualityComparer

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Error.Add : 
                sender:System.Object * e:Newtonsoft.Json.Serialization.ErrorEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).FloatFormatHandling : 
                Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).FloatParseHandling : 
                Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Formatting : 
                Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).MaxDepth : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).MetadataPropertyHandling : 
                Newtonsoft.Json.MetadataPropertyHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).MissingMemberHandling : 
                Newtonsoft.Json.MissingMemberHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).NullValueHandling : 
                Newtonsoft.Json.NullValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).ObjectCreationHandling : 
                Newtonsoft.Json.ObjectCreationHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Populate : 
                reader:Newtonsoft.Json.JsonReader * target:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Populate : 
                reader:System.IO.TextReader * target:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).PreserveReferencesHandling : 
                Newtonsoft.Json.PreserveReferencesHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).ReferenceLoopHandling : 
                Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).ReferenceResolver : 
                Newtonsoft.Json.Serialization.IReferenceResolver

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Serialize : 
                jsonWriter:Newtonsoft.Json.JsonWriter * value:System.Object * objectType:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Serialize : 
                jsonWriter:Newtonsoft.Json.JsonWriter * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Serialize : 
                textWriter:System.IO.TextWriter * value:System.Object * objectType:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).Serialize : 
                textWriter:System.IO.TextWriter * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).StringEscapeHandling : 
                Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).TraceWriter : 
                Newtonsoft.Json.Serialization.ITraceWriter

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).TypeNameAssemblyFormat : 
                System.Runtime.Serialization.Formatters.FormatterAssemblyStyle

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).TypeNameHandling : 
                Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).add_Error : 
                value:System.EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_Binder : 
                System.Void
                -> System.Runtime.Serialization.SerializationBinder

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_CheckAdditionalContent : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_ConstructorHandling : 
                System.Void
                -> Newtonsoft.Json.ConstructorHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_Context : 
                System.Void
                -> System.Runtime.Serialization.StreamingContext

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_ContractResolver : 
                System.Void
                -> Newtonsoft.Json.Serialization.IContractResolver

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_Converters : 
                System.Void
                -> Newtonsoft.Json.JsonConverterCollection

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_DateFormatHandling : 
                System.Void
                -> Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_DateParseHandling : 
                System.Void
                -> Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_DefaultValueHandling : 
                System.Void
                -> Newtonsoft.Json.DefaultValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_EqualityComparer : 
                System.Void
                -> System.Collections.IEqualityComparer

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_FloatFormatHandling : 
                System.Void
                -> Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_FloatParseHandling : 
                System.Void
                -> Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_Formatting : 
                System.Void
                -> Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_MaxDepth : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_MetadataPropertyHandling : 
                System.Void
                -> Newtonsoft.Json.MetadataPropertyHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_MissingMemberHandling : 
                System.Void
                -> Newtonsoft.Json.MissingMemberHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_NullValueHandling : 
                System.Void
                -> Newtonsoft.Json.NullValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_ObjectCreationHandling : 
                System.Void
                -> Newtonsoft.Json.ObjectCreationHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_PreserveReferencesHandling : 
                System.Void
                -> Newtonsoft.Json.PreserveReferencesHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_ReferenceLoopHandling : 
                System.Void
                -> Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_ReferenceResolver : 
                System.Void
                -> Newtonsoft.Json.Serialization.IReferenceResolver

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_StringEscapeHandling : 
                System.Void
                -> Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_TraceWriter : 
                System.Void
                -> Newtonsoft.Json.Serialization.ITraceWriter

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_TypeNameAssemblyFormat : 
                System.Void
                -> System.Runtime.Serialization.Formatters.FormatterAssemblyStyle

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).get_TypeNameHandling : 
                System.Void
                -> Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).remove_Error : 
                value:System.EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_Binder : 
                value:System.Runtime.Serialization.SerializationBinder
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_CheckAdditionalContent : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_ConstructorHandling : 
                value:Newtonsoft.Json.ConstructorHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_Context : 
                value:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_ContractResolver : 
                value:Newtonsoft.Json.Serialization.IContractResolver
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_DateFormatHandling : 
                value:Newtonsoft.Json.DateFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_DateParseHandling : 
                value:Newtonsoft.Json.DateParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_DefaultValueHandling : 
                value:Newtonsoft.Json.DefaultValueHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_EqualityComparer : 
                value:System.Collections.IEqualityComparer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_FloatFormatHandling : 
                value:Newtonsoft.Json.FloatFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_FloatParseHandling : 
                value:Newtonsoft.Json.FloatParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_Formatting : 
                value:Newtonsoft.Json.Formatting
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_MaxDepth : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_MetadataPropertyHandling : 
                value:Newtonsoft.Json.MetadataPropertyHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_MissingMemberHandling : 
                value:Newtonsoft.Json.MissingMemberHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_NullValueHandling : 
                value:Newtonsoft.Json.NullValueHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_ObjectCreationHandling : 
                value:Newtonsoft.Json.ObjectCreationHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_PreserveReferencesHandling : 
                value:Newtonsoft.Json.PreserveReferencesHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_ReferenceLoopHandling : 
                value:Newtonsoft.Json.ReferenceLoopHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_ReferenceResolver : 
                value:Newtonsoft.Json.Serialization.IReferenceResolver
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_StringEscapeHandling : 
                value:Newtonsoft.Json.StringEscapeHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_TraceWriter : 
                value:Newtonsoft.Json.Serialization.ITraceWriter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_TypeNameAssemblyFormat : 
                value:System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializer).set_TypeNameHandling : 
                value:Newtonsoft.Json.TypeNameHandling
                -> System.Void

        Newtonsoft.Json.JsonSerializer.Create : 
                System.Void
                -> Newtonsoft.Json.JsonSerializer

        Newtonsoft.Json.JsonSerializer.Create : 
                settings:Newtonsoft.Json.JsonSerializerSettings
                -> Newtonsoft.Json.JsonSerializer

        Newtonsoft.Json.JsonSerializer.CreateDefault : 
                System.Void
                -> Newtonsoft.Json.JsonSerializer

        Newtonsoft.Json.JsonSerializer.CreateDefault : 
                settings:Newtonsoft.Json.JsonSerializerSettings
                -> Newtonsoft.Json.JsonSerializer

        
* Newtonsoft.Json.JsonSerializerSettings

        Newtonsoft.Json.JsonSerializerSettings (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.JsonSerializerSettings : 
                System.Void
                -> Newtonsoft.Json.JsonSerializerSettings

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).Binder : 
                System.Runtime.Serialization.SerializationBinder

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).CheckAdditionalContent : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).ConstructorHandling : 
                Newtonsoft.Json.ConstructorHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).Context : 
                System.Runtime.Serialization.StreamingContext

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).ContractResolver : 
                Newtonsoft.Json.Serialization.IContractResolver

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).Converters : 
                System.Collections.Generic.IList<Newtonsoft.Json.JsonConverter>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).DateFormatHandling : 
                Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).DateParseHandling : 
                Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).DefaultValueHandling : 
                Newtonsoft.Json.DefaultValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).EqualityComparer : 
                System.Collections.IEqualityComparer

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).Error : 
                System.EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).FloatFormatHandling : 
                Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).FloatParseHandling : 
                Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).Formatting : 
                Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).MaxDepth : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).MetadataPropertyHandling : 
                Newtonsoft.Json.MetadataPropertyHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).MissingMemberHandling : 
                Newtonsoft.Json.MissingMemberHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).NullValueHandling : 
                Newtonsoft.Json.NullValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).ObjectCreationHandling : 
                Newtonsoft.Json.ObjectCreationHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).PreserveReferencesHandling : 
                Newtonsoft.Json.PreserveReferencesHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).ReferenceLoopHandling : 
                Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).ReferenceResolver : 
                Newtonsoft.Json.Serialization.IReferenceResolver

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).ReferenceResolverProvider : 
                System.Func<Newtonsoft.Json.Serialization.IReferenceResolver>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).StringEscapeHandling : 
                Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).TraceWriter : 
                Newtonsoft.Json.Serialization.ITraceWriter

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).TypeNameAssemblyFormat : 
                System.Runtime.Serialization.Formatters.FormatterAssemblyStyle

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).TypeNameHandling : 
                Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_Binder : 
                System.Void
                -> System.Runtime.Serialization.SerializationBinder

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_CheckAdditionalContent : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_ConstructorHandling : 
                System.Void
                -> Newtonsoft.Json.ConstructorHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_Context : 
                System.Void
                -> System.Runtime.Serialization.StreamingContext

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_ContractResolver : 
                System.Void
                -> Newtonsoft.Json.Serialization.IContractResolver

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_Converters : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.JsonConverter>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_DateFormatHandling : 
                System.Void
                -> Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_DateParseHandling : 
                System.Void
                -> Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_DefaultValueHandling : 
                System.Void
                -> Newtonsoft.Json.DefaultValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_EqualityComparer : 
                System.Void
                -> System.Collections.IEqualityComparer

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_Error : 
                System.Void
                -> System.EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_FloatFormatHandling : 
                System.Void
                -> Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_FloatParseHandling : 
                System.Void
                -> Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_Formatting : 
                System.Void
                -> Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_MaxDepth : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_MetadataPropertyHandling : 
                System.Void
                -> Newtonsoft.Json.MetadataPropertyHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_MissingMemberHandling : 
                System.Void
                -> Newtonsoft.Json.MissingMemberHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_NullValueHandling : 
                System.Void
                -> Newtonsoft.Json.NullValueHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_ObjectCreationHandling : 
                System.Void
                -> Newtonsoft.Json.ObjectCreationHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_PreserveReferencesHandling : 
                System.Void
                -> Newtonsoft.Json.PreserveReferencesHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_ReferenceLoopHandling : 
                System.Void
                -> Newtonsoft.Json.ReferenceLoopHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_ReferenceResolver : 
                System.Void
                -> Newtonsoft.Json.Serialization.IReferenceResolver

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_ReferenceResolverProvider : 
                System.Void
                -> System.Func<Newtonsoft.Json.Serialization.IReferenceResolver>

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_StringEscapeHandling : 
                System.Void
                -> Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_TraceWriter : 
                System.Void
                -> Newtonsoft.Json.Serialization.ITraceWriter

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_TypeNameAssemblyFormat : 
                System.Void
                -> System.Runtime.Serialization.Formatters.FormatterAssemblyStyle

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).get_TypeNameHandling : 
                System.Void
                -> Newtonsoft.Json.TypeNameHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_Binder : 
                value:System.Runtime.Serialization.SerializationBinder
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_CheckAdditionalContent : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_ConstructorHandling : 
                value:Newtonsoft.Json.ConstructorHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_Context : 
                value:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_ContractResolver : 
                value:Newtonsoft.Json.Serialization.IContractResolver
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_Converters : 
                value:System.Collections.Generic.IList<Newtonsoft.Json.JsonConverter>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_DateFormatHandling : 
                value:Newtonsoft.Json.DateFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_DateParseHandling : 
                value:Newtonsoft.Json.DateParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_DefaultValueHandling : 
                value:Newtonsoft.Json.DefaultValueHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_EqualityComparer : 
                value:System.Collections.IEqualityComparer
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_Error : 
                value:System.EventHandler<Newtonsoft.Json.Serialization.ErrorEventArgs>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_FloatFormatHandling : 
                value:Newtonsoft.Json.FloatFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_FloatParseHandling : 
                value:Newtonsoft.Json.FloatParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_Formatting : 
                value:Newtonsoft.Json.Formatting
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_MaxDepth : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_MetadataPropertyHandling : 
                value:Newtonsoft.Json.MetadataPropertyHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_MissingMemberHandling : 
                value:Newtonsoft.Json.MissingMemberHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_NullValueHandling : 
                value:Newtonsoft.Json.NullValueHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_ObjectCreationHandling : 
                value:Newtonsoft.Json.ObjectCreationHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_PreserveReferencesHandling : 
                value:Newtonsoft.Json.PreserveReferencesHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_ReferenceLoopHandling : 
                value:Newtonsoft.Json.ReferenceLoopHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_ReferenceResolver : 
                value:Newtonsoft.Json.Serialization.IReferenceResolver
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_ReferenceResolverProvider : 
                value:System.Func<Newtonsoft.Json.Serialization.IReferenceResolver>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_StringEscapeHandling : 
                value:Newtonsoft.Json.StringEscapeHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_TraceWriter : 
                value:Newtonsoft.Json.Serialization.ITraceWriter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_TypeNameAssemblyFormat : 
                value:System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonSerializerSettings).set_TypeNameHandling : 
                value:Newtonsoft.Json.TypeNameHandling
                -> System.Void

        
* Newtonsoft.Json.JsonTextReader

        Newtonsoft.Json.JsonTextReader (.NET type: Class and base: Newtonsoft.Json.JsonReader)

        new Newtonsoft.Json.JsonTextReader : 
                reader:System.IO.TextReader
                -> Newtonsoft.Json.JsonTextReader

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ArrayPool : 
                Newtonsoft.Json.IArrayPool<System.Char>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).Close : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).CloseInput : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).DateParseHandling : 
                Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).Depth : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).FloatParseHandling : 
                Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).HasLineInfo : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).LineNumber : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).LinePosition : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).MaxDepth : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).QuoteChar : 
                System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).Read : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsBytes : 
                System.Void
                -> System.Byte[]

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsDateTime : 
                System.Void
                -> System.Nullable<System.DateTime>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsDateTimeOffset : 
                System.Void
                -> System.Nullable<System.DateTimeOffset>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsDecimal : 
                System.Void
                -> System.Nullable<System.Decimal>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsInt32 : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).Skip : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).SupportMultipleContent : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).TokenType : 
                Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).Value : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ValueType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_ArrayPool : 
                System.Void
                -> Newtonsoft.Json.IArrayPool<System.Char>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_CloseInput : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_DateParseHandling : 
                System.Void
                -> Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_Depth : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_FloatParseHandling : 
                System.Void
                -> Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_LineNumber : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_LinePosition : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_MaxDepth : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_QuoteChar : 
                System.Void
                -> System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_SupportMultipleContent : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_TokenType : 
                System.Void
                -> Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_Value : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_ValueType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_ArrayPool : 
                value:Newtonsoft.Json.IArrayPool<System.Char>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_CloseInput : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_DateParseHandling : 
                value:Newtonsoft.Json.DateParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_FloatParseHandling : 
                value:Newtonsoft.Json.FloatParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_MaxDepth : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_SupportMultipleContent : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.JsonTextWriter

        Newtonsoft.Json.JsonTextWriter (.NET type: Class and base: Newtonsoft.Json.JsonWriter)

        new Newtonsoft.Json.JsonTextWriter : 
                textWriter:System.IO.TextWriter
                -> Newtonsoft.Json.JsonTextWriter

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).ArrayPool : 
                Newtonsoft.Json.IArrayPool<System.Char>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).Close : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).CloseOutput : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).DateFormatHandling : 
                Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).FloatFormatHandling : 
                Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).Flush : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).Formatting : 
                Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).IndentChar : 
                System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).Indentation : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).QuoteChar : 
                System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).QuoteName : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).StringEscapeHandling : 
                Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteComment : 
                text:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteEnd : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteEndArray : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteEndConstructor : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteEndObject : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteNull : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WritePropertyName : 
                name:System.String * escape:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WritePropertyName : 
                name:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteRaw : 
                json:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteRawValue : 
                json:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteStartArray : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteStartConstructor : 
                name:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteStartObject : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteState : 
                Newtonsoft.Json.WriteState

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteToken : 
                reader:Newtonsoft.Json.JsonReader * writeChildren:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteToken : 
                reader:Newtonsoft.Json.JsonReader
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteToken : 
                token:Newtonsoft.Json.JsonToken * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteToken : 
                token:Newtonsoft.Json.JsonToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteUndefined : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Byte
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Byte[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Char
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.DateTime
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.DateTimeOffset
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Decimal
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Double
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Guid
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Int16
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Int64
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Byte>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Char>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.DateTime>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.DateTimeOffset>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Decimal>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Double>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Guid>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Int16>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Int64>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.SByte>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.Single>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.TimeSpan>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.UInt16>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.UInt32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Nullable<System.UInt64>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.SByte
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Single
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.TimeSpan
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.UInt16
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.UInt32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.UInt64
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteValue : 
                value:System.Uri
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).WriteWhitespace : 
                ws:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_ArrayPool : 
                System.Void
                -> Newtonsoft.Json.IArrayPool<System.Char>

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_CloseOutput : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_DateFormatHandling : 
                System.Void
                -> Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_FloatFormatHandling : 
                System.Void
                -> Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_Formatting : 
                System.Void
                -> Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_IndentChar : 
                System.Void
                -> System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_Indentation : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_QuoteChar : 
                System.Void
                -> System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_QuoteName : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_StringEscapeHandling : 
                System.Void
                -> Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_WriteState : 
                System.Void
                -> Newtonsoft.Json.WriteState

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_ArrayPool : 
                value:Newtonsoft.Json.IArrayPool<System.Char>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_CloseOutput : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_DateFormatHandling : 
                value:Newtonsoft.Json.DateFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_FloatFormatHandling : 
                value:Newtonsoft.Json.FloatFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_Formatting : 
                value:Newtonsoft.Json.Formatting
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_IndentChar : 
                value:System.Char
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_Indentation : 
                value:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_QuoteChar : 
                value:System.Char
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_QuoteName : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_StringEscapeHandling : 
                value:Newtonsoft.Json.StringEscapeHandling
                -> System.Void

        
* Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.JsonToken values: [ Boolean:10; Bytes:17; Comment:5; Date:16; EndArray:14; EndConstructor:15; EndObject:13; Float:8; Integer:7; None:0; Null:11; PropertyName:4; Raw:6; StartArray:2; StartConstructor:3; StartObject:1; String:9; Undefined:12 ]

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonToken).value__ : 
                System.Int32

        Newtonsoft.Json.JsonToken.Boolean : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.Bytes : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.Comment : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.Date : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.EndArray : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.EndConstructor : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.EndObject : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.Float : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.Integer : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.None : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.Null : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.PropertyName : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.Raw : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.StartArray : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.StartConstructor : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.StartObject : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.String : 
                Newtonsoft.Json.JsonToken

        Newtonsoft.Json.JsonToken.Undefined : 
                Newtonsoft.Json.JsonToken

        
* Newtonsoft.Json.JsonValidatingReader

        Newtonsoft.Json.JsonValidatingReader (.NET type: Class and base: Newtonsoft.Json.JsonReader)

        new Newtonsoft.Json.JsonValidatingReader : 
                reader:Newtonsoft.Json.JsonReader
                -> Newtonsoft.Json.JsonValidatingReader

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Close : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).CloseInput : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).DateParseHandling : 
                Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Depth : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).FloatParseHandling : 
                Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).MaxDepth : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).QuoteChar : 
                System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Read : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsBytes : 
                System.Void
                -> System.Byte[]

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsDateTime : 
                System.Void
                -> System.Nullable<System.DateTime>

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsDateTimeOffset : 
                System.Void
                -> System.Nullable<System.DateTimeOffset>

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsDecimal : 
                System.Void
                -> System.Nullable<System.Decimal>

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsInt32 : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Reader : 
                Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Schema : 
                Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Skip : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).SupportMultipleContent : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).TokenType : 
                Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ValidationEventHandler.Add : 
                sender:System.Object * e:Newtonsoft.Json.Schema.ValidationEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).Value : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ValueType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).add_ValidationEventHandler : 
                value:Newtonsoft.Json.Schema.ValidationEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_CloseInput : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_DateParseHandling : 
                System.Void
                -> Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_Depth : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_FloatParseHandling : 
                System.Void
                -> Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_MaxDepth : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_QuoteChar : 
                System.Void
                -> System.Char

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_Reader : 
                System.Void
                -> Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_Schema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_SupportMultipleContent : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_TokenType : 
                System.Void
                -> Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_Value : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).get_ValueType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).remove_ValidationEventHandler : 
                value:Newtonsoft.Json.Schema.ValidationEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).set_CloseInput : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).set_DateParseHandling : 
                value:Newtonsoft.Json.DateParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).set_FloatParseHandling : 
                value:Newtonsoft.Json.FloatParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).set_MaxDepth : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).set_Schema : 
                value:Newtonsoft.Json.Schema.JsonSchema
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).set_SupportMultipleContent : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.JsonWriter

        Newtonsoft.Json.JsonWriter (.NET type: Abstract and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).Close : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).CloseOutput : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).DateFormatHandling : 
                Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).FloatFormatHandling : 
                Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).Flush : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).Formatting : 
                Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).StringEscapeHandling : 
                Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteComment : 
                text:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteEnd : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteEndArray : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteEndConstructor : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteEndObject : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteNull : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WritePropertyName : 
                name:System.String * escape:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WritePropertyName : 
                name:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteRaw : 
                json:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteRawValue : 
                json:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteStartArray : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteStartConstructor : 
                name:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteStartObject : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteState : 
                Newtonsoft.Json.WriteState

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteToken : 
                reader:Newtonsoft.Json.JsonReader * writeChildren:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteToken : 
                reader:Newtonsoft.Json.JsonReader
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteToken : 
                token:Newtonsoft.Json.JsonToken * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteToken : 
                token:Newtonsoft.Json.JsonToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteUndefined : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Byte
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Byte[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Char
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.DateTime
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.DateTimeOffset
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Decimal
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Double
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Guid
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Int16
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Int64
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Byte>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Char>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.DateTime>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.DateTimeOffset>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Decimal>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Double>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Guid>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Int16>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Int64>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.SByte>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.Single>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.TimeSpan>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.UInt16>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.UInt32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Nullable<System.UInt64>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.SByte
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Single
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.TimeSpan
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.UInt16
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.UInt32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.UInt64
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteValue : 
                value:System.Uri
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).WriteWhitespace : 
                ws:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_CloseOutput : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_DateFormatHandling : 
                System.Void
                -> Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_FloatFormatHandling : 
                System.Void
                -> Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_Formatting : 
                System.Void
                -> Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_StringEscapeHandling : 
                System.Void
                -> Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).get_WriteState : 
                System.Void
                -> Newtonsoft.Json.WriteState

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).set_CloseOutput : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).set_DateFormatHandling : 
                value:Newtonsoft.Json.DateFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).set_FloatFormatHandling : 
                value:Newtonsoft.Json.FloatFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).set_Formatting : 
                value:Newtonsoft.Json.Formatting
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriter).set_StringEscapeHandling : 
                value:Newtonsoft.Json.StringEscapeHandling
                -> System.Void

        
* Newtonsoft.Json.JsonWriterException

        Newtonsoft.Json.JsonWriterException (.NET type: Class and base: Newtonsoft.Json.JsonException)

        new Newtonsoft.Json.JsonWriterException : 
                System.Void
                -> Newtonsoft.Json.JsonWriterException

        new Newtonsoft.Json.JsonWriterException : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> Newtonsoft.Json.JsonWriterException

        new Newtonsoft.Json.JsonWriterException : 
                message:System.String * innerException:System.Exception
                -> Newtonsoft.Json.JsonWriterException

        new Newtonsoft.Json.JsonWriterException : 
                message:System.String
                -> Newtonsoft.Json.JsonWriterException

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).Data : 
                System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).GetBaseException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).HResult : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).HelpLink : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).InnerException : 
                System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).Message : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).Source : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).StackTrace : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).TargetSite : 
                System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).get_Data : 
                System.Void
                -> System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).get_HResult : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).get_HelpLink : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).get_InnerException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).get_Message : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).get_Source : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).get_StackTrace : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).get_TargetSite : 
                System.Void
                -> System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).set_HelpLink : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.JsonWriterException).set_Source : 
                value:System.String
                -> System.Void

        
* Newtonsoft.Json.Linq

        Newtonsoft.Json.Linq (Namespace)

        
* Newtonsoft.Json.Linq.CommentHandling

        Newtonsoft.Json.Linq.CommentHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.Linq.CommentHandling values: [ Ignore:0; Load:1 ]

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).value__ : 
                System.Int32

        Newtonsoft.Json.Linq.CommentHandling.Ignore : 
                Newtonsoft.Json.Linq.CommentHandling

        Newtonsoft.Json.Linq.CommentHandling.Load : 
                Newtonsoft.Json.Linq.CommentHandling

        
* Newtonsoft.Json.Linq.Extensions

        Newtonsoft.Json.Linq.Extensions (.NET type: Static and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.Linq.Extensions).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.Extensions).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.Extensions).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.Extensions).ToString : 
                System.Void
                -> System.String

        Newtonsoft.Json.Linq.Extensions.Ancestors : 
                source:System.Collections.Generic.IEnumerable<'T>
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        Newtonsoft.Json.Linq.Extensions.AncestorsAndSelf : 
                source:System.Collections.Generic.IEnumerable<'T>
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        Newtonsoft.Json.Linq.Extensions.AsJEnumerable : 
                source:System.Collections.Generic.IEnumerable<'T>
                -> Newtonsoft.Json.Linq.IJEnumerable<'T>

        Newtonsoft.Json.Linq.Extensions.AsJEnumerable : 
                source:System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        Newtonsoft.Json.Linq.Extensions.Children : 
                source:System.Collections.Generic.IEnumerable<'T>
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        Newtonsoft.Json.Linq.Extensions.Children : 
                source:System.Collections.Generic.IEnumerable<'T>
                -> System.Collections.Generic.IEnumerable<'U>

        Newtonsoft.Json.Linq.Extensions.Descendants : 
                source:System.Collections.Generic.IEnumerable<'T>
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        Newtonsoft.Json.Linq.Extensions.DescendantsAndSelf : 
                source:System.Collections.Generic.IEnumerable<'T>
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        Newtonsoft.Json.Linq.Extensions.Properties : 
                source:System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JObject>
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JProperty>

        Newtonsoft.Json.Linq.Extensions.Value : 
                value:System.Collections.Generic.IEnumerable<'T>
                -> 'U

        Newtonsoft.Json.Linq.Extensions.Value : 
                value:System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>
                -> 'U

        Newtonsoft.Json.Linq.Extensions.Values : 
                source:System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken> * key:System.Object
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        Newtonsoft.Json.Linq.Extensions.Values : 
                source:System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken> * key:System.Object
                -> System.Collections.Generic.IEnumerable<'U>

        Newtonsoft.Json.Linq.Extensions.Values : 
                source:System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        Newtonsoft.Json.Linq.Extensions.Values : 
                source:System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>
                -> System.Collections.Generic.IEnumerable<'U>

        
* Newtonsoft.Json.Linq.IJEnumerable<'T>

        Newtonsoft.Json.Linq.IJEnumerable<'T> (.NET type: Interface and base: none)

        (Instance/Inheritance of Newtonsoft.Json.Linq.IJEnumerable<'T>).Item : 
                Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.IJEnumerable<'T>).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        
* Newtonsoft.Json.Linq.JArray

        Newtonsoft.Json.Linq.JArray (.NET type: Class and base: Newtonsoft.Json.Linq.JContainer)

        new Newtonsoft.Json.Linq.JArray : 
                System.Void
                -> Newtonsoft.Json.Linq.JArray

        new Newtonsoft.Json.Linq.JArray : 
                content:System.Object
                -> Newtonsoft.Json.Linq.JArray

        new Newtonsoft.Json.Linq.JArray : 
                content:System.Object[]
                -> Newtonsoft.Json.Linq.JArray

        new Newtonsoft.Json.Linq.JArray : 
                other:Newtonsoft.Json.Linq.JArray
                -> Newtonsoft.Json.Linq.JArray

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Add : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Add : 
                item:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).AddAfterSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).AddAnnotation : 
                annotation:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).AddBeforeSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).AddFirst : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).AddingNew.Add : 
                sender:System.Object * e:System.ComponentModel.AddingNewEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).AfterSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Ancestors : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).AncestorsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Annotation : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Annotation : 
                type:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Annotations : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Annotations : 
                type:System.Type
                -> System.Collections.Generic.IEnumerable<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).BeforeSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Clear : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).CollectionChanged.Add : 
                sender:System.Object * e:System.Collections.Specialized.NotifyCollectionChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Contains : 
                item:Newtonsoft.Json.Linq.JToken
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).CopyTo : 
                array:Newtonsoft.Json.Linq.JToken[] * arrayIndex:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Count : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).CreateReader : 
                System.Void
                -> Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).CreateWriter : 
                System.Void
                -> Newtonsoft.Json.JsonWriter

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).DeepClone : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Descendants : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).DescendantsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).First : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).GetEnumerator : 
                System.Void
                -> System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).HasValues : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).IndexOf : 
                item:Newtonsoft.Json.Linq.JToken
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Insert : 
                index:System.Int32 * item:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).IsReadOnly : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Item : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Last : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).ListChanged.Add : 
                sender:System.Object * e:System.ComponentModel.ListChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Merge : 
                content:System.Object * settings:Newtonsoft.Json.Linq.JsonMergeSettings
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Merge : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Next : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Parent : 
                Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Previous : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Remove : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Remove : 
                item:Newtonsoft.Json.Linq.JToken
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).RemoveAll : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).RemoveAnnotations : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).RemoveAnnotations : 
                type:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).RemoveAt : 
                index:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Replace : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).ReplaceAll : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Root : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).SelectToken : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).SelectToken : 
                path:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).SelectTokens : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).SelectTokens : 
                path:System.String
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).ToObject : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).ToObject : 
                jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).ToObject : 
                objectType:System.Type * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).ToObject : 
                objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).ToString : 
                formatting:Newtonsoft.Json.Formatting * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Type : 
                Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Value : 
                key:System.Object
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).Values : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter * converters:Newtonsoft.Json.JsonConverter[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).add_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).add_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).add_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Count : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_First : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_HasValues : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_IsReadOnly : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Item : 
                index:System.Int32
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Last : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Next : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Parent : 
                System.Void
                -> Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Previous : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Root : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).get_Type : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).remove_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).remove_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).remove_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).set_Item : 
                index:System.Int32 * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JArray).set_Item : 
                key:System.Object * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        Newtonsoft.Json.Linq.JArray.FromObject : 
                o:System.Object * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> Newtonsoft.Json.Linq.JArray

        Newtonsoft.Json.Linq.JArray.FromObject : 
                o:System.Object
                -> Newtonsoft.Json.Linq.JArray

        Newtonsoft.Json.Linq.JArray.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JArray

        Newtonsoft.Json.Linq.JArray.Load : 
                reader:Newtonsoft.Json.JsonReader
                -> Newtonsoft.Json.Linq.JArray

        Newtonsoft.Json.Linq.JArray.Parse : 
                json:System.String * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JArray

        Newtonsoft.Json.Linq.JArray.Parse : 
                json:System.String
                -> Newtonsoft.Json.Linq.JArray

        
* Newtonsoft.Json.Linq.JConstructor

        Newtonsoft.Json.Linq.JConstructor (.NET type: Class and base: Newtonsoft.Json.Linq.JContainer)

        new Newtonsoft.Json.Linq.JConstructor : 
                System.Void
                -> Newtonsoft.Json.Linq.JConstructor

        new Newtonsoft.Json.Linq.JConstructor : 
                name:System.String * content:System.Object
                -> Newtonsoft.Json.Linq.JConstructor

        new Newtonsoft.Json.Linq.JConstructor : 
                name:System.String * content:System.Object[]
                -> Newtonsoft.Json.Linq.JConstructor

        new Newtonsoft.Json.Linq.JConstructor : 
                name:System.String
                -> Newtonsoft.Json.Linq.JConstructor

        new Newtonsoft.Json.Linq.JConstructor : 
                other:Newtonsoft.Json.Linq.JConstructor
                -> Newtonsoft.Json.Linq.JConstructor

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Add : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).AddAfterSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).AddAnnotation : 
                annotation:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).AddBeforeSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).AddFirst : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).AddingNew.Add : 
                sender:System.Object * e:System.ComponentModel.AddingNewEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).AfterSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Ancestors : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).AncestorsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Annotation : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Annotation : 
                type:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Annotations : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Annotations : 
                type:System.Type
                -> System.Collections.Generic.IEnumerable<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).BeforeSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).CollectionChanged.Add : 
                sender:System.Object * e:System.Collections.Specialized.NotifyCollectionChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Count : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).CreateReader : 
                System.Void
                -> Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).CreateWriter : 
                System.Void
                -> Newtonsoft.Json.JsonWriter

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).DeepClone : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Descendants : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).DescendantsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).First : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).HasValues : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Item : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Last : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).ListChanged.Add : 
                sender:System.Object * e:System.ComponentModel.ListChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Merge : 
                content:System.Object * settings:Newtonsoft.Json.Linq.JsonMergeSettings
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Merge : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Name : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Next : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Parent : 
                Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Previous : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Remove : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).RemoveAll : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).RemoveAnnotations : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).RemoveAnnotations : 
                type:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Replace : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).ReplaceAll : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Root : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).SelectToken : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).SelectToken : 
                path:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).SelectTokens : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).SelectTokens : 
                path:System.String
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).ToObject : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).ToObject : 
                jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).ToObject : 
                objectType:System.Type * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).ToObject : 
                objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).ToString : 
                formatting:Newtonsoft.Json.Formatting * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Type : 
                Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Value : 
                key:System.Object
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).Values : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter * converters:Newtonsoft.Json.JsonConverter[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).add_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).add_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).add_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Count : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_First : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_HasValues : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Last : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Name : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Next : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Parent : 
                System.Void
                -> Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Previous : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Root : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).get_Type : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).remove_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).remove_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).remove_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).set_Item : 
                key:System.Object * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JConstructor).set_Name : 
                value:System.String
                -> System.Void

        Newtonsoft.Json.Linq.JConstructor.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JConstructor

        Newtonsoft.Json.Linq.JConstructor.Load : 
                reader:Newtonsoft.Json.JsonReader
                -> Newtonsoft.Json.Linq.JConstructor

        
* Newtonsoft.Json.Linq.JContainer

        Newtonsoft.Json.Linq.JContainer (.NET type: Abstract and base: Newtonsoft.Json.Linq.JToken)

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Add : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).AddAfterSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).AddAnnotation : 
                annotation:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).AddBeforeSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).AddFirst : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).AddingNew.Add : 
                sender:System.Object * e:System.ComponentModel.AddingNewEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).AfterSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Ancestors : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).AncestorsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Annotation : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Annotation : 
                type:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Annotations : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Annotations : 
                type:System.Type
                -> System.Collections.Generic.IEnumerable<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).BeforeSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).CollectionChanged.Add : 
                sender:System.Object * e:System.Collections.Specialized.NotifyCollectionChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Count : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).CreateReader : 
                System.Void
                -> Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).CreateWriter : 
                System.Void
                -> Newtonsoft.Json.JsonWriter

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).DeepClone : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Descendants : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).DescendantsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).First : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).HasValues : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Item : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Last : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).ListChanged.Add : 
                sender:System.Object * e:System.ComponentModel.ListChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Merge : 
                content:System.Object * settings:Newtonsoft.Json.Linq.JsonMergeSettings
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Merge : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Next : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Parent : 
                Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Previous : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Remove : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).RemoveAll : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).RemoveAnnotations : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).RemoveAnnotations : 
                type:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Replace : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).ReplaceAll : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Root : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).SelectToken : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).SelectToken : 
                path:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).SelectTokens : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).SelectTokens : 
                path:System.String
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).ToObject : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).ToObject : 
                jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).ToObject : 
                objectType:System.Type * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).ToObject : 
                objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).ToString : 
                formatting:Newtonsoft.Json.Formatting * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Type : 
                Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Value : 
                key:System.Object
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).Values : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter * converters:Newtonsoft.Json.JsonConverter[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).add_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).add_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).add_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_Count : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_First : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_HasValues : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_Last : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_Next : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_Parent : 
                System.Void
                -> Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_Previous : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_Root : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).get_Type : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).remove_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).remove_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).remove_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JContainer).set_Item : 
                key:System.Object * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        
* Newtonsoft.Json.Linq.JEnumerable<'T>

        Newtonsoft.Json.Linq.JEnumerable<'T> (.NET type: Struct and base: System.ValueType)

        new Newtonsoft.Json.Linq.JEnumerable<'T> : 
                enumerable:System.Collections.Generic.IEnumerable<'T>
                -> Newtonsoft.Json.Linq.JEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JEnumerable<'T>).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JEnumerable<'T>).Equals : 
                other:Newtonsoft.Json.Linq.JEnumerable<'T>
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JEnumerable<'T>).GetEnumerator : 
                System.Void
                -> System.Collections.Generic.IEnumerator<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JEnumerable<'T>).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JEnumerable<'T>).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JEnumerable<'T>).Item : 
                Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JEnumerable<'T>).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JEnumerable<'T>).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>

        Newtonsoft.Json.Linq.JEnumerable<'T>.Empty : 
                Newtonsoft.Json.Linq.JEnumerable<'T>

        
* Newtonsoft.Json.Linq.JObject

        Newtonsoft.Json.Linq.JObject (.NET type: Class and base: Newtonsoft.Json.Linq.JContainer)

        new Newtonsoft.Json.Linq.JObject : 
                System.Void
                -> Newtonsoft.Json.Linq.JObject

        new Newtonsoft.Json.Linq.JObject : 
                content:System.Object
                -> Newtonsoft.Json.Linq.JObject

        new Newtonsoft.Json.Linq.JObject : 
                content:System.Object[]
                -> Newtonsoft.Json.Linq.JObject

        new Newtonsoft.Json.Linq.JObject : 
                other:Newtonsoft.Json.Linq.JObject
                -> Newtonsoft.Json.Linq.JObject

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Add : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Add : 
                propertyName:System.String * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).AddAfterSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).AddAnnotation : 
                annotation:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).AddBeforeSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).AddFirst : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).AddingNew.Add : 
                sender:System.Object * e:System.ComponentModel.AddingNewEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).AfterSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Ancestors : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).AncestorsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Annotation : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Annotation : 
                type:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Annotations : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Annotations : 
                type:System.Type
                -> System.Collections.Generic.IEnumerable<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).BeforeSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).CollectionChanged.Add : 
                sender:System.Object * e:System.Collections.Specialized.NotifyCollectionChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Count : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).CreateReader : 
                System.Void
                -> Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).CreateWriter : 
                System.Void
                -> Newtonsoft.Json.JsonWriter

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).DeepClone : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Descendants : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).DescendantsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).First : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).GetEnumerator : 
                System.Void
                -> System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).GetValue : 
                propertyName:System.String * comparison:System.StringComparison
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).GetValue : 
                propertyName:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).HasValues : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Item : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Last : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).ListChanged.Add : 
                sender:System.Object * e:System.ComponentModel.ListChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Merge : 
                content:System.Object * settings:Newtonsoft.Json.Linq.JsonMergeSettings
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Merge : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Next : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Parent : 
                Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Previous : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Properties : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JProperty>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Property : 
                name:System.String
                -> Newtonsoft.Json.Linq.JProperty

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).PropertyChanged.Add : 
                sender:System.Object * e:System.ComponentModel.PropertyChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).PropertyChanging.Add : 
                sender:System.Object * e:System.ComponentModel.PropertyChangingEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).PropertyValues : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Remove : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Remove : 
                propertyName:System.String
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).RemoveAll : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).RemoveAnnotations : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).RemoveAnnotations : 
                type:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Replace : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).ReplaceAll : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Root : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).SelectToken : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).SelectToken : 
                path:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).SelectTokens : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).SelectTokens : 
                path:System.String
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).ToObject : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).ToObject : 
                jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).ToObject : 
                objectType:System.Type * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).ToObject : 
                objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).ToString : 
                formatting:Newtonsoft.Json.Formatting * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).TryGetValue : 
                propertyName:System.String * comparison:System.StringComparison * value:Newtonsoft.Json.Linq.JToken&
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).TryGetValue : 
                propertyName:System.String * value:Newtonsoft.Json.Linq.JToken&
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Type : 
                Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Value : 
                key:System.Object
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).Values : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter * converters:Newtonsoft.Json.JsonConverter[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).add_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).add_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).add_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).add_PropertyChanged : 
                value:System.ComponentModel.PropertyChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).add_PropertyChanging : 
                value:System.ComponentModel.PropertyChangingEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Count : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_First : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_HasValues : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Item : 
                propertyName:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Last : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Next : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Parent : 
                System.Void
                -> Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Previous : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Root : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).get_Type : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).remove_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).remove_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).remove_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).remove_PropertyChanged : 
                value:System.ComponentModel.PropertyChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).remove_PropertyChanging : 
                value:System.ComponentModel.PropertyChangingEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).set_Item : 
                key:System.Object * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JObject).set_Item : 
                propertyName:System.String * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        Newtonsoft.Json.Linq.JObject.FromObject : 
                o:System.Object * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> Newtonsoft.Json.Linq.JObject

        Newtonsoft.Json.Linq.JObject.FromObject : 
                o:System.Object
                -> Newtonsoft.Json.Linq.JObject

        Newtonsoft.Json.Linq.JObject.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JObject

        Newtonsoft.Json.Linq.JObject.Load : 
                reader:Newtonsoft.Json.JsonReader
                -> Newtonsoft.Json.Linq.JObject

        Newtonsoft.Json.Linq.JObject.Parse : 
                json:System.String * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JObject

        Newtonsoft.Json.Linq.JObject.Parse : 
                json:System.String
                -> Newtonsoft.Json.Linq.JObject

        
* Newtonsoft.Json.Linq.JProperty

        Newtonsoft.Json.Linq.JProperty (.NET type: Class and base: Newtonsoft.Json.Linq.JContainer)

        new Newtonsoft.Json.Linq.JProperty : 
                name:System.String * content:System.Object
                -> Newtonsoft.Json.Linq.JProperty

        new Newtonsoft.Json.Linq.JProperty : 
                name:System.String * content:System.Object[]
                -> Newtonsoft.Json.Linq.JProperty

        new Newtonsoft.Json.Linq.JProperty : 
                other:Newtonsoft.Json.Linq.JProperty
                -> Newtonsoft.Json.Linq.JProperty

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Add : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).AddAfterSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).AddAnnotation : 
                annotation:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).AddBeforeSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).AddFirst : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).AddingNew.Add : 
                sender:System.Object * e:System.ComponentModel.AddingNewEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).AfterSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Ancestors : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).AncestorsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Annotation : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Annotation : 
                type:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Annotations : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Annotations : 
                type:System.Type
                -> System.Collections.Generic.IEnumerable<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).BeforeSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).CollectionChanged.Add : 
                sender:System.Object * e:System.Collections.Specialized.NotifyCollectionChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Count : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).CreateReader : 
                System.Void
                -> Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).CreateWriter : 
                System.Void
                -> Newtonsoft.Json.JsonWriter

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).DeepClone : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Descendants : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).DescendantsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).First : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).HasValues : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Item : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Last : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).ListChanged.Add : 
                sender:System.Object * e:System.ComponentModel.ListChangedEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Merge : 
                content:System.Object * settings:Newtonsoft.Json.Linq.JsonMergeSettings
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Merge : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Name : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Next : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Parent : 
                Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Previous : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Remove : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).RemoveAll : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).RemoveAnnotations : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).RemoveAnnotations : 
                type:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Replace : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).ReplaceAll : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Root : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).SelectToken : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).SelectToken : 
                path:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).SelectTokens : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).SelectTokens : 
                path:System.String
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).ToObject : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).ToObject : 
                jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).ToObject : 
                objectType:System.Type * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).ToObject : 
                objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).ToString : 
                formatting:Newtonsoft.Json.Formatting * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Type : 
                Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Value : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Value : 
                key:System.Object
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).Values : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter * converters:Newtonsoft.Json.JsonConverter[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).add_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).add_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).add_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Count : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_First : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_HasValues : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Last : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Name : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Next : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Parent : 
                System.Void
                -> Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Previous : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Root : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Type : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).get_Value : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).remove_AddingNew : 
                value:System.ComponentModel.AddingNewEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).remove_CollectionChanged : 
                value:System.Collections.Specialized.NotifyCollectionChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).remove_ListChanged : 
                value:System.ComponentModel.ListChangedEventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).set_Item : 
                key:System.Object * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JProperty).set_Value : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        Newtonsoft.Json.Linq.JProperty.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JProperty

        Newtonsoft.Json.Linq.JProperty.Load : 
                reader:Newtonsoft.Json.JsonReader
                -> Newtonsoft.Json.Linq.JProperty

        
* Newtonsoft.Json.Linq.JPropertyDescriptor

        Newtonsoft.Json.Linq.JPropertyDescriptor (.NET type: Class and base: System.ComponentModel.PropertyDescriptor)

        new Newtonsoft.Json.Linq.JPropertyDescriptor : 
                name:System.String
                -> Newtonsoft.Json.Linq.JPropertyDescriptor

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).AddValueChanged : 
                component:System.Object * handler:System.EventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).Attributes : 
                System.ComponentModel.AttributeCollection

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).CanResetValue : 
                component:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).Category : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).ComponentType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).Converter : 
                System.ComponentModel.TypeConverter

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).Description : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).DesignTimeOnly : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).DisplayName : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).GetChildProperties : 
                System.Void
                -> System.ComponentModel.PropertyDescriptorCollection

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).GetChildProperties : 
                filter:System.Attribute[]
                -> System.ComponentModel.PropertyDescriptorCollection

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).GetChildProperties : 
                instance:System.Object * filter:System.Attribute[]
                -> System.ComponentModel.PropertyDescriptorCollection

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).GetChildProperties : 
                instance:System.Object
                -> System.ComponentModel.PropertyDescriptorCollection

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).GetEditor : 
                editorBaseType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).GetValue : 
                component:System.Object
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).IsBrowsable : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).IsLocalizable : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).IsReadOnly : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).Name : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).PropertyType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).RemoveValueChanged : 
                component:System.Object * handler:System.EventHandler
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).ResetValue : 
                component:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).SerializationVisibility : 
                System.ComponentModel.DesignerSerializationVisibility

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).SetValue : 
                component:System.Object * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).ShouldSerializeValue : 
                component:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).SupportsChangeEvents : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_Attributes : 
                System.Void
                -> System.ComponentModel.AttributeCollection

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_Category : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_ComponentType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_Converter : 
                System.Void
                -> System.ComponentModel.TypeConverter

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_Description : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_DesignTimeOnly : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_DisplayName : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_IsBrowsable : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_IsLocalizable : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_IsReadOnly : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_Name : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_PropertyType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_SerializationVisibility : 
                System.Void
                -> System.ComponentModel.DesignerSerializationVisibility

        (Instance/Inheritance of Newtonsoft.Json.Linq.JPropertyDescriptor).get_SupportsChangeEvents : 
                System.Void
                -> System.Boolean

        
* Newtonsoft.Json.Linq.JRaw

        Newtonsoft.Json.Linq.JRaw (.NET type: Class and base: Newtonsoft.Json.Linq.JValue)

        new Newtonsoft.Json.Linq.JRaw : 
                other:Newtonsoft.Json.Linq.JRaw
                -> Newtonsoft.Json.Linq.JRaw

        new Newtonsoft.Json.Linq.JRaw : 
                rawJson:System.Object
                -> Newtonsoft.Json.Linq.JRaw

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).AddAfterSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).AddAnnotation : 
                annotation:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).AddBeforeSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).AfterSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Ancestors : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).AncestorsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Annotation : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Annotation : 
                type:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Annotations : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Annotations : 
                type:System.Type
                -> System.Collections.Generic.IEnumerable<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).BeforeSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).CompareTo : 
                obj:Newtonsoft.Json.Linq.JValue
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).CreateReader : 
                System.Void
                -> Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).DeepClone : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Equals : 
                other:Newtonsoft.Json.Linq.JValue
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).First : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).HasValues : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Item : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Last : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Next : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Parent : 
                Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Previous : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Remove : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).RemoveAnnotations : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).RemoveAnnotations : 
                type:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Replace : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Root : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).SelectToken : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).SelectToken : 
                path:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).SelectTokens : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).SelectTokens : 
                path:System.String
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).ToObject : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).ToObject : 
                jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).ToObject : 
                objectType:System.Type * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).ToObject : 
                objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).ToString : 
                format:System.String * formatProvider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).ToString : 
                formatProvider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).ToString : 
                formatting:Newtonsoft.Json.Formatting * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Type : 
                Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Value : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Value : 
                key:System.Object
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).Values : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter * converters:Newtonsoft.Json.JsonConverter[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_First : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_HasValues : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_Last : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_Next : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_Parent : 
                System.Void
                -> Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_Previous : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_Root : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_Type : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).get_Value : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).set_Item : 
                key:System.Object * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JRaw).set_Value : 
                value:System.Object
                -> System.Void

        Newtonsoft.Json.Linq.JRaw.Create : 
                reader:Newtonsoft.Json.JsonReader
                -> Newtonsoft.Json.Linq.JRaw

        
* Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken (.NET type: Abstract and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).AddAfterSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).AddAnnotation : 
                annotation:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).AddBeforeSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).AfterSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Ancestors : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).AncestorsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Annotation : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Annotation : 
                type:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Annotations : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Annotations : 
                type:System.Type
                -> System.Collections.Generic.IEnumerable<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).BeforeSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).CreateReader : 
                System.Void
                -> Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).DeepClone : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).First : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).HasValues : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Item : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Last : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Next : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Parent : 
                Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Previous : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Remove : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).RemoveAnnotations : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).RemoveAnnotations : 
                type:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Replace : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Root : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).SelectToken : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).SelectToken : 
                path:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).SelectTokens : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).SelectTokens : 
                path:System.String
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).ToObject : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).ToObject : 
                jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).ToObject : 
                objectType:System.Type * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).ToObject : 
                objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).ToString : 
                formatting:Newtonsoft.Json.Formatting * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Type : 
                Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Value : 
                key:System.Object
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).Values : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter * converters:Newtonsoft.Json.JsonConverter[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_First : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_HasValues : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_Last : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_Next : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_Parent : 
                System.Void
                -> Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_Previous : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_Root : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).get_Type : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JToken).set_Item : 
                key:System.Object * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        Newtonsoft.Json.Linq.JToken.DeepEquals : 
                t1:Newtonsoft.Json.Linq.JToken * t2:Newtonsoft.Json.Linq.JToken
                -> System.Boolean

        Newtonsoft.Json.Linq.JToken.EqualityComparer : 
                Newtonsoft.Json.Linq.JTokenEqualityComparer

        Newtonsoft.Json.Linq.JToken.FromObject : 
                o:System.Object * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.FromObject : 
                o:System.Object
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.Load : 
                reader:Newtonsoft.Json.JsonReader
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.Parse : 
                json:System.String * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.Parse : 
                json:System.String
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.ReadFrom : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.ReadFrom : 
                reader:Newtonsoft.Json.JsonReader
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.get_EqualityComparer : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenEqualityComparer

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Boolean

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Byte

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Byte[]

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Char

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.DateTime

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.DateTimeOffset

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Decimal

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Double

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Guid

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Int16

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Int32

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Int64

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Boolean>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Byte>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Char>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.DateTime>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.DateTimeOffset>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Decimal>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Double>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Guid>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Int16>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Int32>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Int64>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.SByte>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.Single>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.TimeSpan>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.UInt16>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.UInt32>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Nullable<System.UInt64>

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.SByte

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Single

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.String

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.TimeSpan

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.UInt16

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.UInt32

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.UInt64

        Newtonsoft.Json.Linq.JToken.op_Explicit : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Uri

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Boolean
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Byte
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Byte[]
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.DateTime
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.DateTimeOffset
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Decimal
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Double
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Guid
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Int16
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Int32
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Int64
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.Boolean>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.Byte>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.DateTime>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.DateTimeOffset>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.Decimal>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.Double>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.Guid>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.Int16>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.Int32>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.Int64>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.SByte>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.Single>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.TimeSpan>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.UInt16>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.UInt32>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Nullable<System.UInt64>
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.SByte
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Single
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.String
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.TimeSpan
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.UInt16
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.UInt32
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.UInt64
                -> Newtonsoft.Json.Linq.JToken

        Newtonsoft.Json.Linq.JToken.op_Implicit : 
                value:System.Uri
                -> Newtonsoft.Json.Linq.JToken

        
* Newtonsoft.Json.Linq.JTokenEqualityComparer

        Newtonsoft.Json.Linq.JTokenEqualityComparer (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Linq.JTokenEqualityComparer : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenEqualityComparer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenEqualityComparer).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenEqualityComparer).Equals : 
                x:Newtonsoft.Json.Linq.JToken * y:Newtonsoft.Json.Linq.JToken
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenEqualityComparer).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenEqualityComparer).GetHashCode : 
                obj:Newtonsoft.Json.Linq.JToken
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenEqualityComparer).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenEqualityComparer).ToString : 
                System.Void
                -> System.String

        
* Newtonsoft.Json.Linq.JTokenReader

        Newtonsoft.Json.Linq.JTokenReader (.NET type: Class and base: Newtonsoft.Json.JsonReader)

        new Newtonsoft.Json.Linq.JTokenReader : 
                token:Newtonsoft.Json.Linq.JToken
                -> Newtonsoft.Json.Linq.JTokenReader

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).Close : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).CloseInput : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).CurrentToken : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).DateParseHandling : 
                Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).Depth : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).FloatParseHandling : 
                Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).MaxDepth : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).QuoteChar : 
                System.Char

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).Read : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsBytes : 
                System.Void
                -> System.Byte[]

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsDateTime : 
                System.Void
                -> System.Nullable<System.DateTime>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsDateTimeOffset : 
                System.Void
                -> System.Nullable<System.DateTimeOffset>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsDecimal : 
                System.Void
                -> System.Nullable<System.Decimal>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsInt32 : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).Skip : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).SupportMultipleContent : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).TokenType : 
                Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).Value : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ValueType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_CloseInput : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_CurrentToken : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_DateParseHandling : 
                System.Void
                -> Newtonsoft.Json.DateParseHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_Depth : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_FloatParseHandling : 
                System.Void
                -> Newtonsoft.Json.FloatParseHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_MaxDepth : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_QuoteChar : 
                System.Void
                -> System.Char

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_SupportMultipleContent : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_TokenType : 
                System.Void
                -> Newtonsoft.Json.JsonToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_Value : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).get_ValueType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).set_CloseInput : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).set_DateParseHandling : 
                value:Newtonsoft.Json.DateParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).set_FloatParseHandling : 
                value:Newtonsoft.Json.FloatParseHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).set_MaxDepth : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).set_SupportMultipleContent : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.Linq.JTokenType values: [ Array:2; Boolean:9; Bytes:14; Comment:5; Constructor:3; Date:12; Float:7; Guid:15; Integer:6; None:0; Null:10; Object:1; Property:4; Raw:13; String:8; TimeSpan:17; Undefined:11; Uri:16 ]

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenType).value__ : 
                System.Int32

        Newtonsoft.Json.Linq.JTokenType.Array : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Boolean : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Bytes : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Comment : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Constructor : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Date : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Float : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Guid : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Integer : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.None : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Null : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Object : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Property : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Raw : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.String : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.TimeSpan : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Undefined : 
                Newtonsoft.Json.Linq.JTokenType

        Newtonsoft.Json.Linq.JTokenType.Uri : 
                Newtonsoft.Json.Linq.JTokenType

        
* Newtonsoft.Json.Linq.JTokenWriter

        Newtonsoft.Json.Linq.JTokenWriter (.NET type: Class and base: Newtonsoft.Json.JsonWriter)

        new Newtonsoft.Json.Linq.JTokenWriter : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenWriter

        new Newtonsoft.Json.Linq.JTokenWriter : 
                container:Newtonsoft.Json.Linq.JContainer
                -> Newtonsoft.Json.Linq.JTokenWriter

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).Close : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).CloseOutput : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).Culture : 
                System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).CurrentToken : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).DateFormatHandling : 
                Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).DateFormatString : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).DateTimeZoneHandling : 
                Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).FloatFormatHandling : 
                Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).Flush : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).Formatting : 
                Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).StringEscapeHandling : 
                Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).Token : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteComment : 
                text:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteEnd : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteEndArray : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteEndConstructor : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteEndObject : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteNull : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WritePropertyName : 
                name:System.String * escape:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WritePropertyName : 
                name:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteRaw : 
                json:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteRawValue : 
                json:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteStartArray : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteStartConstructor : 
                name:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteStartObject : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteState : 
                Newtonsoft.Json.WriteState

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteToken : 
                reader:Newtonsoft.Json.JsonReader * writeChildren:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteToken : 
                reader:Newtonsoft.Json.JsonReader
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteToken : 
                token:Newtonsoft.Json.JsonToken * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteToken : 
                token:Newtonsoft.Json.JsonToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteUndefined : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Byte
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Byte[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Char
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.DateTime
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.DateTimeOffset
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Decimal
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Double
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Guid
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Int16
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Int64
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Byte>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Char>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.DateTime>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.DateTimeOffset>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Decimal>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Double>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Guid>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Int16>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Int64>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.SByte>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.Single>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.TimeSpan>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.UInt16>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.UInt32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Nullable<System.UInt64>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.SByte
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Single
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.TimeSpan
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.UInt16
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.UInt32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.UInt64
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteValue : 
                value:System.Uri
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).WriteWhitespace : 
                ws:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_CloseOutput : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_Culture : 
                System.Void
                -> System.Globalization.CultureInfo

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_CurrentToken : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_DateFormatHandling : 
                System.Void
                -> Newtonsoft.Json.DateFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_DateFormatString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_DateTimeZoneHandling : 
                System.Void
                -> Newtonsoft.Json.DateTimeZoneHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_FloatFormatHandling : 
                System.Void
                -> Newtonsoft.Json.FloatFormatHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_Formatting : 
                System.Void
                -> Newtonsoft.Json.Formatting

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_StringEscapeHandling : 
                System.Void
                -> Newtonsoft.Json.StringEscapeHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_Token : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).get_WriteState : 
                System.Void
                -> Newtonsoft.Json.WriteState

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).set_CloseOutput : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).set_Culture : 
                value:System.Globalization.CultureInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).set_DateFormatHandling : 
                value:Newtonsoft.Json.DateFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).set_DateFormatString : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).set_DateTimeZoneHandling : 
                value:Newtonsoft.Json.DateTimeZoneHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).set_FloatFormatHandling : 
                value:Newtonsoft.Json.FloatFormatHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).set_Formatting : 
                value:Newtonsoft.Json.Formatting
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenWriter).set_StringEscapeHandling : 
                value:Newtonsoft.Json.StringEscapeHandling
                -> System.Void

        
* Newtonsoft.Json.Linq.JValue

        Newtonsoft.Json.Linq.JValue (.NET type: Class and base: Newtonsoft.Json.Linq.JToken)

        new Newtonsoft.Json.Linq.JValue : 
                other:Newtonsoft.Json.Linq.JValue
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.Boolean
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.Char
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.DateTime
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.DateTimeOffset
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.Decimal
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.Double
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.Guid
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.Int64
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.Object
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.Single
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.String
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.TimeSpan
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.UInt64
                -> Newtonsoft.Json.Linq.JValue

        new Newtonsoft.Json.Linq.JValue : 
                value:System.Uri
                -> Newtonsoft.Json.Linq.JValue

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).AddAfterSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).AddAnnotation : 
                annotation:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).AddBeforeSelf : 
                content:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).AfterSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Ancestors : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).AncestorsAndSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Annotation : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Annotation : 
                type:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Annotations : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Annotations : 
                type:System.Type
                -> System.Collections.Generic.IEnumerable<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).BeforeSelf : 
                System.Void
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Children : 
                System.Void
                -> Newtonsoft.Json.Linq.JEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).CompareTo : 
                obj:Newtonsoft.Json.Linq.JValue
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).CreateReader : 
                System.Void
                -> Newtonsoft.Json.JsonReader

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).DeepClone : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Equals : 
                other:Newtonsoft.Json.Linq.JValue
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).First : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).HasValues : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Item : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Last : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Next : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Parent : 
                Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Previous : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Remove : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).RemoveAnnotations : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).RemoveAnnotations : 
                type:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Replace : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Root : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).SelectToken : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).SelectToken : 
                path:System.String
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).SelectTokens : 
                path:System.String * errorWhenNoMatch:System.Boolean
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).SelectTokens : 
                path:System.String
                -> System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).ToObject : 
                System.Void
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).ToObject : 
                jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).ToObject : 
                objectType:System.Type * jsonSerializer:Newtonsoft.Json.JsonSerializer
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).ToObject : 
                objectType:System.Type
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).ToString : 
                format:System.String * formatProvider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).ToString : 
                formatProvider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).ToString : 
                formatting:Newtonsoft.Json.Formatting * converters:Newtonsoft.Json.JsonConverter[]
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Type : 
                Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Value : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Value : 
                key:System.Object
                -> 'T

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).Values : 
                System.Void
                -> System.Collections.Generic.IEnumerable<'T>

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter * converters:Newtonsoft.Json.JsonConverter[]
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_First : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_HasValues : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_Item : 
                key:System.Object
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_Last : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_Next : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_Parent : 
                System.Void
                -> Newtonsoft.Json.Linq.JContainer

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_Previous : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_Root : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_Type : 
                System.Void
                -> Newtonsoft.Json.Linq.JTokenType

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).get_Value : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).set_Item : 
                key:System.Object * value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JValue).set_Value : 
                value:System.Object
                -> System.Void

        Newtonsoft.Json.Linq.JValue.CreateComment : 
                value:System.String
                -> Newtonsoft.Json.Linq.JValue

        Newtonsoft.Json.Linq.JValue.CreateNull : 
                System.Void
                -> Newtonsoft.Json.Linq.JValue

        Newtonsoft.Json.Linq.JValue.CreateString : 
                value:System.String
                -> Newtonsoft.Json.Linq.JValue

        Newtonsoft.Json.Linq.JValue.CreateUndefined : 
                System.Void
                -> Newtonsoft.Json.Linq.JValue

        
* Newtonsoft.Json.Linq.JsonLoadSettings

        Newtonsoft.Json.Linq.JsonLoadSettings (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Linq.JsonLoadSettings : 
                System.Void
                -> Newtonsoft.Json.Linq.JsonLoadSettings

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).CommentHandling : 
                Newtonsoft.Json.Linq.CommentHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).LineInfoHandling : 
                Newtonsoft.Json.Linq.LineInfoHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).get_CommentHandling : 
                System.Void
                -> Newtonsoft.Json.Linq.CommentHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).get_LineInfoHandling : 
                System.Void
                -> Newtonsoft.Json.Linq.LineInfoHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).set_CommentHandling : 
                value:Newtonsoft.Json.Linq.CommentHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).set_LineInfoHandling : 
                value:Newtonsoft.Json.Linq.LineInfoHandling
                -> System.Void

        
* Newtonsoft.Json.Linq.JsonMergeSettings

        Newtonsoft.Json.Linq.JsonMergeSettings (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Linq.JsonMergeSettings : 
                System.Void
                -> Newtonsoft.Json.Linq.JsonMergeSettings

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).MergeArrayHandling : 
                Newtonsoft.Json.Linq.MergeArrayHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).MergeNullValueHandling : 
                Newtonsoft.Json.Linq.MergeNullValueHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).get_MergeArrayHandling : 
                System.Void
                -> Newtonsoft.Json.Linq.MergeArrayHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).get_MergeNullValueHandling : 
                System.Void
                -> Newtonsoft.Json.Linq.MergeNullValueHandling

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).set_MergeArrayHandling : 
                value:Newtonsoft.Json.Linq.MergeArrayHandling
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).set_MergeNullValueHandling : 
                value:Newtonsoft.Json.Linq.MergeNullValueHandling
                -> System.Void

        
* Newtonsoft.Json.Linq.LineInfoHandling

        Newtonsoft.Json.Linq.LineInfoHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.Linq.LineInfoHandling values: [ Ignore:0; Load:1 ]

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).value__ : 
                System.Int32

        Newtonsoft.Json.Linq.LineInfoHandling.Ignore : 
                Newtonsoft.Json.Linq.LineInfoHandling

        Newtonsoft.Json.Linq.LineInfoHandling.Load : 
                Newtonsoft.Json.Linq.LineInfoHandling

        
* Newtonsoft.Json.Linq.MergeArrayHandling

        Newtonsoft.Json.Linq.MergeArrayHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.Linq.MergeArrayHandling values: [ Concat:0; Merge:3; Replace:2; Union:1 ]

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeArrayHandling).value__ : 
                System.Int32

        Newtonsoft.Json.Linq.MergeArrayHandling.Concat : 
                Newtonsoft.Json.Linq.MergeArrayHandling

        Newtonsoft.Json.Linq.MergeArrayHandling.Merge : 
                Newtonsoft.Json.Linq.MergeArrayHandling

        Newtonsoft.Json.Linq.MergeArrayHandling.Replace : 
                Newtonsoft.Json.Linq.MergeArrayHandling

        Newtonsoft.Json.Linq.MergeArrayHandling.Union : 
                Newtonsoft.Json.Linq.MergeArrayHandling

        
* Newtonsoft.Json.Linq.MergeNullValueHandling

        Newtonsoft.Json.Linq.MergeNullValueHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.Linq.MergeNullValueHandling values: [ Ignore:0; Merge:1 ]

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).value__ : 
                System.Int32

        Newtonsoft.Json.Linq.MergeNullValueHandling.Ignore : 
                Newtonsoft.Json.Linq.MergeNullValueHandling

        Newtonsoft.Json.Linq.MergeNullValueHandling.Merge : 
                Newtonsoft.Json.Linq.MergeNullValueHandling

        
* Newtonsoft.Json.MemberSerialization

        Newtonsoft.Json.MemberSerialization (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.MemberSerialization values: [ Fields:2; OptIn:1; OptOut:0 ]

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MemberSerialization).value__ : 
                System.Int32

        Newtonsoft.Json.MemberSerialization.Fields : 
                Newtonsoft.Json.MemberSerialization

        Newtonsoft.Json.MemberSerialization.OptIn : 
                Newtonsoft.Json.MemberSerialization

        Newtonsoft.Json.MemberSerialization.OptOut : 
                Newtonsoft.Json.MemberSerialization

        
* Newtonsoft.Json.MetadataPropertyHandling

        Newtonsoft.Json.MetadataPropertyHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.MetadataPropertyHandling values: [ Default:0; Ignore:2; ReadAhead:1 ]

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MetadataPropertyHandling).value__ : 
                System.Int32

        Newtonsoft.Json.MetadataPropertyHandling.Default : 
                Newtonsoft.Json.MetadataPropertyHandling

        Newtonsoft.Json.MetadataPropertyHandling.Ignore : 
                Newtonsoft.Json.MetadataPropertyHandling

        Newtonsoft.Json.MetadataPropertyHandling.ReadAhead : 
                Newtonsoft.Json.MetadataPropertyHandling

        
* Newtonsoft.Json.MissingMemberHandling

        Newtonsoft.Json.MissingMemberHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.MissingMemberHandling values: [ Error:1; Ignore:0 ]

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.MissingMemberHandling).value__ : 
                System.Int32

        Newtonsoft.Json.MissingMemberHandling.Error : 
                Newtonsoft.Json.MissingMemberHandling

        Newtonsoft.Json.MissingMemberHandling.Ignore : 
                Newtonsoft.Json.MissingMemberHandling

        
* Newtonsoft.Json.NullValueHandling

        Newtonsoft.Json.NullValueHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.NullValueHandling values: [ Ignore:1; Include:0 ]

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.NullValueHandling).value__ : 
                System.Int32

        Newtonsoft.Json.NullValueHandling.Ignore : 
                Newtonsoft.Json.NullValueHandling

        Newtonsoft.Json.NullValueHandling.Include : 
                Newtonsoft.Json.NullValueHandling

        
* Newtonsoft.Json.ObjectCreationHandling

        Newtonsoft.Json.ObjectCreationHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.ObjectCreationHandling values: [ Auto:0; Replace:2; Reuse:1 ]

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ObjectCreationHandling).value__ : 
                System.Int32

        Newtonsoft.Json.ObjectCreationHandling.Auto : 
                Newtonsoft.Json.ObjectCreationHandling

        Newtonsoft.Json.ObjectCreationHandling.Replace : 
                Newtonsoft.Json.ObjectCreationHandling

        Newtonsoft.Json.ObjectCreationHandling.Reuse : 
                Newtonsoft.Json.ObjectCreationHandling

        
* Newtonsoft.Json.PreserveReferencesHandling

        Newtonsoft.Json.PreserveReferencesHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.PreserveReferencesHandling values: [ All:3; Arrays:2; None:0; Objects:1 ]

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.PreserveReferencesHandling).value__ : 
                System.Int32

        Newtonsoft.Json.PreserveReferencesHandling.All : 
                Newtonsoft.Json.PreserveReferencesHandling

        Newtonsoft.Json.PreserveReferencesHandling.Arrays : 
                Newtonsoft.Json.PreserveReferencesHandling

        Newtonsoft.Json.PreserveReferencesHandling.None : 
                Newtonsoft.Json.PreserveReferencesHandling

        Newtonsoft.Json.PreserveReferencesHandling.Objects : 
                Newtonsoft.Json.PreserveReferencesHandling

        
* Newtonsoft.Json.ReferenceLoopHandling

        Newtonsoft.Json.ReferenceLoopHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.ReferenceLoopHandling values: [ Error:0; Ignore:1; Serialize:2 ]

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.ReferenceLoopHandling).value__ : 
                System.Int32

        Newtonsoft.Json.ReferenceLoopHandling.Error : 
                Newtonsoft.Json.ReferenceLoopHandling

        Newtonsoft.Json.ReferenceLoopHandling.Ignore : 
                Newtonsoft.Json.ReferenceLoopHandling

        Newtonsoft.Json.ReferenceLoopHandling.Serialize : 
                Newtonsoft.Json.ReferenceLoopHandling

        
* Newtonsoft.Json.Required

        Newtonsoft.Json.Required (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.Required values: [ AllowNull:1; Always:2; Default:0; DisallowNull:3 ]

        (Instance/Inheritance of Newtonsoft.Json.Required).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Required).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Required).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Required).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Required).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.Required).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Required).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Required).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Required).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Required).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Required).value__ : 
                System.Int32

        Newtonsoft.Json.Required.AllowNull : 
                Newtonsoft.Json.Required

        Newtonsoft.Json.Required.Always : 
                Newtonsoft.Json.Required

        Newtonsoft.Json.Required.Default : 
                Newtonsoft.Json.Required

        Newtonsoft.Json.Required.DisallowNull : 
                Newtonsoft.Json.Required

        
* Newtonsoft.Json.Schema

        Newtonsoft.Json.Schema (Namespace)

        
* Newtonsoft.Json.Schema.Extensions

        Newtonsoft.Json.Schema.Extensions (.NET type: Static and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.Schema.Extensions).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.Extensions).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.Extensions).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Schema.Extensions).ToString : 
                System.Void
                -> System.String

        Newtonsoft.Json.Schema.Extensions.IsValid : 
                source:Newtonsoft.Json.Linq.JToken * schema:Newtonsoft.Json.Schema.JsonSchema * errorMessages:System.Collections.Generic.IList`1[[System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]&
                -> System.Boolean

        Newtonsoft.Json.Schema.Extensions.IsValid : 
                source:Newtonsoft.Json.Linq.JToken * schema:Newtonsoft.Json.Schema.JsonSchema
                -> System.Boolean

        Newtonsoft.Json.Schema.Extensions.Validate : 
                source:Newtonsoft.Json.Linq.JToken * schema:Newtonsoft.Json.Schema.JsonSchema * validationEventHandler:Newtonsoft.Json.Schema.ValidationEventHandler
                -> System.Void

        Newtonsoft.Json.Schema.Extensions.Validate : 
                source:Newtonsoft.Json.Linq.JToken * schema:Newtonsoft.Json.Schema.JsonSchema
                -> System.Void

        
* Newtonsoft.Json.Schema.JsonSchema

        Newtonsoft.Json.Schema.JsonSchema (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Schema.JsonSchema : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).AdditionalItems : 
                Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).AdditionalProperties : 
                Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).AllowAdditionalItems : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).AllowAdditionalProperties : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Default : 
                Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Description : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Disallow : 
                System.Nullable<Newtonsoft.Json.Schema.JsonSchemaType>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).DivisibleBy : 
                System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Enum : 
                System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).ExclusiveMaximum : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).ExclusiveMinimum : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Extends : 
                System.Collections.Generic.IList<Newtonsoft.Json.Schema.JsonSchema>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Format : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Hidden : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Id : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Items : 
                System.Collections.Generic.IList<Newtonsoft.Json.Schema.JsonSchema>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Maximum : 
                System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).MaximumItems : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).MaximumLength : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Minimum : 
                System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).MinimumItems : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).MinimumLength : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Pattern : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).PatternProperties : 
                System.Collections.Generic.IDictionary<System.String,Newtonsoft.Json.Schema.JsonSchema>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).PositionalItemsValidation : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Properties : 
                System.Collections.Generic.IDictionary<System.String,Newtonsoft.Json.Schema.JsonSchema>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).ReadOnly : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Required : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Requires : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Title : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Transient : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).Type : 
                System.Nullable<Newtonsoft.Json.Schema.JsonSchemaType>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).UniqueItems : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter * resolver:Newtonsoft.Json.Schema.JsonSchemaResolver
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).WriteTo : 
                writer:Newtonsoft.Json.JsonWriter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_AdditionalItems : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_AdditionalProperties : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_AllowAdditionalItems : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_AllowAdditionalProperties : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Default : 
                System.Void
                -> Newtonsoft.Json.Linq.JToken

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Description : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Disallow : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.Schema.JsonSchemaType>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_DivisibleBy : 
                System.Void
                -> System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Enum : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_ExclusiveMaximum : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_ExclusiveMinimum : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Extends : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Schema.JsonSchema>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Format : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Hidden : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Id : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Items : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Schema.JsonSchema>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Maximum : 
                System.Void
                -> System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_MaximumItems : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_MaximumLength : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Minimum : 
                System.Void
                -> System.Nullable<System.Double>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_MinimumItems : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_MinimumLength : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Pattern : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_PatternProperties : 
                System.Void
                -> System.Collections.Generic.IDictionary<System.String,Newtonsoft.Json.Schema.JsonSchema>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_PositionalItemsValidation : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Properties : 
                System.Void
                -> System.Collections.Generic.IDictionary<System.String,Newtonsoft.Json.Schema.JsonSchema>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_ReadOnly : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Required : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Requires : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Title : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Transient : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_Type : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.Schema.JsonSchemaType>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).get_UniqueItems : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_AdditionalItems : 
                value:Newtonsoft.Json.Schema.JsonSchema
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_AdditionalProperties : 
                value:Newtonsoft.Json.Schema.JsonSchema
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_AllowAdditionalItems : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_AllowAdditionalProperties : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Default : 
                value:Newtonsoft.Json.Linq.JToken
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Description : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Disallow : 
                value:System.Nullable<Newtonsoft.Json.Schema.JsonSchemaType>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_DivisibleBy : 
                value:System.Nullable<System.Double>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Enum : 
                value:System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_ExclusiveMaximum : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_ExclusiveMinimum : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Extends : 
                value:System.Collections.Generic.IList<Newtonsoft.Json.Schema.JsonSchema>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Format : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Hidden : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Id : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Items : 
                value:System.Collections.Generic.IList<Newtonsoft.Json.Schema.JsonSchema>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Maximum : 
                value:System.Nullable<System.Double>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_MaximumItems : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_MaximumLength : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Minimum : 
                value:System.Nullable<System.Double>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_MinimumItems : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_MinimumLength : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Pattern : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_PatternProperties : 
                value:System.Collections.Generic.IDictionary<System.String,Newtonsoft.Json.Schema.JsonSchema>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_PositionalItemsValidation : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Properties : 
                value:System.Collections.Generic.IDictionary<System.String,Newtonsoft.Json.Schema.JsonSchema>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_ReadOnly : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Required : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Requires : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Title : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Transient : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_Type : 
                value:System.Nullable<Newtonsoft.Json.Schema.JsonSchemaType>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchema).set_UniqueItems : 
                value:System.Boolean
                -> System.Void

        Newtonsoft.Json.Schema.JsonSchema.Parse : 
                json:System.String * resolver:Newtonsoft.Json.Schema.JsonSchemaResolver
                -> Newtonsoft.Json.Schema.JsonSchema

        Newtonsoft.Json.Schema.JsonSchema.Parse : 
                json:System.String
                -> Newtonsoft.Json.Schema.JsonSchema

        Newtonsoft.Json.Schema.JsonSchema.Read : 
                reader:Newtonsoft.Json.JsonReader * resolver:Newtonsoft.Json.Schema.JsonSchemaResolver
                -> Newtonsoft.Json.Schema.JsonSchema

        Newtonsoft.Json.Schema.JsonSchema.Read : 
                reader:Newtonsoft.Json.JsonReader
                -> Newtonsoft.Json.Schema.JsonSchema

        
* Newtonsoft.Json.Schema.JsonSchemaException

        Newtonsoft.Json.Schema.JsonSchemaException (.NET type: Class and base: Newtonsoft.Json.JsonException)

        new Newtonsoft.Json.Schema.JsonSchemaException : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchemaException

        new Newtonsoft.Json.Schema.JsonSchemaException : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> Newtonsoft.Json.Schema.JsonSchemaException

        new Newtonsoft.Json.Schema.JsonSchemaException : 
                message:System.String * innerException:System.Exception
                -> Newtonsoft.Json.Schema.JsonSchemaException

        new Newtonsoft.Json.Schema.JsonSchemaException : 
                message:System.String
                -> Newtonsoft.Json.Schema.JsonSchemaException

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).Data : 
                System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).GetBaseException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).HResult : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).HelpLink : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).InnerException : 
                System.Exception

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).LineNumber : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).LinePosition : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).Message : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).Source : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).StackTrace : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).TargetSite : 
                System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_Data : 
                System.Void
                -> System.Collections.IDictionary

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_HResult : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_HelpLink : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_InnerException : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_LineNumber : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_LinePosition : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_Message : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_Source : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_StackTrace : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).get_TargetSite : 
                System.Void
                -> System.Reflection.MethodBase

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).set_HelpLink : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaException).set_Source : 
                value:System.String
                -> System.Void

        
* Newtonsoft.Json.Schema.JsonSchemaGenerator

        Newtonsoft.Json.Schema.JsonSchemaGenerator (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Schema.JsonSchemaGenerator : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchemaGenerator

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).ContractResolver : 
                Newtonsoft.Json.Serialization.IContractResolver

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).Generate : 
                type:System.Type * resolver:Newtonsoft.Json.Schema.JsonSchemaResolver * rootSchemaNullable:System.Boolean
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).Generate : 
                type:System.Type * resolver:Newtonsoft.Json.Schema.JsonSchemaResolver
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).Generate : 
                type:System.Type * rootSchemaNullable:System.Boolean
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).Generate : 
                type:System.Type
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).UndefinedSchemaIdHandling : 
                Newtonsoft.Json.Schema.UndefinedSchemaIdHandling

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).get_ContractResolver : 
                System.Void
                -> Newtonsoft.Json.Serialization.IContractResolver

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).get_UndefinedSchemaIdHandling : 
                System.Void
                -> Newtonsoft.Json.Schema.UndefinedSchemaIdHandling

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).set_ContractResolver : 
                value:Newtonsoft.Json.Serialization.IContractResolver
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaGenerator).set_UndefinedSchemaIdHandling : 
                value:Newtonsoft.Json.Schema.UndefinedSchemaIdHandling
                -> System.Void

        
* Newtonsoft.Json.Schema.JsonSchemaResolver

        Newtonsoft.Json.Schema.JsonSchemaResolver (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Schema.JsonSchemaResolver : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchemaResolver

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaResolver).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaResolver).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaResolver).GetSchema : 
                reference:System.String
                -> Newtonsoft.Json.Schema.JsonSchema

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaResolver).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaResolver).LoadedSchemas : 
                System.Collections.Generic.IList<Newtonsoft.Json.Schema.JsonSchema>

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaResolver).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaResolver).get_LoadedSchemas : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Schema.JsonSchema>

        
* Newtonsoft.Json.Schema.JsonSchemaType

        Newtonsoft.Json.Schema.JsonSchemaType (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.Schema.JsonSchemaType values: [ Any:127; Array:32; Boolean:8; Float:2; Integer:4; None:0; Null:64; Object:16; String:1 ]

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.JsonSchemaType).value__ : 
                System.Int32

        Newtonsoft.Json.Schema.JsonSchemaType.Any : 
                Newtonsoft.Json.Schema.JsonSchemaType

        Newtonsoft.Json.Schema.JsonSchemaType.Array : 
                Newtonsoft.Json.Schema.JsonSchemaType

        Newtonsoft.Json.Schema.JsonSchemaType.Boolean : 
                Newtonsoft.Json.Schema.JsonSchemaType

        Newtonsoft.Json.Schema.JsonSchemaType.Float : 
                Newtonsoft.Json.Schema.JsonSchemaType

        Newtonsoft.Json.Schema.JsonSchemaType.Integer : 
                Newtonsoft.Json.Schema.JsonSchemaType

        Newtonsoft.Json.Schema.JsonSchemaType.None : 
                Newtonsoft.Json.Schema.JsonSchemaType

        Newtonsoft.Json.Schema.JsonSchemaType.Null : 
                Newtonsoft.Json.Schema.JsonSchemaType

        Newtonsoft.Json.Schema.JsonSchemaType.Object : 
                Newtonsoft.Json.Schema.JsonSchemaType

        Newtonsoft.Json.Schema.JsonSchemaType.String : 
                Newtonsoft.Json.Schema.JsonSchemaType

        
* Newtonsoft.Json.Schema.UndefinedSchemaIdHandling

        Newtonsoft.Json.Schema.UndefinedSchemaIdHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.Schema.UndefinedSchemaIdHandling values: [ None:0; UseAssemblyQualifiedName:2; UseTypeName:1 ]

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.UndefinedSchemaIdHandling).value__ : 
                System.Int32

        Newtonsoft.Json.Schema.UndefinedSchemaIdHandling.None : 
                Newtonsoft.Json.Schema.UndefinedSchemaIdHandling

        Newtonsoft.Json.Schema.UndefinedSchemaIdHandling.UseAssemblyQualifiedName : 
                Newtonsoft.Json.Schema.UndefinedSchemaIdHandling

        Newtonsoft.Json.Schema.UndefinedSchemaIdHandling.UseTypeName : 
                Newtonsoft.Json.Schema.UndefinedSchemaIdHandling

        
* Newtonsoft.Json.Schema.ValidationEventArgs

        Newtonsoft.Json.Schema.ValidationEventArgs (.NET type: Class and base: System.EventArgs)

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).Exception : 
                Newtonsoft.Json.Schema.JsonSchemaException

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).Message : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).get_Exception : 
                System.Void
                -> Newtonsoft.Json.Schema.JsonSchemaException

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).get_Message : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventArgs).get_Path : 
                System.Void
                -> System.String

        
* Newtonsoft.Json.Schema.ValidationEventHandler

        Newtonsoft.Json.Schema.ValidationEventHandler (.NET type: Class and base: System.MulticastDelegate)

        new Newtonsoft.Json.Schema.ValidationEventHandler : 
                object:System.Object * method:System.IntPtr
                -> Newtonsoft.Json.Schema.ValidationEventHandler

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).BeginInvoke : 
                sender:System.Object * e:Newtonsoft.Json.Schema.ValidationEventArgs * callback:System.AsyncCallback * object:System.Object
                -> System.IAsyncResult

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).Clone : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).DynamicInvoke : 
                args:System.Object[]
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).EndInvoke : 
                result:System.IAsyncResult
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).GetInvocationList : 
                System.Void
                -> System.Delegate[]

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).Invoke : 
                sender:System.Object * e:Newtonsoft.Json.Schema.ValidationEventArgs
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).Method : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).Target : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).get_Method : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Schema.ValidationEventHandler).get_Target : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.Serialization

        Newtonsoft.Json.Serialization (Namespace)

        
* Newtonsoft.Json.Serialization.CamelCaseNamingStrategy

        Newtonsoft.Json.Serialization.CamelCaseNamingStrategy (.NET type: Class and base: Newtonsoft.Json.Serialization.NamingStrategy)

        new Newtonsoft.Json.Serialization.CamelCaseNamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.CamelCaseNamingStrategy

        new Newtonsoft.Json.Serialization.CamelCaseNamingStrategy : 
                processDictionaryKeys:System.Boolean * overrideSpecifiedNames:System.Boolean
                -> Newtonsoft.Json.Serialization.CamelCaseNamingStrategy

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).GetDictionaryKey : 
                key:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).GetPropertyName : 
                name:System.String * hasSpecifiedName:System.Boolean
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).OverrideSpecifiedNames : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).ProcessDictionaryKeys : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).get_OverrideSpecifiedNames : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).get_ProcessDictionaryKeys : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).set_OverrideSpecifiedNames : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).set_ProcessDictionaryKeys : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver

        Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver (.NET type: Class and base: Newtonsoft.Json.Serialization.DefaultContractResolver)

        new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver : 
                System.Void
                -> Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).DefaultMembersSearchFlags : 
                System.Reflection.BindingFlags

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).DynamicCodeGeneration : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).GetResolvedPropertyName : 
                propertyName:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).IgnoreSerializableAttribute : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).IgnoreSerializableInterface : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).NamingStrategy : 
                Newtonsoft.Json.Serialization.NamingStrategy

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).ResolveContract : 
                type:System.Type
                -> Newtonsoft.Json.Serialization.JsonContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).SerializeCompilerGeneratedMembers : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).get_DefaultMembersSearchFlags : 
                System.Void
                -> System.Reflection.BindingFlags

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).get_DynamicCodeGeneration : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).get_IgnoreSerializableAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).get_IgnoreSerializableInterface : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).get_NamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.NamingStrategy

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).get_SerializeCompilerGeneratedMembers : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).set_DefaultMembersSearchFlags : 
                value:System.Reflection.BindingFlags
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).set_IgnoreSerializableAttribute : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).set_IgnoreSerializableInterface : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).set_NamingStrategy : 
                value:Newtonsoft.Json.Serialization.NamingStrategy
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).set_SerializeCompilerGeneratedMembers : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.DefaultContractResolver

        Newtonsoft.Json.Serialization.DefaultContractResolver (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Serialization.DefaultContractResolver : 
                System.Void
                -> Newtonsoft.Json.Serialization.DefaultContractResolver

        new Newtonsoft.Json.Serialization.DefaultContractResolver : 
                shareCache:System.Boolean
                -> Newtonsoft.Json.Serialization.DefaultContractResolver

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).DefaultMembersSearchFlags : 
                System.Reflection.BindingFlags

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).DynamicCodeGeneration : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).GetResolvedPropertyName : 
                propertyName:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).IgnoreSerializableAttribute : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).IgnoreSerializableInterface : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).NamingStrategy : 
                Newtonsoft.Json.Serialization.NamingStrategy

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).ResolveContract : 
                type:System.Type
                -> Newtonsoft.Json.Serialization.JsonContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).SerializeCompilerGeneratedMembers : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).get_DefaultMembersSearchFlags : 
                System.Void
                -> System.Reflection.BindingFlags

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).get_DynamicCodeGeneration : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).get_IgnoreSerializableAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).get_IgnoreSerializableInterface : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).get_NamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.NamingStrategy

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).get_SerializeCompilerGeneratedMembers : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).set_DefaultMembersSearchFlags : 
                value:System.Reflection.BindingFlags
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).set_IgnoreSerializableAttribute : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).set_IgnoreSerializableInterface : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).set_NamingStrategy : 
                value:Newtonsoft.Json.Serialization.NamingStrategy
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).set_SerializeCompilerGeneratedMembers : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.DefaultNamingStrategy

        Newtonsoft.Json.Serialization.DefaultNamingStrategy (.NET type: Class and base: Newtonsoft.Json.Serialization.NamingStrategy)

        new Newtonsoft.Json.Serialization.DefaultNamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.DefaultNamingStrategy

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).GetDictionaryKey : 
                key:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).GetPropertyName : 
                name:System.String * hasSpecifiedName:System.Boolean
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).OverrideSpecifiedNames : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).ProcessDictionaryKeys : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).get_OverrideSpecifiedNames : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).get_ProcessDictionaryKeys : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).set_OverrideSpecifiedNames : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).set_ProcessDictionaryKeys : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.DefaultSerializationBinder

        Newtonsoft.Json.Serialization.DefaultSerializationBinder (.NET type: Class and base: System.Runtime.Serialization.SerializationBinder)

        new Newtonsoft.Json.Serialization.DefaultSerializationBinder : 
                System.Void
                -> Newtonsoft.Json.Serialization.DefaultSerializationBinder

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultSerializationBinder).BindToName : 
                serializedType:System.Type * assemblyName:System.String& * typeName:System.String&
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultSerializationBinder).BindToType : 
                assemblyName:System.String * typeName:System.String
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultSerializationBinder).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultSerializationBinder).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultSerializationBinder).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultSerializationBinder).ToString : 
                System.Void
                -> System.String

        
* Newtonsoft.Json.Serialization.DiagnosticsTraceWriter

        Newtonsoft.Json.Serialization.DiagnosticsTraceWriter (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Serialization.DiagnosticsTraceWriter : 
                System.Void
                -> Newtonsoft.Json.Serialization.DiagnosticsTraceWriter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DiagnosticsTraceWriter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DiagnosticsTraceWriter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DiagnosticsTraceWriter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DiagnosticsTraceWriter).LevelFilter : 
                System.Diagnostics.TraceLevel

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DiagnosticsTraceWriter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DiagnosticsTraceWriter).Trace : 
                level:System.Diagnostics.TraceLevel * message:System.String * ex:System.Exception
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DiagnosticsTraceWriter).get_LevelFilter : 
                System.Void
                -> System.Diagnostics.TraceLevel

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DiagnosticsTraceWriter).set_LevelFilter : 
                value:System.Diagnostics.TraceLevel
                -> System.Void

        
* Newtonsoft.Json.Serialization.DynamicValueProvider

        Newtonsoft.Json.Serialization.DynamicValueProvider (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Serialization.DynamicValueProvider : 
                memberInfo:System.Reflection.MemberInfo
                -> Newtonsoft.Json.Serialization.DynamicValueProvider

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DynamicValueProvider).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DynamicValueProvider).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DynamicValueProvider).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DynamicValueProvider).GetValue : 
                target:System.Object
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DynamicValueProvider).SetValue : 
                target:System.Object * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.DynamicValueProvider).ToString : 
                System.Void
                -> System.String

        
* Newtonsoft.Json.Serialization.ErrorContext

        Newtonsoft.Json.Serialization.ErrorContext (.NET type: Class and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).Error : 
                System.Exception

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).Handled : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).Member : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).OriginalObject : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).Path : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).get_Error : 
                System.Void
                -> System.Exception

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).get_Handled : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).get_Member : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).get_OriginalObject : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).get_Path : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorContext).set_Handled : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.ErrorEventArgs

        Newtonsoft.Json.Serialization.ErrorEventArgs (.NET type: Class and base: System.EventArgs)

        new Newtonsoft.Json.Serialization.ErrorEventArgs : 
                currentObject:System.Object * errorContext:Newtonsoft.Json.Serialization.ErrorContext
                -> Newtonsoft.Json.Serialization.ErrorEventArgs

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorEventArgs).CurrentObject : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorEventArgs).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorEventArgs).ErrorContext : 
                Newtonsoft.Json.Serialization.ErrorContext

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorEventArgs).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorEventArgs).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorEventArgs).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorEventArgs).get_CurrentObject : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ErrorEventArgs).get_ErrorContext : 
                System.Void
                -> Newtonsoft.Json.Serialization.ErrorContext

        
* Newtonsoft.Json.Serialization.ExpressionValueProvider

        Newtonsoft.Json.Serialization.ExpressionValueProvider (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Serialization.ExpressionValueProvider : 
                memberInfo:System.Reflection.MemberInfo
                -> Newtonsoft.Json.Serialization.ExpressionValueProvider

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExpressionValueProvider).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExpressionValueProvider).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExpressionValueProvider).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExpressionValueProvider).GetValue : 
                target:System.Object
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExpressionValueProvider).SetValue : 
                target:System.Object * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExpressionValueProvider).ToString : 
                System.Void
                -> System.String

        
* Newtonsoft.Json.Serialization.ExtensionDataGetter

        Newtonsoft.Json.Serialization.ExtensionDataGetter (.NET type: Class and base: System.MulticastDelegate)

        new Newtonsoft.Json.Serialization.ExtensionDataGetter : 
                object:System.Object * method:System.IntPtr
                -> Newtonsoft.Json.Serialization.ExtensionDataGetter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).BeginInvoke : 
                o:System.Object * callback:System.AsyncCallback * object:System.Object
                -> System.IAsyncResult

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).Clone : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).DynamicInvoke : 
                args:System.Object[]
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).EndInvoke : 
                result:System.IAsyncResult
                -> System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<System.Object,System.Object>>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).GetInvocationList : 
                System.Void
                -> System.Delegate[]

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).Invoke : 
                o:System.Object
                -> System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<System.Object,System.Object>>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).Method : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).Target : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).get_Method : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataGetter).get_Target : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.Serialization.ExtensionDataSetter

        Newtonsoft.Json.Serialization.ExtensionDataSetter (.NET type: Class and base: System.MulticastDelegate)

        new Newtonsoft.Json.Serialization.ExtensionDataSetter : 
                object:System.Object * method:System.IntPtr
                -> Newtonsoft.Json.Serialization.ExtensionDataSetter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).BeginInvoke : 
                o:System.Object * key:System.String * value:System.Object * callback:System.AsyncCallback * object:System.Object
                -> System.IAsyncResult

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).Clone : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).DynamicInvoke : 
                args:System.Object[]
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).EndInvoke : 
                result:System.IAsyncResult
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).GetInvocationList : 
                System.Void
                -> System.Delegate[]

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).Invoke : 
                o:System.Object * key:System.String * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).Method : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).Target : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).get_Method : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ExtensionDataSetter).get_Target : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.Serialization.IAttributeProvider

        Newtonsoft.Json.Serialization.IAttributeProvider (.NET type: Interface and base: none)

        (Instance/Inheritance of Newtonsoft.Json.Serialization.IAttributeProvider).GetAttributes : 
                attributeType:System.Type * inherit:System.Boolean
                -> System.Collections.Generic.IList<System.Attribute>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.IAttributeProvider).GetAttributes : 
                inherit:System.Boolean
                -> System.Collections.Generic.IList<System.Attribute>

        
* Newtonsoft.Json.Serialization.IContractResolver

        Newtonsoft.Json.Serialization.IContractResolver (.NET type: Interface and base: none)

        (Instance/Inheritance of Newtonsoft.Json.Serialization.IContractResolver).ResolveContract : 
                type:System.Type
                -> Newtonsoft.Json.Serialization.JsonContract

        
* Newtonsoft.Json.Serialization.IReferenceResolver

        Newtonsoft.Json.Serialization.IReferenceResolver (.NET type: Interface and base: none)

        (Instance/Inheritance of Newtonsoft.Json.Serialization.IReferenceResolver).AddReference : 
                context:System.Object * reference:System.String * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.IReferenceResolver).GetReference : 
                context:System.Object * value:System.Object
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.IReferenceResolver).IsReferenced : 
                context:System.Object * value:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.IReferenceResolver).ResolveReference : 
                context:System.Object * reference:System.String
                -> System.Object

        
* Newtonsoft.Json.Serialization.ITraceWriter

        Newtonsoft.Json.Serialization.ITraceWriter (.NET type: Interface and base: none)

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ITraceWriter).LevelFilter : 
                System.Diagnostics.TraceLevel

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ITraceWriter).Trace : 
                level:System.Diagnostics.TraceLevel * message:System.String * ex:System.Exception
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ITraceWriter).get_LevelFilter : 
                System.Void
                -> System.Diagnostics.TraceLevel

        
* Newtonsoft.Json.Serialization.IValueProvider

        Newtonsoft.Json.Serialization.IValueProvider (.NET type: Interface and base: none)

        (Instance/Inheritance of Newtonsoft.Json.Serialization.IValueProvider).GetValue : 
                target:System.Object
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.IValueProvider).SetValue : 
                target:System.Object * value:System.Object
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonArrayContract

        Newtonsoft.Json.Serialization.JsonArrayContract (.NET type: Class and base: Newtonsoft.Json.Serialization.JsonContainerContract)

        new Newtonsoft.Json.Serialization.JsonArrayContract : 
                underlyingType:System.Type
                -> Newtonsoft.Json.Serialization.JsonArrayContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).CollectionItemType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).HasParameterizedCreator : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).IsMultidimensionalArray : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).ItemConverter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).ItemIsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).ItemReferenceLoopHandling : 
                System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).ItemTypeNameHandling : 
                System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OverrideCreator : 
                Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_CollectionItemType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_HasParameterizedCreator : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_IsMultidimensionalArray : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_ItemConverter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_ItemIsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_ItemReferenceLoopHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_ItemTypeNameHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OverrideCreator : 
                System.Void
                -> Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_HasParameterizedCreator : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_ItemConverter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_ItemIsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_ItemReferenceLoopHandling : 
                value:System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_ItemTypeNameHandling : 
                value:System.Nullable<Newtonsoft.Json.TypeNameHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_OverrideCreator : 
                value:Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonContainerContract

        Newtonsoft.Json.Serialization.JsonContainerContract (.NET type: Class and base: Newtonsoft.Json.Serialization.JsonContract)

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).ItemConverter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).ItemIsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).ItemReferenceLoopHandling : 
                System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).ItemTypeNameHandling : 
                System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_ItemConverter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_ItemIsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_ItemReferenceLoopHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_ItemTypeNameHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_ItemConverter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_ItemIsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_ItemReferenceLoopHandling : 
                value:System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_ItemTypeNameHandling : 
                value:System.Nullable<Newtonsoft.Json.TypeNameHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContainerContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonContract

        Newtonsoft.Json.Serialization.JsonContract (.NET type: Abstract and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonDictionaryContract

        Newtonsoft.Json.Serialization.JsonDictionaryContract (.NET type: Class and base: Newtonsoft.Json.Serialization.JsonContainerContract)

        new Newtonsoft.Json.Serialization.JsonDictionaryContract : 
                underlyingType:System.Type
                -> Newtonsoft.Json.Serialization.JsonDictionaryContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).DictionaryKeyResolver : 
                System.Func<System.String,System.String>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).DictionaryKeyType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).DictionaryValueType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).HasParameterizedCreator : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).ItemConverter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).ItemIsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).ItemReferenceLoopHandling : 
                System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).ItemTypeNameHandling : 
                System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OverrideCreator : 
                Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).PropertyNameResolver : 
                System.Func<System.String,System.String>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_DictionaryKeyResolver : 
                System.Void
                -> System.Func<System.String,System.String>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_DictionaryKeyType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_DictionaryValueType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_HasParameterizedCreator : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_ItemConverter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_ItemIsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_ItemReferenceLoopHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_ItemTypeNameHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OverrideCreator : 
                System.Void
                -> Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_PropertyNameResolver : 
                System.Void
                -> System.Func<System.String,System.String>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_DictionaryKeyResolver : 
                value:System.Func<System.String,System.String>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_HasParameterizedCreator : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_ItemConverter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_ItemIsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_ItemReferenceLoopHandling : 
                value:System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_ItemTypeNameHandling : 
                value:System.Nullable<Newtonsoft.Json.TypeNameHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_OverrideCreator : 
                value:Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_PropertyNameResolver : 
                value:System.Func<System.String,System.String>
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonDynamicContract

        Newtonsoft.Json.Serialization.JsonDynamicContract (.NET type: Class and base: Newtonsoft.Json.Serialization.JsonContainerContract)

        new Newtonsoft.Json.Serialization.JsonDynamicContract : 
                underlyingType:System.Type
                -> Newtonsoft.Json.Serialization.JsonDynamicContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).ItemConverter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).ItemIsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).ItemReferenceLoopHandling : 
                System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).ItemTypeNameHandling : 
                System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).Properties : 
                Newtonsoft.Json.Serialization.JsonPropertyCollection

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).PropertyNameResolver : 
                System.Func<System.String,System.String>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_ItemConverter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_ItemIsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_ItemReferenceLoopHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_ItemTypeNameHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_Properties : 
                System.Void
                -> Newtonsoft.Json.Serialization.JsonPropertyCollection

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_PropertyNameResolver : 
                System.Void
                -> System.Func<System.String,System.String>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_ItemConverter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_ItemIsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_ItemReferenceLoopHandling : 
                value:System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_ItemTypeNameHandling : 
                value:System.Nullable<Newtonsoft.Json.TypeNameHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDynamicContract).set_PropertyNameResolver : 
                value:System.Func<System.String,System.String>
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonISerializableContract

        Newtonsoft.Json.Serialization.JsonISerializableContract (.NET type: Class and base: Newtonsoft.Json.Serialization.JsonContainerContract)

        new Newtonsoft.Json.Serialization.JsonISerializableContract : 
                underlyingType:System.Type
                -> Newtonsoft.Json.Serialization.JsonISerializableContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).ISerializableCreator : 
                Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).ItemConverter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).ItemIsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).ItemReferenceLoopHandling : 
                System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).ItemTypeNameHandling : 
                System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_ISerializableCreator : 
                System.Void
                -> Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_ItemConverter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_ItemIsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_ItemReferenceLoopHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_ItemTypeNameHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_ISerializableCreator : 
                value:Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_ItemConverter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_ItemIsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_ItemReferenceLoopHandling : 
                value:System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_ItemTypeNameHandling : 
                value:System.Nullable<Newtonsoft.Json.TypeNameHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonISerializableContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonLinqContract

        Newtonsoft.Json.Serialization.JsonLinqContract (.NET type: Class and base: Newtonsoft.Json.Serialization.JsonContract)

        new Newtonsoft.Json.Serialization.JsonLinqContract : 
                underlyingType:System.Type
                -> Newtonsoft.Json.Serialization.JsonLinqContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonLinqContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonObjectContract

        Newtonsoft.Json.Serialization.JsonObjectContract (.NET type: Class and base: Newtonsoft.Json.Serialization.JsonContainerContract)

        new Newtonsoft.Json.Serialization.JsonObjectContract : 
                underlyingType:System.Type
                -> Newtonsoft.Json.Serialization.JsonObjectContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ConstructorParameters : 
                Newtonsoft.Json.Serialization.JsonPropertyCollection

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).CreatorParameters : 
                Newtonsoft.Json.Serialization.JsonPropertyCollection

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ExtensionDataGetter : 
                Newtonsoft.Json.Serialization.ExtensionDataGetter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ExtensionDataSetter : 
                Newtonsoft.Json.Serialization.ExtensionDataSetter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ExtensionDataValueType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ItemConverter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ItemIsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ItemReferenceLoopHandling : 
                System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ItemRequired : 
                System.Nullable<Newtonsoft.Json.Required>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ItemTypeNameHandling : 
                System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).MemberSerialization : 
                Newtonsoft.Json.MemberSerialization

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OverrideConstructor : 
                System.Reflection.ConstructorInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).OverrideCreator : 
                Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ParametrizedConstructor : 
                System.Reflection.ConstructorInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).Properties : 
                Newtonsoft.Json.Serialization.JsonPropertyCollection

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ConstructorParameters : 
                System.Void
                -> Newtonsoft.Json.Serialization.JsonPropertyCollection

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_CreatorParameters : 
                System.Void
                -> Newtonsoft.Json.Serialization.JsonPropertyCollection

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ExtensionDataGetter : 
                System.Void
                -> Newtonsoft.Json.Serialization.ExtensionDataGetter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ExtensionDataSetter : 
                System.Void
                -> Newtonsoft.Json.Serialization.ExtensionDataSetter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ExtensionDataValueType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ItemConverter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ItemIsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ItemReferenceLoopHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ItemRequired : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.Required>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ItemTypeNameHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_MemberSerialization : 
                System.Void
                -> Newtonsoft.Json.MemberSerialization

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OverrideConstructor : 
                System.Void
                -> System.Reflection.ConstructorInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_OverrideCreator : 
                System.Void
                -> Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ParametrizedConstructor : 
                System.Void
                -> System.Reflection.ConstructorInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_Properties : 
                System.Void
                -> Newtonsoft.Json.Serialization.JsonPropertyCollection

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ExtensionDataGetter : 
                value:Newtonsoft.Json.Serialization.ExtensionDataGetter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ExtensionDataSetter : 
                value:Newtonsoft.Json.Serialization.ExtensionDataSetter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ExtensionDataValueType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ItemConverter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ItemIsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ItemReferenceLoopHandling : 
                value:System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ItemRequired : 
                value:System.Nullable<Newtonsoft.Json.Required>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ItemTypeNameHandling : 
                value:System.Nullable<Newtonsoft.Json.TypeNameHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_MemberSerialization : 
                value:Newtonsoft.Json.MemberSerialization
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_OverrideConstructor : 
                value:System.Reflection.ConstructorInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_OverrideCreator : 
                value:Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ParametrizedConstructor : 
                value:System.Reflection.ConstructorInfo
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonPrimitiveContract

        Newtonsoft.Json.Serialization.JsonPrimitiveContract (.NET type: Class and base: Newtonsoft.Json.Serialization.JsonContract)

        new Newtonsoft.Json.Serialization.JsonPrimitiveContract : 
                underlyingType:System.Type
                -> Newtonsoft.Json.Serialization.JsonPrimitiveContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPrimitiveContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonProperty

        Newtonsoft.Json.Serialization.JsonProperty (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Serialization.JsonProperty : 
                System.Void
                -> Newtonsoft.Json.Serialization.JsonProperty

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).AttributeProvider : 
                Newtonsoft.Json.Serialization.IAttributeProvider

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).DeclaringType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).DefaultValue : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).DefaultValueHandling : 
                System.Nullable<Newtonsoft.Json.DefaultValueHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).GetIsSpecified : 
                System.Predicate<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).HasMemberAttribute : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).Ignored : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ItemConverter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ItemIsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ItemReferenceLoopHandling : 
                System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ItemTypeNameHandling : 
                System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).MemberConverter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).NullValueHandling : 
                System.Nullable<Newtonsoft.Json.NullValueHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ObjectCreationHandling : 
                System.Nullable<Newtonsoft.Json.ObjectCreationHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).Order : 
                System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).PropertyName : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).PropertyType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).Readable : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ReferenceLoopHandling : 
                System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).Required : 
                Newtonsoft.Json.Required

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).SetIsSpecified : 
                System.Action<System.Object,System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ShouldDeserialize : 
                System.Predicate<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ShouldSerialize : 
                System.Predicate<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).TypeNameHandling : 
                System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).UnderlyingName : 
                System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ValueProvider : 
                Newtonsoft.Json.Serialization.IValueProvider

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).Writable : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_AttributeProvider : 
                System.Void
                -> Newtonsoft.Json.Serialization.IAttributeProvider

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_DeclaringType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_DefaultValue : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_DefaultValueHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.DefaultValueHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_GetIsSpecified : 
                System.Void
                -> System.Predicate<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_HasMemberAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_Ignored : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ItemConverter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ItemIsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ItemReferenceLoopHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ItemTypeNameHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_MemberConverter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_NullValueHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.NullValueHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ObjectCreationHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.ObjectCreationHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_Order : 
                System.Void
                -> System.Nullable<System.Int32>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_PropertyName : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_PropertyType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_Readable : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ReferenceLoopHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_Required : 
                System.Void
                -> Newtonsoft.Json.Required

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_SetIsSpecified : 
                System.Void
                -> System.Action<System.Object,System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ShouldDeserialize : 
                System.Void
                -> System.Predicate<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ShouldSerialize : 
                System.Void
                -> System.Predicate<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_TypeNameHandling : 
                System.Void
                -> System.Nullable<Newtonsoft.Json.TypeNameHandling>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_UnderlyingName : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ValueProvider : 
                System.Void
                -> Newtonsoft.Json.Serialization.IValueProvider

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_Writable : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_AttributeProvider : 
                value:Newtonsoft.Json.Serialization.IAttributeProvider
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_DeclaringType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_DefaultValue : 
                value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_DefaultValueHandling : 
                value:System.Nullable<Newtonsoft.Json.DefaultValueHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_GetIsSpecified : 
                value:System.Predicate<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_HasMemberAttribute : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_Ignored : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ItemConverter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ItemIsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ItemReferenceLoopHandling : 
                value:System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ItemTypeNameHandling : 
                value:System.Nullable<Newtonsoft.Json.TypeNameHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_MemberConverter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_NullValueHandling : 
                value:System.Nullable<Newtonsoft.Json.NullValueHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ObjectCreationHandling : 
                value:System.Nullable<Newtonsoft.Json.ObjectCreationHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_Order : 
                value:System.Nullable<System.Int32>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_PropertyName : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_PropertyType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_Readable : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ReferenceLoopHandling : 
                value:System.Nullable<Newtonsoft.Json.ReferenceLoopHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_Required : 
                value:Newtonsoft.Json.Required
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_SetIsSpecified : 
                value:System.Action<System.Object,System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ShouldDeserialize : 
                value:System.Predicate<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ShouldSerialize : 
                value:System.Predicate<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_TypeNameHandling : 
                value:System.Nullable<Newtonsoft.Json.TypeNameHandling>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_UnderlyingName : 
                value:System.String
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ValueProvider : 
                value:Newtonsoft.Json.Serialization.IValueProvider
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_Writable : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonPropertyCollection

        Newtonsoft.Json.Serialization.JsonPropertyCollection (.NET type: Class and base: System.Collections.ObjectModel.KeyedCollection<System.String,Newtonsoft.Json.Serialization.JsonProperty>)

        new Newtonsoft.Json.Serialization.JsonPropertyCollection : 
                type:System.Type
                -> Newtonsoft.Json.Serialization.JsonPropertyCollection

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Add : 
                item:Newtonsoft.Json.Serialization.JsonProperty
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).AddProperty : 
                property:Newtonsoft.Json.Serialization.JsonProperty
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Clear : 
                System.Void
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Comparer : 
                System.Collections.Generic.IEqualityComparer<System.String>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Contains : 
                item:Newtonsoft.Json.Serialization.JsonProperty
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Contains : 
                key:System.String
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).CopyTo : 
                array:Newtonsoft.Json.Serialization.JsonProperty[] * index:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Count : 
                System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).GetClosestMatchProperty : 
                propertyName:System.String
                -> Newtonsoft.Json.Serialization.JsonProperty

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).GetEnumerator : 
                System.Void
                -> System.Collections.Generic.IEnumerator<Newtonsoft.Json.Serialization.JsonProperty>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).GetProperty : 
                propertyName:System.String * comparisonType:System.StringComparison
                -> Newtonsoft.Json.Serialization.JsonProperty

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).IndexOf : 
                item:Newtonsoft.Json.Serialization.JsonProperty
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Insert : 
                index:System.Int32 * item:Newtonsoft.Json.Serialization.JsonProperty
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Item : 
                Newtonsoft.Json.Serialization.JsonProperty

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Remove : 
                item:Newtonsoft.Json.Serialization.JsonProperty
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).Remove : 
                key:System.String
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).RemoveAt : 
                index:System.Int32
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).get_Comparer : 
                System.Void
                -> System.Collections.Generic.IEqualityComparer<System.String>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).get_Count : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).get_Item : 
                index:System.Int32
                -> Newtonsoft.Json.Serialization.JsonProperty

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).get_Item : 
                key:System.String
                -> Newtonsoft.Json.Serialization.JsonProperty

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonPropertyCollection).set_Item : 
                index:System.Int32 * value:Newtonsoft.Json.Serialization.JsonProperty
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonStringContract

        Newtonsoft.Json.Serialization.JsonStringContract (.NET type: Class and base: Newtonsoft.Json.Serialization.JsonPrimitiveContract)

        new Newtonsoft.Json.Serialization.JsonStringContract : 
                underlyingType:System.Type
                -> Newtonsoft.Json.Serialization.JsonStringContract

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).Converter : 
                Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).CreatedType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).DefaultCreator : 
                System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).DefaultCreatorNonPublic : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).IsReference : 
                System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnDeserialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnDeserializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnDeserializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnDeserializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnError : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnErrorCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnSerialized : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnSerializedCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnSerializing : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).OnSerializingCallbacks : 
                System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).UnderlyingType : 
                System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_Converter : 
                System.Void
                -> Newtonsoft.Json.JsonConverter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_CreatedType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_DefaultCreator : 
                System.Void
                -> System.Func<System.Object>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_DefaultCreatorNonPublic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_IsReference : 
                System.Void
                -> System.Nullable<System.Boolean>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnDeserialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnDeserializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnDeserializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnDeserializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnError : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnErrorCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationErrorCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnSerialized : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnSerializedCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnSerializing : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_OnSerializingCallbacks : 
                System.Void
                -> System.Collections.Generic.IList<Newtonsoft.Json.Serialization.SerializationCallback>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).get_UnderlyingType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_Converter : 
                value:Newtonsoft.Json.JsonConverter
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_CreatedType : 
                value:System.Type
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_DefaultCreator : 
                value:System.Func<System.Object>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_DefaultCreatorNonPublic : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_IsReference : 
                value:System.Nullable<System.Boolean>
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_OnDeserialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_OnDeserializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_OnError : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_OnSerialized : 
                value:System.Reflection.MethodInfo
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonStringContract).set_OnSerializing : 
                value:System.Reflection.MethodInfo
                -> System.Void

        
* Newtonsoft.Json.Serialization.MemoryTraceWriter

        Newtonsoft.Json.Serialization.MemoryTraceWriter (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Serialization.MemoryTraceWriter : 
                System.Void
                -> Newtonsoft.Json.Serialization.MemoryTraceWriter

        (Instance/Inheritance of Newtonsoft.Json.Serialization.MemoryTraceWriter).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.MemoryTraceWriter).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.MemoryTraceWriter).GetTraceMessages : 
                System.Void
                -> System.Collections.Generic.IEnumerable<System.String>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.MemoryTraceWriter).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.MemoryTraceWriter).LevelFilter : 
                System.Diagnostics.TraceLevel

        (Instance/Inheritance of Newtonsoft.Json.Serialization.MemoryTraceWriter).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.MemoryTraceWriter).Trace : 
                level:System.Diagnostics.TraceLevel * message:System.String * ex:System.Exception
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.MemoryTraceWriter).get_LevelFilter : 
                System.Void
                -> System.Diagnostics.TraceLevel

        (Instance/Inheritance of Newtonsoft.Json.Serialization.MemoryTraceWriter).set_LevelFilter : 
                value:System.Diagnostics.TraceLevel
                -> System.Void

        
* Newtonsoft.Json.Serialization.NamingStrategy

        Newtonsoft.Json.Serialization.NamingStrategy (.NET type: Abstract and base: System.Object)

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).GetDictionaryKey : 
                key:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).GetPropertyName : 
                name:System.String * hasSpecifiedName:System.Boolean
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).OverrideSpecifiedNames : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).ProcessDictionaryKeys : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).get_OverrideSpecifiedNames : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).get_ProcessDictionaryKeys : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).set_OverrideSpecifiedNames : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).set_ProcessDictionaryKeys : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.ObjectConstructor<'T>

        Newtonsoft.Json.Serialization.ObjectConstructor<'T> (.NET type: Class and base: System.MulticastDelegate)

        new Newtonsoft.Json.Serialization.ObjectConstructor<'T> : 
                object:System.Object * method:System.IntPtr
                -> Newtonsoft.Json.Serialization.ObjectConstructor<'T>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).BeginInvoke : 
                args:System.Object[] * callback:System.AsyncCallback * object:System.Object
                -> System.IAsyncResult

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).Clone : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).DynamicInvoke : 
                args:System.Object[]
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).EndInvoke : 
                result:System.IAsyncResult
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).GetInvocationList : 
                System.Void
                -> System.Delegate[]

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).Invoke : 
                args:System.Object[]
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).Method : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).Target : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).get_Method : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ObjectConstructor<'T>).get_Target : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.Serialization.OnErrorAttribute

        Newtonsoft.Json.Serialization.OnErrorAttribute (.NET type: Class and base: System.Attribute)

        new Newtonsoft.Json.Serialization.OnErrorAttribute : 
                System.Void
                -> Newtonsoft.Json.Serialization.OnErrorAttribute

        (Instance/Inheritance of Newtonsoft.Json.Serialization.OnErrorAttribute).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.OnErrorAttribute).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.OnErrorAttribute).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.OnErrorAttribute).IsDefaultAttribute : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.OnErrorAttribute).Match : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.OnErrorAttribute).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.OnErrorAttribute).TypeId : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.OnErrorAttribute).get_TypeId : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.Serialization.ReflectionAttributeProvider

        Newtonsoft.Json.Serialization.ReflectionAttributeProvider (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Serialization.ReflectionAttributeProvider : 
                attributeProvider:System.Object
                -> Newtonsoft.Json.Serialization.ReflectionAttributeProvider

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionAttributeProvider).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionAttributeProvider).GetAttributes : 
                attributeType:System.Type * inherit:System.Boolean
                -> System.Collections.Generic.IList<System.Attribute>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionAttributeProvider).GetAttributes : 
                inherit:System.Boolean
                -> System.Collections.Generic.IList<System.Attribute>

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionAttributeProvider).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionAttributeProvider).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionAttributeProvider).ToString : 
                System.Void
                -> System.String

        
* Newtonsoft.Json.Serialization.ReflectionValueProvider

        Newtonsoft.Json.Serialization.ReflectionValueProvider (.NET type: Class and base: System.Object)

        new Newtonsoft.Json.Serialization.ReflectionValueProvider : 
                memberInfo:System.Reflection.MemberInfo
                -> Newtonsoft.Json.Serialization.ReflectionValueProvider

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionValueProvider).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionValueProvider).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionValueProvider).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionValueProvider).GetValue : 
                target:System.Object
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionValueProvider).SetValue : 
                target:System.Object * value:System.Object
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.ReflectionValueProvider).ToString : 
                System.Void
                -> System.String

        
* Newtonsoft.Json.Serialization.SerializationCallback

        Newtonsoft.Json.Serialization.SerializationCallback (.NET type: Class and base: System.MulticastDelegate)

        new Newtonsoft.Json.Serialization.SerializationCallback : 
                object:System.Object * method:System.IntPtr
                -> Newtonsoft.Json.Serialization.SerializationCallback

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).BeginInvoke : 
                o:System.Object * context:System.Runtime.Serialization.StreamingContext * callback:System.AsyncCallback * object:System.Object
                -> System.IAsyncResult

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).Clone : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).DynamicInvoke : 
                args:System.Object[]
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).EndInvoke : 
                result:System.IAsyncResult
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).GetInvocationList : 
                System.Void
                -> System.Delegate[]

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).Invoke : 
                o:System.Object * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).Method : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).Target : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).get_Method : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationCallback).get_Target : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.Serialization.SerializationErrorCallback

        Newtonsoft.Json.Serialization.SerializationErrorCallback (.NET type: Class and base: System.MulticastDelegate)

        new Newtonsoft.Json.Serialization.SerializationErrorCallback : 
                object:System.Object * method:System.IntPtr
                -> Newtonsoft.Json.Serialization.SerializationErrorCallback

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).BeginInvoke : 
                o:System.Object * context:System.Runtime.Serialization.StreamingContext * errorContext:Newtonsoft.Json.Serialization.ErrorContext * callback:System.AsyncCallback * object:System.Object
                -> System.IAsyncResult

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).Clone : 
                System.Void
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).DynamicInvoke : 
                args:System.Object[]
                -> System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).EndInvoke : 
                result:System.IAsyncResult
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).GetInvocationList : 
                System.Void
                -> System.Delegate[]

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).GetObjectData : 
                info:System.Runtime.Serialization.SerializationInfo * context:System.Runtime.Serialization.StreamingContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).Invoke : 
                o:System.Object * context:System.Runtime.Serialization.StreamingContext * errorContext:Newtonsoft.Json.Serialization.ErrorContext
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).Method : 
                System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).Target : 
                System.Object

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).get_Method : 
                System.Void
                -> System.Reflection.MethodInfo

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SerializationErrorCallback).get_Target : 
                System.Void
                -> System.Object

        
* Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy

        Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy (.NET type: Class and base: Newtonsoft.Json.Serialization.NamingStrategy)

        new Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy

        new Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy : 
                processDictionaryKeys:System.Boolean * overrideSpecifiedNames:System.Boolean
                -> Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).GetDictionaryKey : 
                key:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).GetPropertyName : 
                name:System.String * hasSpecifiedName:System.Boolean
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).OverrideSpecifiedNames : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).ProcessDictionaryKeys : 
                System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).get_OverrideSpecifiedNames : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).get_ProcessDictionaryKeys : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).set_OverrideSpecifiedNames : 
                value:System.Boolean
                -> System.Void

        (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).set_ProcessDictionaryKeys : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.StringEscapeHandling

        Newtonsoft.Json.StringEscapeHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.StringEscapeHandling values: [ Default:0; EscapeHtml:2; EscapeNonAscii:1 ]

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.StringEscapeHandling).value__ : 
                System.Int32

        Newtonsoft.Json.StringEscapeHandling.Default : 
                Newtonsoft.Json.StringEscapeHandling

        Newtonsoft.Json.StringEscapeHandling.EscapeHtml : 
                Newtonsoft.Json.StringEscapeHandling

        Newtonsoft.Json.StringEscapeHandling.EscapeNonAscii : 
                Newtonsoft.Json.StringEscapeHandling

        
* Newtonsoft.Json.TypeNameHandling

        Newtonsoft.Json.TypeNameHandling (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.TypeNameHandling values: [ All:3; Arrays:2; Auto:4; None:0; Objects:1 ]

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.TypeNameHandling).value__ : 
                System.Int32

        Newtonsoft.Json.TypeNameHandling.All : 
                Newtonsoft.Json.TypeNameHandling

        Newtonsoft.Json.TypeNameHandling.Arrays : 
                Newtonsoft.Json.TypeNameHandling

        Newtonsoft.Json.TypeNameHandling.Auto : 
                Newtonsoft.Json.TypeNameHandling

        Newtonsoft.Json.TypeNameHandling.None : 
                Newtonsoft.Json.TypeNameHandling

        Newtonsoft.Json.TypeNameHandling.Objects : 
                Newtonsoft.Json.TypeNameHandling

        
* Newtonsoft.Json.WriteState

        Newtonsoft.Json.WriteState (.NET type: Enum and base: System.Enum)

        Newtonsoft.Json.WriteState values: [ Array:3; Closed:1; Constructor:4; Error:0; Object:2; Property:5; Start:6 ]

        (Instance/Inheritance of Newtonsoft.Json.WriteState).CompareTo : 
                target:System.Object
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.WriteState).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.WriteState).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of Newtonsoft.Json.WriteState).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of Newtonsoft.Json.WriteState).GetTypeCode : 
                System.Void
                -> System.TypeCode

        (Instance/Inheritance of Newtonsoft.Json.WriteState).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        (Instance/Inheritance of Newtonsoft.Json.WriteState).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.WriteState).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.WriteState).ToString : 
                format:System.String
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.WriteState).ToString : 
                provider:System.IFormatProvider
                -> System.String

        (Instance/Inheritance of Newtonsoft.Json.WriteState).value__ : 
                System.Int32

        Newtonsoft.Json.WriteState.Array : 
                Newtonsoft.Json.WriteState

        Newtonsoft.Json.WriteState.Closed : 
                Newtonsoft.Json.WriteState

        Newtonsoft.Json.WriteState.Constructor : 
                Newtonsoft.Json.WriteState

        Newtonsoft.Json.WriteState.Error : 
                Newtonsoft.Json.WriteState

        Newtonsoft.Json.WriteState.Object : 
                Newtonsoft.Json.WriteState

        Newtonsoft.Json.WriteState.Property : 
                Newtonsoft.Json.WriteState

        Newtonsoft.Json.WriteState.Start : 
                Newtonsoft.Json.WriteState
