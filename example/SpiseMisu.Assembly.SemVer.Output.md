### 10.0.0.0 - Dec 02 2016 - Major

        
* Newtonsoft.Json.Bson.BsonReader

        - (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        - (Instance/Inheritance of Newtonsoft.Json.Bson.BsonReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        
* Newtonsoft.Json.Converters.StringEnumConverter

        - new Newtonsoft.Json.Converters.StringEnumConverter : 
                camelCaseText:System.Boolean
                -> Newtonsoft.Json.Converters.StringEnumConverter

        
* Newtonsoft.Json.IArrayPool<'T>

        Removed: Newtonsoft.Json.IArrayPool<'T> (.NET type: Interface and base: none)

        - (Instance/Inheritance of Newtonsoft.Json.IArrayPool<'T>).Rent : 
                minimumLength:System.Int32
                -> T[]

        - (Instance/Inheritance of Newtonsoft.Json.IArrayPool<'T>).Return : 
                array:T[]
                -> System.Void

        
* Newtonsoft.Json.JsonArrayAttribute

        - (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).NamingStrategyParameters : 
                System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).NamingStrategyType : 
                System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.JsonArrayAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        
* Newtonsoft.Json.JsonContainerAttribute

        - (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).NamingStrategyParameters : 
                System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).NamingStrategyType : 
                System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.JsonContainerAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        
* Newtonsoft.Json.JsonDictionaryAttribute

        - (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).NamingStrategyParameters : 
                System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).NamingStrategyType : 
                System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.JsonDictionaryAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        
* Newtonsoft.Json.JsonObjectAttribute

        - (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).NamingStrategyParameters : 
                System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).NamingStrategyType : 
                System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.JsonObjectAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        
* Newtonsoft.Json.JsonPropertyAttribute

        - (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).NamingStrategyParameters : 
                System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).NamingStrategyType : 
                System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_NamingStrategyParameters : 
                System.Void
                -> System.Object[]

        - (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).get_NamingStrategyType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_NamingStrategyParameters : 
                value:System.Object[]
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.JsonPropertyAttribute).set_NamingStrategyType : 
                value:System.Type
                -> System.Void

        
* Newtonsoft.Json.JsonReader

        - (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        - (Instance/Inheritance of Newtonsoft.Json.JsonReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        
* Newtonsoft.Json.JsonTextReader

        - (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ArrayPool : 
                Newtonsoft.Json.IArrayPool<System.Char>

        - (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        - (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        - (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).get_ArrayPool : 
                System.Void
                -> Newtonsoft.Json.IArrayPool<System.Char>

        - (Instance/Inheritance of Newtonsoft.Json.JsonTextReader).set_ArrayPool : 
                value:Newtonsoft.Json.IArrayPool<System.Char>
                -> System.Void

        
* Newtonsoft.Json.JsonTextWriter

        - (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).ArrayPool : 
                Newtonsoft.Json.IArrayPool<System.Char>

        - (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).get_ArrayPool : 
                System.Void
                -> Newtonsoft.Json.IArrayPool<System.Char>

        - (Instance/Inheritance of Newtonsoft.Json.JsonTextWriter).set_ArrayPool : 
                value:Newtonsoft.Json.IArrayPool<System.Char>
                -> System.Void

        
* Newtonsoft.Json.JsonValidatingReader

        - (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        - (Instance/Inheritance of Newtonsoft.Json.JsonValidatingReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        
* Newtonsoft.Json.Linq.CommentHandling

        Removed: Newtonsoft.Json.Linq.CommentHandling (.NET type: Enum and base: System.Enum)

        Removed: Newtonsoft.Json.Linq.CommentHandling values: [ Ignore:0; Load:1 ]

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).GetHashCode : 
                System.Void
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).GetType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).ToString : 
                System.Void
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).ToString : 
                format:System.String
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.CommentHandling).value__ : 
                System.Int32

        - Newtonsoft.Json.Linq.CommentHandling.Ignore : 
                Newtonsoft.Json.Linq.CommentHandling

        - Newtonsoft.Json.Linq.CommentHandling.Load : 
                Newtonsoft.Json.Linq.CommentHandling

        
* Newtonsoft.Json.Linq.JArray

        - Newtonsoft.Json.Linq.JArray.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JArray

        - Newtonsoft.Json.Linq.JArray.Parse : 
                json:System.String * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JArray

        
* Newtonsoft.Json.Linq.JConstructor

        - Newtonsoft.Json.Linq.JConstructor.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JConstructor

        
* Newtonsoft.Json.Linq.JObject

        - Newtonsoft.Json.Linq.JObject.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JObject

        - Newtonsoft.Json.Linq.JObject.Parse : 
                json:System.String * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JObject

        
* Newtonsoft.Json.Linq.JProperty

        - Newtonsoft.Json.Linq.JProperty.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JProperty

        
* Newtonsoft.Json.Linq.JToken

        - Newtonsoft.Json.Linq.JToken.Load : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JToken

        - Newtonsoft.Json.Linq.JToken.Parse : 
                json:System.String * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JToken

        - Newtonsoft.Json.Linq.JToken.ReadFrom : 
                reader:Newtonsoft.Json.JsonReader * settings:Newtonsoft.Json.Linq.JsonLoadSettings
                -> Newtonsoft.Json.Linq.JToken

        
* Newtonsoft.Json.Linq.JTokenReader

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsBoolean : 
                System.Void
                -> System.Nullable<System.Boolean>

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JTokenReader).ReadAsDouble : 
                System.Void
                -> System.Nullable<System.Double>

        
* Newtonsoft.Json.Linq.JsonLoadSettings

        Removed: Newtonsoft.Json.Linq.JsonLoadSettings (.NET type: Class and base: System.Object)

        - new Newtonsoft.Json.Linq.JsonLoadSettings : 
                System.Void
                -> Newtonsoft.Json.Linq.JsonLoadSettings

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).CommentHandling : 
                Newtonsoft.Json.Linq.CommentHandling

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).Equals : 
                obj:System.Object
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).GetHashCode : 
                System.Void
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).GetType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).LineInfoHandling : 
                Newtonsoft.Json.Linq.LineInfoHandling

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).ToString : 
                System.Void
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).get_CommentHandling : 
                System.Void
                -> Newtonsoft.Json.Linq.CommentHandling

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).get_LineInfoHandling : 
                System.Void
                -> Newtonsoft.Json.Linq.LineInfoHandling

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).set_CommentHandling : 
                value:Newtonsoft.Json.Linq.CommentHandling
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonLoadSettings).set_LineInfoHandling : 
                value:Newtonsoft.Json.Linq.LineInfoHandling
                -> System.Void

        
* Newtonsoft.Json.Linq.JsonMergeSettings

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).MergeNullValueHandling : 
                Newtonsoft.Json.Linq.MergeNullValueHandling

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).get_MergeNullValueHandling : 
                System.Void
                -> Newtonsoft.Json.Linq.MergeNullValueHandling

        - (Instance/Inheritance of Newtonsoft.Json.Linq.JsonMergeSettings).set_MergeNullValueHandling : 
                value:Newtonsoft.Json.Linq.MergeNullValueHandling
                -> System.Void

        
* Newtonsoft.Json.Linq.LineInfoHandling

        Removed: Newtonsoft.Json.Linq.LineInfoHandling (.NET type: Enum and base: System.Enum)

        Removed: Newtonsoft.Json.Linq.LineInfoHandling values: [ Ignore:0; Load:1 ]

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).GetHashCode : 
                System.Void
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).GetType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).ToString : 
                System.Void
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).ToString : 
                format:System.String
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.LineInfoHandling).value__ : 
                System.Int32

        - Newtonsoft.Json.Linq.LineInfoHandling.Ignore : 
                Newtonsoft.Json.Linq.LineInfoHandling

        - Newtonsoft.Json.Linq.LineInfoHandling.Load : 
                Newtonsoft.Json.Linq.LineInfoHandling

        
* Newtonsoft.Json.Linq.MergeNullValueHandling

        Removed: Newtonsoft.Json.Linq.MergeNullValueHandling (.NET type: Enum and base: System.Enum)

        Removed: Newtonsoft.Json.Linq.MergeNullValueHandling values: [ Ignore:0; Merge:1 ]

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).CompareTo : 
                target:System.Object
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).Equals : 
                obj:System.Object
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).GetHashCode : 
                System.Void
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).GetType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).GetTypeCode : 
                System.Void
                -> System.TypeCode

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).HasFlag : 
                flag:System.Enum
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).ToString : 
                System.Void
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).ToString : 
                format:System.String * provider:System.IFormatProvider
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).ToString : 
                format:System.String
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).ToString : 
                provider:System.IFormatProvider
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Linq.MergeNullValueHandling).value__ : 
                System.Int32

        - Newtonsoft.Json.Linq.MergeNullValueHandling.Ignore : 
                Newtonsoft.Json.Linq.MergeNullValueHandling

        - Newtonsoft.Json.Linq.MergeNullValueHandling.Merge : 
                Newtonsoft.Json.Linq.MergeNullValueHandling

        
* Newtonsoft.Json.Required

        Added: Newtonsoft.Json.Required values: [ AllowNull:1; Always:2; Default:0 ]

        Removed: Newtonsoft.Json.Required values: [ AllowNull:1; Always:2; Default:0; DisallowNull:3 ]

        - Newtonsoft.Json.Required.DisallowNull : 
                Newtonsoft.Json.Required

        
* Newtonsoft.Json.Serialization.CamelCaseNamingStrategy

        Removed: Newtonsoft.Json.Serialization.CamelCaseNamingStrategy (.NET type: Class and base: Newtonsoft.Json.Serialization.NamingStrategy)

        - new Newtonsoft.Json.Serialization.CamelCaseNamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.CamelCaseNamingStrategy

        - new Newtonsoft.Json.Serialization.CamelCaseNamingStrategy : 
                processDictionaryKeys:System.Boolean * overrideSpecifiedNames:System.Boolean
                -> Newtonsoft.Json.Serialization.CamelCaseNamingStrategy

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).Equals : 
                obj:System.Object
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).GetDictionaryKey : 
                key:System.String
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).GetHashCode : 
                System.Void
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).GetPropertyName : 
                name:System.String * hasSpecifiedName:System.Boolean
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).GetType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).OverrideSpecifiedNames : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).ProcessDictionaryKeys : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).ToString : 
                System.Void
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).get_OverrideSpecifiedNames : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).get_ProcessDictionaryKeys : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).set_OverrideSpecifiedNames : 
                value:System.Boolean
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCaseNamingStrategy).set_ProcessDictionaryKeys : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).NamingStrategy : 
                Newtonsoft.Json.Serialization.NamingStrategy

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).get_NamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.NamingStrategy

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver).set_NamingStrategy : 
                value:Newtonsoft.Json.Serialization.NamingStrategy
                -> System.Void

        
* Newtonsoft.Json.Serialization.DefaultContractResolver

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).NamingStrategy : 
                Newtonsoft.Json.Serialization.NamingStrategy

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).get_NamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.NamingStrategy

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultContractResolver).set_NamingStrategy : 
                value:Newtonsoft.Json.Serialization.NamingStrategy
                -> System.Void

        
* Newtonsoft.Json.Serialization.DefaultNamingStrategy

        Removed: Newtonsoft.Json.Serialization.DefaultNamingStrategy (.NET type: Class and base: Newtonsoft.Json.Serialization.NamingStrategy)

        - new Newtonsoft.Json.Serialization.DefaultNamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.DefaultNamingStrategy

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).Equals : 
                obj:System.Object
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).GetDictionaryKey : 
                key:System.String
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).GetHashCode : 
                System.Void
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).GetPropertyName : 
                name:System.String * hasSpecifiedName:System.Boolean
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).GetType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).OverrideSpecifiedNames : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).ProcessDictionaryKeys : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).ToString : 
                System.Void
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).get_OverrideSpecifiedNames : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).get_ProcessDictionaryKeys : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).set_OverrideSpecifiedNames : 
                value:System.Boolean
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.DefaultNamingStrategy).set_ProcessDictionaryKeys : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonArrayContract

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).HasParameterizedCreator : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).OverrideCreator : 
                Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_HasParameterizedCreator : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).get_OverrideCreator : 
                System.Void
                -> Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_HasParameterizedCreator : 
                value:System.Boolean
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonArrayContract).set_OverrideCreator : 
                value:Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonDictionaryContract

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).HasParameterizedCreator : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).OverrideCreator : 
                Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_HasParameterizedCreator : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).get_OverrideCreator : 
                System.Void
                -> Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_HasParameterizedCreator : 
                value:System.Boolean
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonDictionaryContract).set_OverrideCreator : 
                value:Newtonsoft.Json.Serialization.ObjectConstructor<System.Object>
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonObjectContract

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).ExtensionDataValueType : 
                System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).get_ExtensionDataValueType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonObjectContract).set_ExtensionDataValueType : 
                value:System.Type
                -> System.Void

        
* Newtonsoft.Json.Serialization.JsonProperty

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).ShouldDeserialize : 
                System.Predicate<System.Object>

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).get_ShouldDeserialize : 
                System.Void
                -> System.Predicate<System.Object>

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.JsonProperty).set_ShouldDeserialize : 
                value:System.Predicate<System.Object>
                -> System.Void

        
* Newtonsoft.Json.Serialization.NamingStrategy

        Removed: Newtonsoft.Json.Serialization.NamingStrategy (.NET type: Abstract and base: System.Object)

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).Equals : 
                obj:System.Object
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).GetDictionaryKey : 
                key:System.String
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).GetHashCode : 
                System.Void
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).GetPropertyName : 
                name:System.String * hasSpecifiedName:System.Boolean
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).GetType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).OverrideSpecifiedNames : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).ProcessDictionaryKeys : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).ToString : 
                System.Void
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).get_OverrideSpecifiedNames : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).get_ProcessDictionaryKeys : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).set_OverrideSpecifiedNames : 
                value:System.Boolean
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.NamingStrategy).set_ProcessDictionaryKeys : 
                value:System.Boolean
                -> System.Void

        
* Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy

        Removed: Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy (.NET type: Class and base: Newtonsoft.Json.Serialization.NamingStrategy)

        - new Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy : 
                System.Void
                -> Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy

        - new Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy : 
                processDictionaryKeys:System.Boolean * overrideSpecifiedNames:System.Boolean
                -> Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).Equals : 
                obj:System.Object
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).GetDictionaryKey : 
                key:System.String
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).GetHashCode : 
                System.Void
                -> System.Int32

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).GetPropertyName : 
                name:System.String * hasSpecifiedName:System.Boolean
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).GetType : 
                System.Void
                -> System.Type

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).OverrideSpecifiedNames : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).ProcessDictionaryKeys : 
                System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).ToString : 
                System.Void
                -> System.String

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).get_OverrideSpecifiedNames : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).get_ProcessDictionaryKeys : 
                System.Void
                -> System.Boolean

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).set_OverrideSpecifiedNames : 
                value:System.Boolean
                -> System.Void

        - (Instance/Inheritance of Newtonsoft.Json.Serialization.SnakeCaseNamingStrategy).set_ProcessDictionaryKeys : 
                value:System.Boolean
                -> System.Void
