### 43.0.0.0 - Dec 02 2016 - Major

        
* SpiseMisu.Enum

        Added: SpiseMisu.Enum values: [ Bar:0; Foo:42 ]

        Removed: SpiseMisu.Enum values: [ Bar:42; Foo:0 ]
### 43.0.0.0 - Dec 02 2016 - Major

        
* SpiseMisu.Enum

        Added: SpiseMisu.Enum values: [ Bar:42; Baz:4; Foo:0; Qux:2 ]

        Removed: SpiseMisu.Enum values: [ Bar:42; Foo:0 ]

        + SpiseMisu.Enum.Baz : 
                SpiseMisu.Enum

        + SpiseMisu.Enum.Qux : 
                SpiseMisu.Enum
