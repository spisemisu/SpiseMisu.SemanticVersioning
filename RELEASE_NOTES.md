### 1.0.0 - Dec 16 2016

* SpiseMisu

        SpiseMisu (Namespace)

        
* SpiseMisu.NuGet

        SpiseMisu.NuGet (.NET type: Module and base: System.Object)

        (Instance/Inheritance of SpiseMisu.NuGet).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.NuGet).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of SpiseMisu.NuGet).ToString : 
                System.Void
                -> System.String

        SpiseMisu.NuGet.get : 
                apiUrl:Microsoft.FSharp.Core.FSharpOption<System.String>
                -> packageID:System.String
                -> versionNumber:Microsoft.FSharp.Core.FSharpOption<System.String>
                -> dotnet:SpiseMisu.NuGet.dotNet
                -> Microsoft.FSharp.Core.FSharpChoice<System.Tuple<System.String,System.Reflection.Assembly>,System.Exception>

        SpiseMisu.NuGet.package : 
                url:System.String
                -> Microsoft.FSharp.Control.FSharpAsync<Microsoft.FSharp.Core.FSharpChoice<System.IO.Compression.ZipArchive,System.Exception>>

        
* SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet (.NET type: SumType and base: System.Object)

        SpiseMisu.NuGet.dotNet values: [ Net20 | Net35 | Net40 | Net45 | NetStandard1Dot0 | Netcore451 | PortableDashNet40 | PortableDashNet45 ]

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).CompareTo : 
                obj:SpiseMisu.NuGet.dotNet
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).CompareTo : 
                obj:System.Object * comp:System.Collections.IComparer
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).CompareTo : 
                obj:System.Object
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).Equals : 
                obj:SpiseMisu.NuGet.dotNet
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).Equals : 
                obj:System.Object * comp:System.Collections.IEqualityComparer
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).GetHashCode : 
                comp:System.Collections.IEqualityComparer
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).IsNet20 : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).IsNet35 : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).IsNet40 : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).IsNet45 : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).IsNetStandard1Dot0 : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).IsNetcore451 : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).IsPortableDashNet40 : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).IsPortableDashNet45 : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).Tag : 
                System.Int32

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).get_IsNet20 : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).get_IsNet35 : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).get_IsNet40 : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).get_IsNet45 : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).get_IsNetStandard1Dot0 : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).get_IsNetcore451 : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).get_IsPortableDashNet40 : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).get_IsPortableDashNet45 : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet).get_Tag : 
                System.Void
                -> System.Int32

        SpiseMisu.NuGet.dotNet.Net20 : 
                SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.Net35 : 
                SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.Net40 : 
                SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.Net45 : 
                SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.NetStandard1Dot0 : 
                SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.Netcore451 : 
                SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.PortableDashNet40 : 
                SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.PortableDashNet45 : 
                SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.get_Net20 : 
                System.Void
                -> SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.get_Net35 : 
                System.Void
                -> SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.get_Net40 : 
                System.Void
                -> SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.get_Net45 : 
                System.Void
                -> SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.get_NetStandard1Dot0 : 
                System.Void
                -> SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.get_Netcore451 : 
                System.Void
                -> SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.get_PortableDashNet40 : 
                System.Void
                -> SpiseMisu.NuGet.dotNet

        SpiseMisu.NuGet.dotNet.get_PortableDashNet45 : 
                System.Void
                -> SpiseMisu.NuGet.dotNet

        
* SpiseMisu.NuGet.dotNet+Tags

        SpiseMisu.NuGet.dotNet+Tags (.NET type: UnionTags and base: System.Object)

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet+Tags).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet+Tags).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet+Tags).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of SpiseMisu.NuGet.dotNet+Tags).ToString : 
                System.Void
                -> System.String

        SpiseMisu.NuGet.dotNet+Tags.Net20 : 
                System.Int32

        SpiseMisu.NuGet.dotNet+Tags.Net35 : 
                System.Int32

        SpiseMisu.NuGet.dotNet+Tags.Net40 : 
                System.Int32

        SpiseMisu.NuGet.dotNet+Tags.Net45 : 
                System.Int32

        SpiseMisu.NuGet.dotNet+Tags.NetStandard1Dot0 : 
                System.Int32

        SpiseMisu.NuGet.dotNet+Tags.Netcore451 : 
                System.Int32

        SpiseMisu.NuGet.dotNet+Tags.PortableDashNet40 : 
                System.Int32

        SpiseMisu.NuGet.dotNet+Tags.PortableDashNet45 : 
                System.Int32

        
* SpiseMisu.Semantic

        SpiseMisu.Semantic (.NET type: Module and base: System.Object)

        (Instance/Inheritance of SpiseMisu.Semantic).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of SpiseMisu.Semantic).ToString : 
                System.Void
                -> System.String

        
* SpiseMisu.Semantic.Versioning

        SpiseMisu.Semantic.Versioning (.NET type: Module and base: System.Object)

        (Instance/Inheritance of SpiseMisu.Semantic.Versioning).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.Versioning).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.Versioning).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of SpiseMisu.Semantic.Versioning).ToString : 
                System.Void
                -> System.String

        SpiseMisu.Semantic.Versioning.asmbump : 
                released:System.Reflection.Assembly
                -> modified:System.Reflection.Assembly
                -> System.String

        SpiseMisu.Semantic.Versioning.asmdiff : 
                source:System.Reflection.Assembly
                -> target:System.Reflection.Assembly
                -> System.String[]

        SpiseMisu.Semantic.Versioning.bump : 
                verNr:System.String
                -> released:System.Reflection.Assembly
                -> modified:System.Reflection.Assembly
                -> System.String

        SpiseMisu.Semantic.Versioning.diff : 
                source:System.Reflection.Assembly
                -> target:System.Reflection.Assembly
                -> System.String[]

        SpiseMisu.Semantic.Versioning.docs : 
                asm:System.Reflection.Assembly
                -> System.String[]

        SpiseMisu.Semantic.Versioning.nugetbump : 
                apiUrl:Microsoft.FSharp.Core.FSharpOption<System.String>
                -> packageID:System.String
                -> dotnet:SpiseMisu.NuGet.dotNet
                -> modified:System.Reflection.Assembly
                -> System.String

        SpiseMisu.Semantic.Versioning.nugetdiff : 
                apiUrl1:Microsoft.FSharp.Core.FSharpOption<System.String>
                -> packageID1:System.String
                -> dotnet1:SpiseMisu.NuGet.dotNet
                -> verNr1:Microsoft.FSharp.Core.FSharpOption<System.String>
                -> apiUrl2:Microsoft.FSharp.Core.FSharpOption<System.String>
                -> packageID2:System.String
                -> dotnet2:SpiseMisu.NuGet.dotNet
                -> verNr2:Microsoft.FSharp.Core.FSharpOption<System.String>
                -> System.String[]

        SpiseMisu.Semantic.Versioning.raw : 
                asm:System.Reflection.Assembly
                -> Microsoft.FSharp.Collections.FSharpSet<System.Tuple<System.String,System.String>>

        SpiseMisu.Semantic.Versioning.versionNr : 
                asm:System.Reflection.Assembly
                -> System.String

        
* SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType (.NET type: SumType and base: System.Object)

        SpiseMisu.Semantic.netType values: [ Abstract | Class | Enum | Interface | Module | RecordType | Static | Struct | SumType | UnionConstructor | UnionTags ]

        (Instance/Inheritance of SpiseMisu.Semantic.netType).CompareTo : 
                obj:SpiseMisu.Semantic.netType
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.netType).CompareTo : 
                obj:System.Object * comp:System.Collections.IComparer
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.netType).CompareTo : 
                obj:System.Object
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.netType).Equals : 
                obj:SpiseMisu.Semantic.netType
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).Equals : 
                obj:System.Object * comp:System.Collections.IEqualityComparer
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.netType).GetHashCode : 
                comp:System.Collections.IEqualityComparer
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.netType).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsAbstract : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsClass : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsEnum : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsInterface : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsModule : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsRecordType : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsStatic : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsStruct : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsSumType : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsUnionConstructor : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).IsUnionTags : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).Tag : 
                System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.netType).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsAbstract : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsClass : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsEnum : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsInterface : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsModule : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsRecordType : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsStatic : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsStruct : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsSumType : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsUnionConstructor : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_IsUnionTags : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType).get_Tag : 
                System.Void
                -> System.Int32

        SpiseMisu.Semantic.netType.Abstract : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.Class : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.Enum : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.Interface : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.Module : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.RecordType : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.Static : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.Struct : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.SumType : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.UnionConstructor : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.UnionTags : 
                SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_Abstract : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_Class : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_Enum : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_Interface : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_Module : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_RecordType : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_Static : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_Struct : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_SumType : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_UnionConstructor : 
                System.Void
                -> SpiseMisu.Semantic.netType

        SpiseMisu.Semantic.netType.get_UnionTags : 
                System.Void
                -> SpiseMisu.Semantic.netType

        
* SpiseMisu.Semantic.netType+Tags

        SpiseMisu.Semantic.netType+Tags (.NET type: UnionTags and base: System.Object)

        (Instance/Inheritance of SpiseMisu.Semantic.netType+Tags).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.netType+Tags).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.netType+Tags).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of SpiseMisu.Semantic.netType+Tags).ToString : 
                System.Void
                -> System.String

        SpiseMisu.Semantic.netType+Tags.Abstract : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.Class : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.Enum : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.Interface : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.Module : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.RecordType : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.Static : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.Struct : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.SumType : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.UnionConstructor : 
                System.Int32

        SpiseMisu.Semantic.netType+Tags.UnionTags : 
                System.Int32

        
* SpiseMisu.Semantic.version

        SpiseMisu.Semantic.version (.NET type: SumType and base: System.Object)

        SpiseMisu.Semantic.version values: [ Major | Minor | Patch ]

        (Instance/Inheritance of SpiseMisu.Semantic.version).CompareTo : 
                obj:SpiseMisu.Semantic.version
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.version).CompareTo : 
                obj:System.Object * comp:System.Collections.IComparer
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.version).CompareTo : 
                obj:System.Object
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.version).Equals : 
                obj:SpiseMisu.Semantic.version
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version).Equals : 
                obj:System.Object * comp:System.Collections.IEqualityComparer
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.version).GetHashCode : 
                comp:System.Collections.IEqualityComparer
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.version).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of SpiseMisu.Semantic.version).IsMajor : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version).IsMinor : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version).IsPatch : 
                System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version).Tag : 
                System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.version).ToString : 
                System.Void
                -> System.String

        (Instance/Inheritance of SpiseMisu.Semantic.version).get_IsMajor : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version).get_IsMinor : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version).get_IsPatch : 
                System.Void
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version).get_Tag : 
                System.Void
                -> System.Int32

        SpiseMisu.Semantic.version.Major : 
                SpiseMisu.Semantic.version

        SpiseMisu.Semantic.version.Minor : 
                SpiseMisu.Semantic.version

        SpiseMisu.Semantic.version.Patch : 
                SpiseMisu.Semantic.version

        SpiseMisu.Semantic.version.get_Major : 
                System.Void
                -> SpiseMisu.Semantic.version

        SpiseMisu.Semantic.version.get_Minor : 
                System.Void
                -> SpiseMisu.Semantic.version

        SpiseMisu.Semantic.version.get_Patch : 
                System.Void
                -> SpiseMisu.Semantic.version

        
* SpiseMisu.Semantic.version+Tags

        SpiseMisu.Semantic.version+Tags (.NET type: UnionTags and base: System.Object)

        (Instance/Inheritance of SpiseMisu.Semantic.version+Tags).Equals : 
                obj:System.Object
                -> System.Boolean

        (Instance/Inheritance of SpiseMisu.Semantic.version+Tags).GetHashCode : 
                System.Void
                -> System.Int32

        (Instance/Inheritance of SpiseMisu.Semantic.version+Tags).GetType : 
                System.Void
                -> System.Type

        (Instance/Inheritance of SpiseMisu.Semantic.version+Tags).ToString : 
                System.Void
                -> System.String

        SpiseMisu.Semantic.version+Tags.Major : 
                System.Int32

        SpiseMisu.Semantic.version+Tags.Minor : 
                System.Int32

        SpiseMisu.Semantic.version+Tags.Patch : 
                System.Int32
